/*
 FUSE: Filesystem in Userspace
 Copyright (C) 2001-2007  Miklos Szeredi <miklos@szeredi.hu>

 This program can be distributed under the terms of the GNU GPL.
 See the file COPYING.

 gcc -Wall hello.c `pkg-config fuse --cflags --libs` -o hello
 */

#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <jni.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>
#include <unistd.h>

//static pthread_mutex_t mutex1;
char *jar = "../target/filesystem-20210401.jar";

struct Userdata
{
  JNIEnv *env;
  JavaVM *jvm;

  jclass cls;
  jmethodID readdir;
  jmethodID isFile;
  jmethodID getBlockNo;
  jmethodID readFileContent;
  jmethodID getFileLength;
};

JNIEnv *create_vm(JavaVM **jvm)
{
  printf("create_vm\n");
  JNIEnv *env;
  JavaVMInitArgs vm_args;
  JavaVMOption options;
  if (access(jar, F_OK) != 0)
  {
    printf("%s not exit\n", jar);
    exit(1);
  }
  char temp[1000];
  strcpy(temp, "-Djava.class.path=");
  strcat(temp, jar);
  options.optionString = temp;
  vm_args.version = JNI_VERSION_1_6;
  vm_args.nOptions = 1;
  vm_args.options = &options;
  vm_args.ignoreUnrecognized = 0;

  printf("JNI_CreateJavaVM\n");
  JNI_CreateJavaVM(jvm, (void **)&env, &vm_args);
  return env;
}

static int peter_open(const char *path, struct fuse_file_info *fi)
{
  printf("peter_open = %s\n", path);
  //	if (strcmp(path, peter_path) != 0)
  //		return -ENOENT;

  //	if ((fi->flags & 3) != O_RDONLY)
  //		return -EACCES;

  struct Userdata *userdata = fuse_get_context()->private_data;
  (*userdata->jvm)->AttachCurrentThreadAsDaemon(userdata->jvm, (void **)&userdata->env, NULL);
  jstring pathPara = (*userdata->env)->NewStringUTF(userdata->env, path);
  long blockNo = (*userdata->env)->CallStaticLongMethod(userdata->env, userdata->cls, userdata->getBlockNo, pathPara);
  if (blockNo == -1)
  {
    return -ENOENT;
  }
  printf("blockNo = %ld\n", blockNo);
  return 0;
}

static int peter_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
  printf("peter_readdir = %s\n", path);
  (void)offset;
  (void)fi;

  struct Userdata *userdata = fuse_get_context()->private_data;
  (*userdata->jvm)->AttachCurrentThreadAsDaemon(userdata->jvm, (void **)&userdata->env, NULL);
  jstring pathPara = (*userdata->env)->NewStringUTF(userdata->env, path);
  long blockNo = (*userdata->env)->CallStaticLongMethod(userdata->env, userdata->cls, userdata->getBlockNo, pathPara);
  if (blockNo == -1)
  {
    return -ENOENT;
  }

  filler(buf, ".", NULL, 0);
  filler(buf, "..", NULL, 0);
  //filler(buf, peter_path + 1, NULL, 0);

  //pthread_mutex_lock(&mutex1);

  jstring paraReadDir = (*userdata->env)->NewStringUTF(userdata->env, path);
  jobjectArray returnArray = (jstring)(*userdata->env)->CallStaticObjectMethod(userdata->env, userdata->cls, userdata->readdir, paraReadDir);
  int len = (*userdata->env)->GetArrayLength(userdata->env, returnArray);
  int x;
  printf("\tlen = %d\n", len);
  for (x = 0; x < len; x++)
  {
    jobject obj = (*userdata->env)->GetObjectArrayElement(userdata->env, returnArray, x);
    printf("\t\tfilepath = %s\n", (*userdata->env)->GetStringUTFChars(userdata->env, obj, 0));
    filler(buf, (*userdata->env)->GetStringUTFChars(userdata->env, obj, 0), NULL, 0);
  }
  //pthread_mutex_unlock(&mutex1);

  return 0;
}

static int peter_getattr(const char *path, struct stat *stbuf)
{
  //	pthread_mutex_lock(&mutex1);
  printf("peter_getattr = %s\n", path);
  int res = 0;
  memset(stbuf, 0, sizeof(struct stat));
  struct Userdata *userdata = fuse_get_context()->private_data;
  (*userdata->jvm)->AttachCurrentThreadAsDaemon(userdata->jvm, (void **)&userdata->env, NULL);
  jstring pathPara = (*userdata->env)->NewStringUTF(userdata->env, path);
  long blockNo = (*userdata->env)->CallStaticLongMethod(userdata->env, userdata->cls, userdata->getBlockNo, pathPara);
  printf("peter_getattr blockNo=%ld\n", blockNo);
  if (blockNo == -1)
  {
    printf("return ENOENT\n");
    return -ENOENT;
  }

  pathPara = (*userdata->env)->NewStringUTF(userdata->env, path);
  bool isFile = (*userdata->env)->CallStaticBooleanMethod(userdata->env, userdata->cls, userdata->isFile, pathPara);
  printf("path=%s, %x\n", path, isFile);
  if (isFile)
  {
    jlong blockNoL = blockNo;
    int length = (*userdata->env)->CallStaticLongMethod(userdata->env, userdata->cls, userdata->getFileLength, blockNoL);
    stbuf->st_mode = S_IFREG | 0444;
    stbuf->st_nlink = 1;
    stbuf->st_size = length;

    printf("length=%d\n", length);
  }
  else
  {
    stbuf->st_mode = S_IFDIR | 0755;
    stbuf->st_nlink = 1;
  }
  //	pthread_mutex_unlock(&mutex1);
  return res;
}

static int peter_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
  printf("peter_read = %s\n", path);
  (void)fi;

  struct Userdata *userdata = fuse_get_context()->private_data;
  (*userdata->jvm)->AttachCurrentThreadAsDaemon(userdata->jvm, (void **)&userdata->env, NULL);
  jstring pathPara = (*userdata->env)->NewStringUTF(userdata->env, path);
  long blockNo = (*userdata->env)->CallStaticLongMethod(userdata->env, userdata->cls, userdata->getBlockNo, pathPara);
  if (blockNo == -1)
  {
    return -ENOENT;
  }

  printf("peter_read = %s, blockNo=%ld\n", path, blockNo);

  //	jlong blockNoL = blockNo;
  //jstring content = (jstring) (*userdata->env)->CallStaticObjectMethod(userdata->env, userdata->cls, userdata->readFileContent, blockNo);
  jbyteArray content = (jbyteArray)(*userdata->env)->CallStaticObjectMethod(userdata->env, userdata->cls, userdata->readFileContent, blockNo);
  jboolean isCopy;
  jbyte *b = (*userdata->env)->GetByteArrayElements(userdata->env, content, &isCopy);
  const char *jcstr = (char *)b;
  //const char *jcstr = (*userdata->env)->GetStringUTFChars(userdata->env, content, 0);
  //	printf("jcstr=%s\n", jcstr);
  //len = strlen(jcstr);
  printf("jcstr=");
  for (int x = 0; x < 30; x++)
  {
    printf("%d, ", (char)jcstr[x]);
  }
  printf("\n");
  //	int length = (*userdata->env)->GetStringLength(userdata->env, content);
  int length = (*userdata->env)->GetArrayLength(userdata->env, content);
  if (offset < length)
  {
    if (offset + size > length)
    {
      size = length - offset;
    }
    memcpy(buf, jcstr + offset, size);
  }
  else
  {
    size = 0;
  }

  return size;
}

static int peter_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
  printf("peter_create = %s\n", path);
  return 1;
}

static int peter_fgetattr(const char *path, struct stat *buf, struct fuse_file_info *fi)
{
  printf("peter_fgetattr\n");
  return 0;
}

int peter_rename(const char *oldpath, const char *newpath)
{
  printf("peter_rename\n");
  return 0;
}

int peter_unlink(const char *path)
{
  printf("peter_unlink\n");
  return 0;
}

int peter_rmdir(const char *path)
{
  printf("peter_rmdir\n");
  return 0;
}

int peter_symlink(const char *linkname, const char *path)
{
  printf("peter_symlink\n");
  return 0;
}

int peter_link(const char *oldpath, const char *newpath)
{
  printf("peter_link\n");
  return 0;
}

int peter_release(const char *path, struct fuse_file_info *fi)
{
  printf("peter_release\n");
  return 0;
}

int peter_write(const char *path, const char *buf, size_t size, off_t off, struct fuse_file_info *fi)
{
  printf("peter_write\n");
  return 0;
}

int peter_fsync(const char *path, int datasync, struct fuse_file_info *fi)
{
  printf("peter_fsync\n");
  return 0;
}

int peter_flush(const char *path, struct fuse_file_info *fi)
{
  printf("peter_flush\n");
  return 0;
}

int peter_statfs(const char *path, struct statvfs *buf)
{
  //	struct statvfs {
  //        unsigned long   f_bsize;        /* File system block size */
  //        unsigned long   f_frsize;       /* Fundamental file system block size */
  //        fsblkcnt_t      f_blocks;       /* Blocks on FS in units of f_frsize */
  //        fsblkcnt_t      f_bfree;        /* Free blocks */
  //        fsblkcnt_t      f_bavail;       /* Blocks available to non-root */
  //        fsfilcnt_t      f_files;        /* Total inodes */
  //        fsfilcnt_t      f_ffree;        /* Free inodes */
  //        fsfilcnt_t      f_favail;       /* Free inodes for non-root */
  //        unsigned long   f_fsid;         /* Filesystem ID */
  //        unsigned long   f_flag;         /* Bit mask of values */
  //        unsigned long   f_namemax;      /* Max file name length */
  //	};
  printf("peter_statfs, path=%s\n", path);
  buf->f_bsize = 1234;
  buf->f_frsize = 1234;
  buf->f_blocks = 1234;
  buf->f_bfree = 1234;
  buf->f_bavail = 1234;
  buf->f_files = 1234;
  buf->f_ffree = 1234;
  buf->f_favail = 1234;
  buf->f_fsid = 1234;
  buf->f_flag = 1234;
  buf->f_namemax = 1234;
  return 0;
}

int peter_opendir(const char *path, struct fuse_file_info *fi)
{
  printf("peter_opendir\n");
  return 0;
}

int peter_fsyncdir(const char *path, int datasync, struct fuse_file_info *fi)
{
  printf("peter_fsyncdir\n");
  return 0;
}

int peter_releasedir(const char *path, struct fuse_file_info *fi)
{
  printf("peter_releasedir\n");
  return 0;
}

int peter_lock(const char *path, struct fuse_file_info *fi, int cmd, struct flock *lock)
{
  printf("peter_lock, %s\n", path);
  return 0;
}

int peter_chmod(const char *path, mode_t mode)
{
  printf("peter_chmod, %s\n", path);
  return 0;
}

int peter_chown(const char *path, uid_t uid, gid_t gid)
{
  printf("peter_chown, %s\n", path);
  return 0;
}

int peter_truncate(const char *path, off_t size)
{
  printf("peter_truncate, %s\n", path);
  return 0;
}

int peter_ftruncate(const char *path, off_t size, struct fuse_file_info *fi)
{
  printf("peter_ftruncate, %s\n", path);
  return 0;
}

int peter_utimens(const char *path, const struct timespec tv[2])
{
  printf("peter_utimens, %s\n", path);
  return 0;
}

int peter_access(const char *path, int mask)
{
  printf("peter_access, %s\n", path);
  return 0;
}

int peter_readlink(const char *path, char *buf, size_t len)
{
  printf("peter_readlink, %s\n", path);
  return 0;
}

int peter_mknod(const char *path, mode_t mode, dev_t rdev)
{
  printf("peter_mknod, %s\n", path);
  return 0;
}

int peter_mkdir(const char *path, mode_t mode)
{
  printf("peter_mkdir, %s\n", path);
  return 0;
}

#ifdef __APPLE__

int peter_setxattr(const char *path, const char *name, const char *value, size_t size, int flags, uint32_t position)
{
  printf("peter_setxattr, %s\n", path);
  return 0;
}
#else

int peter_setxattr(const char *path, const char *name, const char *value, size_t size, int flags)
{
  printf("peter_setxattr\n");
  return 0;
}
#endif
#ifdef __APPLE__

int peter_getxattr(const char *path, const char *name, char *value, size_t size, uint32_t position)
{
  printf("peter_getxattr, %s\n", path);
  return 0;
}
#else

int peter_getxattr(const char *path, const char *name, char *value, size_t size)
{
  printf("peter_getxattr\n");
  return 0;
}
#endif

int peter_listxattr(const char *path, char *list, size_t size)
{
  printf("peter_listxattr, %s\n", path);
  return 0;
}

int peter_removexattr(const char *path, const char *name)
{
  printf("peter_removexattr, %s\n", path);
  return 0;
}

int peter_bmap(const char *path, size_t blocksize, uint64_t *idx)
{
  printf("peter_bmap, %s\n", path);
  return 0;
}

void *peter_init(struct fuse_conn_info *conn)
{
  printf("peter_init\n");
  return NULL;
}

void *peter_destroy(void *fs)
{
  printf("peter_destroy\n");
  return NULL;
}

static struct fuse_operations peter_oper = {
    .open = peter_open,
    .readdir = peter_readdir,
    .getattr = peter_getattr,
    .read = peter_read,
    .fgetattr = peter_fgetattr,
    .rename =
        peter_rename,
    .unlink = peter_unlink,
    .rmdir = peter_rmdir,
    .symlink = peter_symlink,
    .link = peter_link,
    .release = peter_release,
    .write = peter_write,
    .fsync =
        peter_fsync,
    .flush = peter_flush,
    .statfs = peter_statfs,
    .opendir = peter_opendir,
    .fsyncdir = peter_fsyncdir,
    .releasedir = peter_releasedir,
    .lock = peter_lock,
    .chmod = peter_chmod,
    .chown = peter_chown,
    .truncate = peter_truncate,
    .ftruncate = peter_ftruncate,
    .utimens = peter_utimens,
    .access = peter_access,
    .readlink =
        peter_readlink,
    .mknod = peter_mknod,
    .mkdir = peter_mkdir,
    .setxattr = peter_setxattr,
    .getxattr = peter_getxattr,
    .listxattr = peter_listxattr,
    .removexattr =
        peter_removexattr,
    .bmap = peter_bmap,
    /*.init = peter_init,*/ .destroy = peter_destroy,
    .create = peter_create,
};

int main(int argc, char *argv[])
{
  printf("starting qfs-fuse\n");
  //	pthread_mutex_init(&mutex1, NULL);
  struct Userdata userdata;
  userdata.env = create_vm(&userdata.jvm);
  userdata.cls = (*userdata.env)->FindClass(userdata.env, "hk/quantr/fs/fuse/FuseStub");

  printf("building file system\n");

  if (userdata.cls == 0)
  {
    printf("load class error\n");
    exit(-1);
  }

  userdata.readdir = (*userdata.env)->GetStaticMethodID(userdata.env, userdata.cls, "readdir", "(Ljava/lang/String;)[Ljava/lang/String;");
  if (userdata.readdir == 0)
  {
    printf("load readdir error\n");
    exit(-1);
  }

  userdata.isFile = (*userdata.env)->GetStaticMethodID(userdata.env, userdata.cls, "isFile", "(Ljava/lang/String;)Z");
  if (userdata.isFile == 0)
  {
    printf("load isFile error\n");
    exit(-1);
  }

  userdata.getBlockNo = (*userdata.env)->GetStaticMethodID(userdata.env, userdata.cls, "getBlockNo", "(Ljava/lang/String;)J");
  if (userdata.getBlockNo == 0)
  {
    printf("load getBlockNo error\n");
    exit(-1);
  }

  userdata.readFileContent = (*userdata.env)->GetStaticMethodID(userdata.env, userdata.cls, "readFileContent", "(J)[B");
  if (userdata.readFileContent == 0)
  {
    printf("load readFileContent error\n");
    exit(-1);
  }

  userdata.getFileLength = (*userdata.env)->GetStaticMethodID(userdata.env, userdata.cls, "getFileLength", "(J)J");
  if (userdata.getFileLength == 0)
  {
    printf("load getFileLength error\n");
    exit(-1);
  }

  printf("starting fuse\n");

  return fuse_main(argc, argv, &peter_oper, &userdata);
}
