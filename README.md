# Quantr File System

A very lightweight experimental file system, we have built

- Cli to compress folder into fs image
- Cli to extract image into folder
- A simple FUSE stub, so you can mount the fs image in Mac or Linux
- Super simple permission system

![](https://www.quantr.foundation/wp-content/uploads/2021/04/qfs1.png)

![](https://www.quantr.foundation/wp-content/uploads/2021/04/qfs2.png)

# CLI

**compress:**

-projectfile generate/project.ini -inputdirectory generate/root -output pfs.img -partitionsize 100M -partitionoffset 0m -blocksize 4096 -partitionname PFS -c

**decompress:**

-projectfile generate/project.ini -input pfs.img -partitionsize 100M -partitionoffset 0M -outputdirectory generate/temp -x

**decompress with GUI:**

-projectfile generate/project.ini -input pfs.img -partitionsize 100M -partitionoffset 0M -outputdirectory generate/temp -x -gui%    
