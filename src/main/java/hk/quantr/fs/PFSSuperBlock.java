package hk.quantr.fs;

/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class PFSSuperBlock {
	public byte id[] = { 'S', 'B' };
	public byte partitionName[] = new byte[500];
	public byte rootDirectoryLink[] = new byte[8];
	public byte createTime[] = new byte[8];
	public byte lastModifiedTime[] = new byte[8];
	public byte blockSize[] = new byte[2];
	public byte numberOfFreeBlockList[] = new byte[8];
	public byte unused[] = new byte[3560];

	public PFSSuperBlock() {
	}

	public String toString() {
		return "Super Block";
	}
}
