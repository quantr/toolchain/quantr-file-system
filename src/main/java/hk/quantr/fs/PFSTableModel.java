package hk.quantr.fs;

import java.io.File;

import javax.swing.table.DefaultTableModel;

/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class PFSTableModel extends DefaultTableModel {
	private String[] columnNames1 = { "Offset", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" };
	private String[] columnNames2 = { "Block No.", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" };
	private int mode = 0;
	private File file;
	private long showOffset = 0;
	private boolean showHex = true;
	private boolean showDecimal = false;
	private boolean showOctal = false;
	private boolean showBinary = false;
	private boolean showCharacter = false;

	public PFSTableModel() {
		super();
	}

	public int getColumnCount() {
		if (this.getMode() == 0) {
			return columnNames1.length;
		} else {
			return columnNames2.length;
		}
	}

	public int getRowCount() {
		try {
			if (PFSImageConstants.blockSize == 0) {
				return 0;
			} else {
				if (mode == 0) {
					int x = (int) ((file.length() - showOffset) / (getColumnCount() - 1));
					// System.out.println(x);
					return x;
				} else {
					if (((file.length() - showOffset) / PFSImageConstants.blockSize) < getColumnCount()) {
						return 1;
					} else {
						return (int) ((file.length() - showOffset) / PFSImageConstants.blockSize / (getColumnCount() - 1));
					}
				}
			}
		} catch (Exception ex) {
			return 0;
		}
	}

	public String getColumnName(int col) {
		if (this.getMode() == 0) {
			return columnNames1[col];
		} else {
			return columnNames2[col];
		}
	}

	public boolean isCellEditable(int row, int col) {
		return false;
	}

	public int getMode() {
		return mode;
	}

	public long getShowOffset() {
		return showOffset;
	}

	public Boolean getShowCharacter() {
		return showCharacter;
	}

	public void setMode(int mode) {
		this.mode = mode;
		this.fireTableDataChanged();
	}

	public void setShowOffset(long offset) {
		this.showOffset = offset;
		this.fireTableDataChanged();
	}

	public void setShowHex(boolean showHex) {
		this.showHex = showHex;
		this.fireTableDataChanged();
	}

	public void setShowDecimal(boolean showDecimal) {
		this.showDecimal = showDecimal;
		this.fireTableDataChanged();
	}

	public void setShowOctal(boolean showOctal) {
		this.showOctal = showOctal;
		this.fireTableDataChanged();
	}

	public void setShowBinary(boolean showBinary) {
		this.showBinary = showBinary;
		this.fireTableDataChanged();
	}

	public void setShowCharacter(boolean showCharacter) {
		this.showCharacter = showCharacter;
		this.fireTableDataChanged();
	}

	public void setValue(String filename) {
		try {
			if (filename == null) {
				this.file = null;
				PFSImageConstants.blockSize = 0;
				this.showOffset = 0;
			} else {
				this.file = new File(filename);
				this.showOffset = 0;
			}
			this.fireTableDataChanged();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public Object getValueAt(int row, int column) {
		// System.out.println(">>"+row+","+column);
		if (mode == 0) {
			if (column == 0) {
				return row * 16;
			} else {
				byte c = PFSCommonLib.readFileByte(file, (row * (getColumnCount() - 1)) + column - 1 + showOffset);
				String returnValue = "";
				if (showHex) {
					returnValue += Long.toHexString(c & 0xff);
				}
				if (showDecimal) {
					returnValue += "/" + (c & 0xff);
				}
				if (showOctal) {
					returnValue += "/" + (Long.toOctalString(c & 0xff));
				}
				if (showBinary) {
					returnValue += "/" + (Long.toBinaryString(c & 0xff));
				}
				if (showCharacter) {
					returnValue += "/" + ((char) c);
				}
				if (returnValue.startsWith("/")) {
					returnValue = returnValue.substring(1);
				}
				return returnValue;
			}
		} else {
			if (column == 0) {
				return row * 16;
			} else {
				long offset = (long) ((row * (getColumnCount() - 1)) + column - 1) * PFSImageConstants.blockSize;
				if (offset < 0) {
					System.out.println("net");
				}
				return PFSCommonLib.readFile(file, offset + showOffset, PFSImageConstants.blockSize);
			}
		}
	}
}
