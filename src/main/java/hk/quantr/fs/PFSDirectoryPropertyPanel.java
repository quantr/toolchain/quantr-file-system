package hk.quantr.fs;

import java.awt.FlowLayout;
import java.io.File;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;

import hk.quantr.Dialogs.SearchDialog;

/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class PFSDirectoryPropertyPanel extends JPanel {
	PFSPermissionPanel pFSPermissionPanel1 = new PFSPermissionPanel();
	private PFSPermissionPanel pFSPermissionPanel1_1;
	PFSOwnerPanel pFSOwnerPanel1;
	private JLabel pNodeNoLabel;
	private JPanel pNodeNoPanel;
	PFSCreateTimeAndLastModifiedTimePanel pFSCreateTimeAndLastModifiedTimePanel1;
	JFrame parent;

	public PFSDirectoryPropertyPanel(String projectFile, String selectedFile) {
		super();
		try {
			pFSCreateTimeAndLastModifiedTimePanel1 = new PFSCreateTimeAndLastModifiedTimePanel(projectFile, selectedFile);
			pFSPermissionPanel1_1 = new PFSPermissionPanel(new File(projectFile), new File(selectedFile));
			pFSOwnerPanel1 = new PFSOwnerPanel(parent, new File(projectFile), new File(selectedFile));
			jbInit();

			// search for the node number
			SearchDialog pSearchDialog = new SearchDialog();
			Vector<HashMap<String, String>> vector = new Vector<HashMap<String, String>>();
			pSearchDialog.pFilenameOrDirectoryNameTextField.setText(
					"/" + new File(PFSSettingConstants.tempDirectory).getName() + selectedFile.substring(PFSSettingConstants.tempDirectory.length()).replaceAll("\\\\", "/"));
			pSearchDialog.searchPFS(pSearchDialog.SEARCH_BY_FILENAME_OR_DIRECTORY_NAME, vector);
			if (vector.size() > 0) {
				this.pNodeNoLabel.setText("Node number : " + vector.get(0).get("blockNumber"));
			}
			// end search for the node number
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void jbInit() throws Exception {
		{
			pNodeNoPanel = new JPanel();
			FlowLayout pNodeNumberPanelLayout = new FlowLayout();
			pNodeNumberPanelLayout.setAlignment(FlowLayout.LEFT);
			pNodeNoPanel.setLayout(pNodeNumberPanelLayout);
			{
				pNodeNoLabel = new JLabel();
				pNodeNoPanel.add(pNodeNoLabel);
			}
		}
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING).addComponent(pNodeNoPanel, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 584, Short.MAX_VALUE)
								.addComponent(pFSCreateTimeAndLastModifiedTimePanel1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 584, Short.MAX_VALUE)
								.addComponent(pFSOwnerPanel1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 584, Short.MAX_VALUE)
								.addComponent(pFSPermissionPanel1_1, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 584, Short.MAX_VALUE))
						.addGap(0)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addComponent(pFSPermissionPanel1_1, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED).addComponent(pFSOwnerPanel1, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(pFSCreateTimeAndLastModifiedTimePanel1, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED).addComponent(pNodeNoPanel, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE).addGap(124)));
		setLayout(groupLayout);
	}
}
