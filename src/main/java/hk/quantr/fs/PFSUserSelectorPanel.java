package hk.quantr.fs;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.StringTokenizer;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class PFSUserSelectorPanel extends JPanel {
	BorderLayout borderLayout1 = new BorderLayout();
	JScrollPane JScrollPane1 = new JScrollPane();
	JTable pMainTable = new JTable(new UneditableTableModel());
	JPanel pControlPanel = new JPanel();
	JButton pCancelButton = new JButton();
	JButton pOKButton = new JButton();
	JDialog parent = new JDialog();
	boolean cancel = true;
	String tempDirectory;

	public PFSUserSelectorPanel() {
		super();
		try {
			jbInit();
			initData();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public PFSUserSelectorPanel(JDialog parent, String tempDirectory) {
		super();
		this.parent = parent;
		this.tempDirectory = tempDirectory;
		try {
			jbInit();
			initData();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void jbInit() throws Exception {
		this.setLayout(borderLayout1);
		pCancelButton.setText("Cancel");
		pCancelButton.addActionListener(new PFSUserSelectorPanel_pCancelButton_actionAdapter(this));
		pOKButton.setText("OK");
		pOKButton.addActionListener(new PFSUserSelectorPanel_pOKButton_actionAdapter(this));
		pMainTable.addMouseListener(new PFSUserSelectorPanel_pMainTable_mouseAdapter(this));
		this.add(pControlPanel, java.awt.BorderLayout.SOUTH);
		JScrollPane1.getViewport().add(pMainTable);
		this.add(JScrollPane1, java.awt.BorderLayout.CENTER);
		pControlPanel.add(pOKButton);
		pControlPanel.add(pCancelButton);
		pMainTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	private void initData() {
		try {
			File passwordsFile = new File(tempDirectory + File.separator + "config" + File.separator + "passwords");
			if (passwordsFile.exists()) {
				((DefaultTableModel) pMainTable.getModel()).addColumn("Name");
				BufferedReader br = new BufferedReader(new FileReader(passwordsFile));
				String str;
				while ((str = br.readLine()) != null) {
					if (str.length() >= 3 && str.indexOf(":") != -1) {
						StringTokenizer stk = new StringTokenizer(str, ":");
						((DefaultTableModel) pMainTable.getModel()).addRow(new Object[] { stk.nextToken() });
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public void pCancelButton_actionPerformed(ActionEvent e) {
		parent.dispose();
	}

	public boolean isCancel() {
		return cancel;
	}

	public void pOKButton_actionPerformed(ActionEvent e) {
		cancel = false;
		parent.setVisible(false);
	}

	public String getSelectedUser() {
		if (pMainTable.getSelectedRow() >= 0) {
			return pMainTable.getValueAt(pMainTable.getSelectedRow(), 0).toString();
		} else {
			return null;
		}
	}

	public void pMainTable_mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			cancel = false;
			parent.setVisible(false);
		}
	}

	public class UneditableTableModel extends DefaultTableModel {
		public boolean isCellEditable(int row, int column) {
			return false;
		}
	}
}

class PFSUserSelectorPanel_pMainTable_mouseAdapter extends MouseAdapter {
	private PFSUserSelectorPanel adaptee;

	PFSUserSelectorPanel_pMainTable_mouseAdapter(PFSUserSelectorPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void mouseClicked(MouseEvent e) {
		adaptee.pMainTable_mouseClicked(e);
	}
}

class PFSUserSelectorPanel_pOKButton_actionAdapter implements ActionListener {
	private PFSUserSelectorPanel adaptee;

	PFSUserSelectorPanel_pOKButton_actionAdapter(PFSUserSelectorPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pOKButton_actionPerformed(e);
	}
}

class PFSUserSelectorPanel_pCancelButton_actionAdapter implements ActionListener {
	private PFSUserSelectorPanel adaptee;

	PFSUserSelectorPanel_pCancelButton_actionAdapter(PFSUserSelectorPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pCancelButton_actionPerformed(e);
	}
}
