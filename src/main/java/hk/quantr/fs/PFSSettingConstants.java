package hk.quantr.fs;

/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class PFSSettingConstants {
	public static String filename;
	public static String projectFile;
	public static long partitionOffset;
	public static String tempDirectory;
	public static long partitionSize;
	public static String partitionName;
	public static int blockSize;
	public static String version;
	public static String C, H, S;
	public static int bufferSize = 1 * 1024;
	public static int superBlockSize = 4096;
	public static long trimto = -1;

	private PFSSettingConstants() {
	}
}
