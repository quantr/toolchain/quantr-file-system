package hk.quantr.fs;

import java.awt.Color;
import java.awt.Component;
import java.awt.FontMetrics;
import java.awt.Graphics;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class HexTable_CellRenderer extends DefaultTableCellRenderer {
	private String value;
	private int column;
	private boolean isSelected;

	public HexTable_CellRenderer() {
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		// Component cell = super.getTableCellRendererComponent
		// (table, value, isSelected, hasFocus, row, column);
		// super.getTableCellRendererComponent(table, value, isSelected,
		// hasFocus,
		// row, column);

		this.value = (String) value;
		this.column = column;
		this.isSelected = isSelected;
		return this;
	}

	public void paint(Graphics g) {
		int width = getWidth();
		int height = getHeight();
		if (isSelected) {
			g.setColor(new Color(179, 205, 247));
			g.fillRect(0, 0, width, height);
		} else {
			if (column > 15) {
				g.setColor(new Color(210, 228, 255));
				g.fillRect(0, 0, width, height);
			}
		}

		// draw base line
		// g.setColor(new Color(240, 240, 240));
		// g.drawLine(0, 0, 0, height);
		// g.drawLine(0, height - 1, width, height - 1);

		// draw string
		if (column == 0) {
			g.setColor(Color.red);
		} else {
			g.setColor(Color.black);
		}
		FontMetrics fontMetrics = this.getFontMetrics(getFont());
		int stringWidth = fontMetrics.stringWidth(value);
		g.drawString(value, (width - stringWidth) / 2, 10);
	}

}
