package hk.quantr.fs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
/**
 * <p>
 * Title: Petersoft Java Style
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author not attributable
 * @version 2004
 */
public class PExceptionDialog extends JFrame {
	BorderLayout borderLayout1 = new BorderLayout();
	JPanel pMainPanel = new JPanel();
	JPanel pControlPanel = new JPanel();
	FlowLayout flowLayout1 = new FlowLayout(FlowLayout.RIGHT);
	JButton pOKButton = new JButton();
	JButton pToNotepadButton = new JButton();
	JTabbedPane pMainTabbedPane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);
	BorderLayout borderLayout2 = new BorderLayout();

	Exception exception;

	JPanel JPanel3 = new JPanel();
	BorderLayout borderLayout9 = new BorderLayout();
	JLabel pStatusLabel = new JLabel();
	int sessionNumber = 0;

	public PExceptionDialog() throws HeadlessException {
		super();
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public PExceptionDialog(Exception exception, int sessionNumber) throws HeadlessException {
		super();
		try {
			this.exception = exception;
			this.sessionNumber = sessionNumber;
			jbInit();
			showException(exception);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void jbInit() throws Exception {
		setTitle("Exception");
		this.getContentPane().setLayout(borderLayout1);
		pControlPanel.setLayout(flowLayout1);
		pOKButton.setText("OK");
		pOKButton.addActionListener(new PExceptionDialog_pOKButton_actionAdapter(this));
		pToNotepadButton.setText("To Notepad");
		pMainPanel.setLayout(borderLayout2);

		JPanel3.setLayout(borderLayout9);
		this.getContentPane().add(pMainPanel, java.awt.BorderLayout.CENTER);
		pControlPanel.add(pToNotepadButton);
		pControlPanel.add(pOKButton);
		pMainPanel.add(pMainTabbedPane, java.awt.BorderLayout.CENTER);

		pMainPanel.add(JPanel3, java.awt.BorderLayout.SOUTH);
		JPanel3.add(pControlPanel, java.awt.BorderLayout.EAST);
		JPanel3.add(pStatusLabel, java.awt.BorderLayout.CENTER);

		this.setSize(800, 600);
		PFSCommonLib.centerDialog(this);
	}

	public void pOKButton_actionPerformed(ActionEvent e) {
		this.dispose();
	}

	public void showException(Exception exception) {
		if (exception != null) {
			String stackTrace = "";
			StackTraceElement stackTraceElement[] = exception.getStackTrace();
			for (StackTraceElement element : stackTraceElement) {
				stackTrace += element.toString() + "\n";
			}
			addTab(new Date(), exception.getClass().toString(), stackTrace);
			pStatusLabel.setText("Uploading exception message to Petersoft.com");
			// new PetersoftLogger().logExceptionToPetersoftDB(
			// "PetersoftJavaStyle", "PetersoftJavaStyle", "exception",
			// sessionNumber, exception.getClass().toString() + "\n"
			// + stackTrace, "exception");
			pStatusLabel.setText("");
		}
	}

	private void addTab(Date date, String type, String stackTrace) {
		JPanel pExceptionPanel = new JPanel();
		GroupLayout borderLayout7 = new GroupLayout((JComponent) pExceptionPanel);

		pExceptionPanel.setLayout(borderLayout7);
		JPanel pExceptionDetailPanel = new JPanel();
		GroupLayout pExceptionDetailPanelLayout = new GroupLayout((JComponent) pExceptionDetailPanel);
		pExceptionDetailPanel.setLayout(pExceptionDetailPanelLayout);
		JPanel pStackTracePanel = new JPanel();

		BorderLayout borderLayout5 = new BorderLayout();
		BorderLayout borderLayout6 = new BorderLayout();
		pStackTracePanel.setLayout(borderLayout5);
		pExceptionDetailPanelLayout.setHorizontalGroup(pExceptionDetailPanelLayout.createSequentialGroup());
		pExceptionDetailPanelLayout.setVerticalGroup(pExceptionDetailPanelLayout.createSequentialGroup());
		JLabel pStackTraceLabel = new JLabel();
		pStackTraceLabel.setPreferredSize(new Dimension(100, 18));
		pStackTraceLabel.setText("Stack Trace");
		pStackTraceLabel.setVerticalAlignment(SwingConstants.TOP);
		pStackTracePanel.add(pStackTraceLabel, java.awt.BorderLayout.WEST);
		JScrollPane pStackTraceScrollPane = new JScrollPane();
		pStackTracePanel.add(pStackTraceScrollPane, java.awt.BorderLayout.NORTH);
		pStackTracePanel.add(pStackTraceScrollPane, java.awt.BorderLayout.CENTER);
		JTextArea pStackTraceTextArea = new JTextArea(stackTrace);
		pStackTraceScrollPane.getViewport().add(pStackTraceTextArea);

		JPanel JPanel2 = new JPanel();
		BorderLayout borderLayout8 = new BorderLayout();
		JPanel2.setLayout(borderLayout8);
		borderLayout7.setHorizontalGroup(
				borderLayout7.createParallelGroup().addComponent(JPanel2, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 785, GroupLayout.PREFERRED_SIZE)
						.addComponent(pExceptionDetailPanel, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 785, GroupLayout.PREFERRED_SIZE));
		borderLayout7.setVerticalGroup(borderLayout7.createSequentialGroup().addComponent(JPanel2, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
				.addComponent(pExceptionDetailPanel, GroupLayout.PREFERRED_SIZE, 471, GroupLayout.PREFERRED_SIZE));
		JPanel pTypePanel = new JPanel();
		pTypePanel.setLayout(borderLayout6);
		JLabel pTypeLabel = new JLabel();
		JTextField pTypeTextField = new JTextField(type);
		pTypeLabel.setPreferredSize(new Dimension(100, 18));
		pTypeLabel.setText("Type");
		pTypePanel.add(pTypeTextField, java.awt.BorderLayout.CENTER);
		pTypePanel.add(pTypeLabel, java.awt.BorderLayout.WEST);
		JPanel2.add(pTypePanel, java.awt.BorderLayout.CENTER);
		JPanel pDateTimePanel = new JPanel();
		BorderLayout borderLayout3 = new BorderLayout();
		pDateTimePanel.setLayout(borderLayout3);
		JPanel2.add(pDateTimePanel, java.awt.BorderLayout.NORTH);
		JLabel pDateTimeLabel = new JLabel();
		JTextField pDateTimeTextField = new JTextField(date.toString());
		pDateTimePanel.add(pDateTimeTextField, java.awt.BorderLayout.CENTER);
		pDateTimeLabel.setPreferredSize(new Dimension(100, 18));
		pDateTimeLabel.setText("Date / Time");
		pDateTimePanel.add(pDateTimeLabel, java.awt.BorderLayout.WEST);
		pMainTabbedPane.add(pExceptionPanel, new SimpleDateFormat("y/M/d H:m:s").format(date), 0);
		pMainTabbedPane.setSelectedIndex(0);
		PFSCommonLib.highlightUsingRegularExpression(pStackTraceTextArea, "[a-zA-Z]+[.]java", new Color(0xff, 0x66, 0xff));

		// StackTraceElement stackTraceElement[] = exception.getStackTrace();
		// for (StackTraceElement element : stackTraceElement) {
		// this.pStackTraceTextArea.setText(this.pStackTraceTextArea.getText() +
		// element.toString() + "\n");
		// }
		//
		// this.pDateTimeTextField.setText(new Date().toString());
		// this.pTypeTextField.setText(exception.getClass().toString());
	}
}

class PExceptionDialog_pOKButton_actionAdapter implements ActionListener {
	private PExceptionDialog adaptee;

	PExceptionDialog_pOKButton_actionAdapter(PExceptionDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pOKButton_actionPerformed(e);
	}
}
