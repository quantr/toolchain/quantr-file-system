package hk.quantr.fs;

/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class PFSImageConstants {
	public static long partitionOffset;
	public static int superBlockSize = 4096;
	public static int blockSize;
	public static String filename;
	public static long partitionSize;

	private PFSImageConstants() {
	}
}
