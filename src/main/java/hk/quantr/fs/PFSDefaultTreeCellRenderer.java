package hk.quantr.fs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;

public class PFSDefaultTreeCellRenderer extends JLabel implements TreeCellRenderer {
	static ImageIcon fileIcon = new ImageIcon(QFSBuilder.class.getResource("images/tree/file/file.png"));
	static ImageIcon dirOpenIcon = new ImageIcon(QFSBuilder.class.getResource("images/tree/dir/dirOpen.png"));
	static ImageIcon dirCloseIcon = new ImageIcon(QFSBuilder.class.getResource("images/tree/dir/dirClose.png"));
	BorderLayout borderLayout1 = new BorderLayout();

	public PFSDefaultTreeCellRenderer() {
		super();
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (hk.quantr.fs.Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
		if (value.getClass() == File.class) {
			File node = (File) value;
			String text = node.getName();
			this.setText(text);

			if (node.isFile()) {
				this.setIcon(fileIcon);
			} else {
				if (expanded) {
					this.setIcon(dirOpenIcon);
				} else {
					this.setIcon(dirCloseIcon);
				}
			}

			if (tree.isEnabled()) {
				if (sel) {
					this.setBackground(new Color(180, 226, 252));
				} else {
					this.setBackground(tree.getBackground());
				}
			} else {
				this.setBackground(tree.getBackground());
			}
			return this;
		} else {
			return new JLabel(value.toString());
		}
	}

	private void jbInit() throws Exception {
		this.setOpaque(true);
	}
}
