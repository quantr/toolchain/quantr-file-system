package hk.quantr.fs.Dialogs;

import hk.quantr.fs.Global;
import hk.quantr.fs.PExceptionDialog;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JToolBar;


/**
 * <p>
 * Title: PFS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Petersoft
 * </p>
 *
 * @author Peter
 * @version 2005
 */
public class BlockDialogPanel extends JPanel {
	JToolBar JToolBar1 = new JToolBar();
	JCheckBox pHexCheckBox = new JCheckBox();
	JCheckBox pDecimalCheckBox = new JCheckBox();
	JCheckBox pOctalCheckBox = new JCheckBox();
	JCheckBox pBinaryCheckBox = new JCheckBox();
	JCheckBox pCharacterCheckBox = new JCheckBox();
	JPanel JPanel2 = new JPanel();
	JButton pCloseButton = new JButton();
	JSplitPane JSplitPane1 = new JSplitPane();
	JScrollPane JScrollPane2 = new JScrollPane();
	JTable JTable1 = new JTable();
	JScrollPane JScrollPane3 = new JScrollPane();
	JTextArea jMainTextArea = new JTextArea();
	BorderLayout borderLayout1 = new BorderLayout();
	BlockDialog_TableModel jBlockDialog_TableModel = new BlockDialog_TableModel();
	BlockDialog parent;
	long blockNo;

	public BlockDialogPanel() {
		super();
		try {
			jbInit();
			//this.add(JScrollPane2, java.awt.BorderLayout.CENTER);
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public BlockDialogPanel(BlockDialog parent, JPanel JPanel, long blockNo) {
		super();
		try {
			this.parent = parent;
			this.blockNo = blockNo;
			jbInit();
			JSplitPane1.add(JPanel, JSplitPane.BOTTOM);
			jBlockDialog_TableModel.setValue(blockNo);
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void jbInit() throws Exception {
		pHexCheckBox.setSelected(true);
		pHexCheckBox.setText("H");
		pHexCheckBox.addActionListener(new JBlockDialogPanel_pHexCheckBox_actionAdapter(this));
		pDecimalCheckBox.setText("D");
		pDecimalCheckBox.addActionListener(new JBlockDialogPanel_pDecimalCheckBox_actionAdapter(this));
		pOctalCheckBox.setText("O");
		pOctalCheckBox.addActionListener(new JBlockDialogPanel_pOctalCheckBox_actionAdapter(this));
		pBinaryCheckBox.setText("B");
		pBinaryCheckBox.addActionListener(new JBlockDialogPanel_pBinaryCheckBox_actionAdapter(this));
		pCharacterCheckBox.setText("C");
		pCharacterCheckBox.addActionListener(new JBlockDialogPanel_pCharacterCheckBox_actionAdapter(this));
		pCloseButton.setText("Close");
		pCloseButton.addActionListener(new JBlockDialogPanel_pCloseButton_actionAdapter(this));
		this.setLayout(borderLayout1);
		JSplitPane1.setOrientation(JSplitPane.VERTICAL_SPLIT);
		JTable1.setModel(jBlockDialog_TableModel);
		JTable1.addMouseListener(new JBlockDialogPanel_JTable1_mouseAdapter(this));
		JToolBar1.add(pHexCheckBox);
		JToolBar1.add(pDecimalCheckBox);
		JToolBar1.add(pOctalCheckBox);
		JToolBar1.add(pBinaryCheckBox);
		JToolBar1.add(pCharacterCheckBox);
		JSplitPane1.add(JScrollPane2, JSplitPane.TOP);
		JSplitPane1.add(JScrollPane3, JSplitPane.BOTTOM);
		JPanel2.add(pCloseButton);
		JScrollPane3.getViewport().add(jMainTextArea);
		JScrollPane2.getViewport().add(JTable1);

		this.add(JPanel2, java.awt.BorderLayout.SOUTH);
		this.add(JToolBar1, java.awt.BorderLayout.NORTH);
		this.add(JSplitPane1, java.awt.BorderLayout.CENTER);
		JSplitPane1.setDividerLocation(150);

		JTable1.setDefaultEditor(Object.class, new BlockDialog_TableCellEditor());
	}

	public void pCloseButton_actionPerformed(ActionEvent e) {
		((JTabbedPane) this.getParent()).remove(this);
	}

	public void pHexCheckBox_actionPerformed(ActionEvent e) {
		jBlockDialog_TableModel.setShowHex(pHexCheckBox.isSelected());
	}

	public void pDecimalCheckBox_actionPerformed(ActionEvent e) {
		jBlockDialog_TableModel.setShowDecimal(pDecimalCheckBox.isSelected());
	}

	public void pOctalCheckBox_actionPerformed(ActionEvent e) {
		jBlockDialog_TableModel.setShowOctal(pOctalCheckBox.isSelected());
	}

	public void pBinaryCheckBox_actionPerformed(ActionEvent e) {
		jBlockDialog_TableModel.setShowBinary(pBinaryCheckBox.isSelected());
	}

	public void pCharacterCheckBox_actionPerformed(ActionEvent e) {
		jBlockDialog_TableModel.setShowCharacter(pCharacterCheckBox.isSelected());
	}

	public void JTable1_mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2 && JTable1.getSelectedColumn() > 0) {
			new EditByteDialog(parent, blockNo, (byte) jBlockDialog_TableModel.getDecValueAt(JTable1.getSelectedRow(), JTable1.getSelectedColumn())).setVisible(true);
			jBlockDialog_TableModel.setValue(blockNo);
		}
	}
}

class JBlockDialogPanel_JTable1_mouseAdapter extends MouseAdapter {
	private BlockDialogPanel adaptee;

	JBlockDialogPanel_JTable1_mouseAdapter(BlockDialogPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void mouseClicked(MouseEvent e) {
		adaptee.JTable1_mouseClicked(e);
	}
}

class JBlockDialogPanel_pCharacterCheckBox_actionAdapter implements ActionListener {
	private BlockDialogPanel adaptee;

	JBlockDialogPanel_pCharacterCheckBox_actionAdapter(BlockDialogPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pCharacterCheckBox_actionPerformed(e);
	}
}

class JBlockDialogPanel_pBinaryCheckBox_actionAdapter implements ActionListener {
	private BlockDialogPanel adaptee;

	JBlockDialogPanel_pBinaryCheckBox_actionAdapter(BlockDialogPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pBinaryCheckBox_actionPerformed(e);
	}
}

class JBlockDialogPanel_pOctalCheckBox_actionAdapter implements ActionListener {
	private BlockDialogPanel adaptee;

	JBlockDialogPanel_pOctalCheckBox_actionAdapter(BlockDialogPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pOctalCheckBox_actionPerformed(e);
	}
}

class JBlockDialogPanel_pDecimalCheckBox_actionAdapter implements ActionListener {
	private BlockDialogPanel adaptee;

	JBlockDialogPanel_pDecimalCheckBox_actionAdapter(BlockDialogPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pDecimalCheckBox_actionPerformed(e);
	}
}

class JBlockDialogPanel_pHexCheckBox_actionAdapter implements ActionListener {
	private BlockDialogPanel adaptee;

	JBlockDialogPanel_pHexCheckBox_actionAdapter(BlockDialogPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pHexCheckBox_actionPerformed(e);
	}
}

class JBlockDialogPanel_pCloseButton_actionAdapter implements ActionListener {
	private BlockDialogPanel adaptee;

	JBlockDialogPanel_pCloseButton_actionAdapter(BlockDialogPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pCloseButton_actionPerformed(e);
	}
}
