package hk.quantr.fs.Dialogs;

import hk.quantr.fs.Global;
import hk.quantr.fs.PExceptionDialog;
import hk.quantr.fs.PIni;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class SearchPartitionOffsetDialog extends JDialog implements Runnable {
	JPanel panel1 = new JPanel();
	JLabel jOrLabel = new JLabel();
	JTextField jPartitionOffsetTextField = new JTextField();
	JPanel pSearchPanel = new JPanel();
	JPanel JTextFieldPanel = new JPanel();
	BorderLayout borderLayout1 = new BorderLayout(10, 10);
	JButton pOKButton = new JButton();
	JComboBox pOffsetComboBox = new JComboBox();
	BorderLayout borderLayout2 = new BorderLayout(10, 10);
	JButton pSearchButton = new JButton();
	JLabel jLabel2 = new JLabel();
	JProgressBar JProgressBar1 = new JProgressBar();
	JPanel pControlPanel = new JPanel();
	File file;
	JRadioButton JRadioButton1 = new JRadioButton();
	JRadioButton JRadioButton2 = new JRadioButton();
	ButtonGroup buttonGroup1 = new ButtonGroup();
	PIni ini = new PIni("Setting.ini");
	boolean cancel = true;

	public SearchPartitionOffsetDialog(Frame owner, File file) {
		super(owner, "Search Offset", true);
		this.file = file;
		try {
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			jbInit();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	private void jbInit() throws Exception {
		jOrLabel.setFont(new java.awt.Font("Tahoma", Font.PLAIN, 16));
		jOrLabel.setHorizontalAlignment(SwingConstants.CENTER);
		jOrLabel.setText("or");
		JTextFieldPanel.setLayout(borderLayout1);
		pOKButton.setText("OK");
		pOKButton.addActionListener(new PSearchPartitionOffsetDialog_pOKButton_actionAdapter(this));
		pSearchPanel.setLayout(borderLayout2);
		pSearchButton.setEnabled(false);
		pSearchButton.setText("Search");
		pSearchButton.addActionListener(new PSearchPartitionOffsetDialog_JButton1_actionAdapter(this));
		jLabel2.setFont(new java.awt.Font("Tahoma", Font.PLAIN, 16));
		jLabel2.setHorizontalAlignment(SwingConstants.CENTER);
		jLabel2.setText("Please input partition offset ?");
		jPartitionOffsetTextField.setText("0");
		jPartitionOffsetTextField.addMouseListener(new PSearchPartitionOffsetDialog_jPartitionOffsetTextField_mouseAdapter(this));
		JRadioButton1.setSelected(true);
		JRadioButton1.addActionListener(new PSearchPartitionOffsetDialog_JRadioButton1_actionAdapter(this));
		pOffsetComboBox.setEnabled(false);
		JRadioButton2.addActionListener(new PSearchPartitionOffsetDialog_JRadioButton2_actionAdapter(this));
		this.addWindowListener(new PSearchPartitionOffsetDialog_this_windowAdapter(this));
		BoxLayout panel1Layout = new BoxLayout(panel1, javax.swing.BoxLayout.Y_AXIS);
		panel1.setLayout(panel1Layout);
		getContentPane().add(panel1);
		panel1.add(jLabel2);
		panel1.add(JTextFieldPanel);
		panel1.add(jOrLabel);
		panel1.add(pSearchPanel);
		JTextFieldPanel.add(jPartitionOffsetTextField, java.awt.BorderLayout.CENTER);
		panel1.add(pControlPanel);
		pControlPanel.add(pOKButton);
		pSearchPanel.add(pOffsetComboBox, java.awt.BorderLayout.CENTER);
		pSearchPanel.add(pSearchButton, java.awt.BorderLayout.EAST);
		pSearchPanel.add(JProgressBar1, java.awt.BorderLayout.SOUTH);
		pSearchPanel.add(JRadioButton2, java.awt.BorderLayout.WEST);
		JTextFieldPanel.add(JRadioButton1, java.awt.BorderLayout.WEST);
		JProgressBar1.setVisible(false);
		buttonGroup1.add(JRadioButton1);
		buttonGroup1.add(JRadioButton2);

		String iniOffset = ini.getStringProperty("File Offsets", file.getAbsolutePath());
		if (iniOffset != null) {
			jPartitionOffsetTextField.setText(iniOffset);
		}
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setSize(400, 210);
		centerDialog();
	}

	void centerDialog() {
		// Center the window
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension frameSize = this.getSize();

		if (frameSize.height > screenSize.height) {
			frameSize.height = screenSize.height;
		}

		if (frameSize.width > screenSize.width) {
			frameSize.width = screenSize.width;
		}

		this.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);

	}

	public void pSearchButton_actionPerformed(ActionEvent e) {
		if (pSearchButton.getText().equals("Search")) {
			pSearchButton.setText("Cancel");
			// ////////////////////////////////////////////////
			jPartitionOffsetTextField.setEnabled(false);
			JProgressBar1.setValue(0);
			JProgressBar1.setMaximum((int) file.length() - 1);
			JProgressBar1.setVisible(true);
			pOKButton.setEnabled(false);
			this.pOffsetComboBox.removeAllItems();
			// /////////////////////////////////////////////////
			new Thread(this).start();
		} else {
			pSearchButton.setText("Search");
			// /////////////////////////////////////////////////
			JProgressBar1.setValue(0);
			JProgressBar1.setVisible(false);
			pOKButton.setEnabled(true);
			jPartitionOffsetTextField.setEnabled(true);
			// /////////////////////////////////////////////////
			this.setVisible(false);
		}
	}

	public void run() {
		try {
			FileInputStream br = new FileInputStream(file);
			int bufferSize = 10 * 1024;
			byte buffer[] = new byte[bufferSize];
			for (int x = 0; x < file.length(); x += bufferSize) {
				if (pSearchButton.getText().equals("Search")) {
					return;
				} else {
					int bytesRead = br.read(buffer);
					for (int y = 0; y < bytesRead - 1; y++) {
						int offset = x + y;
						if (buffer[y] == 'S' && buffer[y + 1] == 'B') {
							if ((offset % (1024 * 1024)) == 0) {
								this.pOffsetComboBox.addItem(String.valueOf(offset) + ": " + offset / 1024 / 1024 + " MB");
							} else if (offset % 1024 == 0) {
								this.pOffsetComboBox.addItem(String.valueOf(offset) + ": " + offset / 1024 + " KB");
							} else {
								this.pOffsetComboBox.addItem(String.valueOf(offset));
							}
						}
						// JProgressBar1.setValue(JProgressBar1.getValue() + 1);
					}
					JProgressBar1.setValue(JProgressBar1.getValue() + bytesRead);
				}
			}

			if (pOffsetComboBox.getItemCount() == 0) {
				pOKButton.setEnabled(false);
			} else {
				pOKButton.setEnabled(true);
			}

			pSearchButton.setText("Search");
			JProgressBar1.setVisible(false);
			br.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public void JRadioButton1_actionPerformed(ActionEvent e) {
		pOffsetComboBox.setEnabled(false);
		pSearchButton.setEnabled(false);
		jPartitionOffsetTextField.setEnabled(true);
		pOKButton.setEnabled(true);
	}

	public void JRadioButton2_actionPerformed(ActionEvent e) {
		pOffsetComboBox.setEnabled(true);
		pSearchButton.setEnabled(true);
		jPartitionOffsetTextField.setEnabled(false);

		if (pOffsetComboBox.getItemCount() == 0) {
			pOKButton.setEnabled(false);
		} else {
			pOKButton.setEnabled(true);
		}
	}

	public void pOKButton_actionPerformed(ActionEvent e) {
		if (JRadioButton1.isSelected()) {
			ini.setStringProperty("File Offsets", file.getAbsolutePath(), jPartitionOffsetTextField.getText(), null);
		} else {
			ini.setStringProperty("File Offsets", file.getAbsolutePath(), pOffsetComboBox.getSelectedItem().toString(), null);
		}
		ini.save();
		cancel = false;
		this.setVisible(false);
	}

	public void jPartitionOffsetTextField_mouseClicked(MouseEvent e) {
		jPartitionOffsetTextField.setSelectionStart(0);
		jPartitionOffsetTextField.setSelectionEnd(jPartitionOffsetTextField.getText().length());
	}

	public boolean isCancel() {
		return cancel;
	}

	public void this_windowClosed(WindowEvent e) {
		cancel = true;
	}
}

class PSearchPartitionOffsetDialog_this_windowAdapter extends WindowAdapter {
	private SearchPartitionOffsetDialog adaptee;

	PSearchPartitionOffsetDialog_this_windowAdapter(SearchPartitionOffsetDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void windowClosed(WindowEvent e) {
		adaptee.this_windowClosed(e);
	}
}

class PSearchPartitionOffsetDialog_jPartitionOffsetTextField_mouseAdapter extends MouseAdapter {
	private SearchPartitionOffsetDialog adaptee;

	PSearchPartitionOffsetDialog_jPartitionOffsetTextField_mouseAdapter(SearchPartitionOffsetDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void mouseClicked(MouseEvent e) {
		adaptee.jPartitionOffsetTextField_mouseClicked(e);
	}
}

class PSearchPartitionOffsetDialog_pOKButton_actionAdapter implements ActionListener {
	private SearchPartitionOffsetDialog adaptee;

	PSearchPartitionOffsetDialog_pOKButton_actionAdapter(SearchPartitionOffsetDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pOKButton_actionPerformed(e);
	}
}

class PSearchPartitionOffsetDialog_JRadioButton2_actionAdapter implements ActionListener {
	private SearchPartitionOffsetDialog adaptee;

	PSearchPartitionOffsetDialog_JRadioButton2_actionAdapter(SearchPartitionOffsetDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.JRadioButton2_actionPerformed(e);
	}
}

class PSearchPartitionOffsetDialog_JRadioButton1_actionAdapter implements ActionListener {
	private SearchPartitionOffsetDialog adaptee;

	PSearchPartitionOffsetDialog_JRadioButton1_actionAdapter(SearchPartitionOffsetDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.JRadioButton1_actionPerformed(e);
	}
}

class PSearchPartitionOffsetDialog_JButton1_actionAdapter implements ActionListener {
	private SearchPartitionOffsetDialog adaptee;

	PSearchPartitionOffsetDialog_JButton1_actionAdapter(SearchPartitionOffsetDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pSearchButton_actionPerformed(e);
	}
}
