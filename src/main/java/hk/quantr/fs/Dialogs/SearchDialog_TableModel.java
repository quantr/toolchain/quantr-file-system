package hk.quantr.fs.Dialogs;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */

public class SearchDialog_TableModel extends AbstractTableModel {
	private String[] columnNames = { "Block ID", "Type", "Name" };
	private Vector<Long> blockIDs = new Vector();
	private Vector<String> types = new Vector();
	private Vector<String> names = new Vector();

	public SearchDialog_TableModel() {
		super();
	}

	public Object getValueAt(int row, int column) {
		if (column == 0) {
			return blockIDs.get(row);
		} else if (column == 1) {
			return types.get(row);
		} else if (column == 2) {
			return names.get(row);
		}
		return "";
	}

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return blockIDs.size();
	}

	public void add(long blockID, String type, String name) {
		if (!blockIDs.contains(new Long(blockID))) {
			blockIDs.add(new Long(blockID));
			types.add(type);
			names.add(name);
			this.fireTableDataChanged();
		}
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public boolean isCellEditable(int row, int col) {
		return false;
	}

	public Class getColumnClass(int columnIndex) {
		return String.class;
	}

	public void clear() {
		blockIDs.clear();
		types.clear();
		names.clear();
		this.fireTableDataChanged();
	}
}
