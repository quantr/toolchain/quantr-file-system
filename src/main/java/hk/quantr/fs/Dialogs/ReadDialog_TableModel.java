package hk.quantr.fs.Dialogs;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

/**
 * <p>
 * Title: PFS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Petersoft
 * </p>
 *
 * @author Peter
 * @version 2005
 */
public class ReadDialog_TableModel extends AbstractTableModel {
	private String[] columnNames = { "Progress", "Status" };
	private Vector<String> content = new Vector<String>();
	private Vector<String> errorContent = new Vector<String>();
	private boolean showErrorOnly;

	public ReadDialog_TableModel() {
		super();
	}

	public Object getValueAt(int row, int column) {
		if (showErrorOnly) {
			return errorContent.get(getColumnCount() * row + column);
		} else {
			return content.get(getColumnCount() * row + column);
		}
	}

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getNormalRowCount() {
		return content.size() / getColumnCount();
	}

	public int getRowCount() {
		if (showErrorOnly) {
			return errorContent.size() / getColumnCount();
		} else {
			return content.size() / getColumnCount();
		}
	}

	public void add(String progress, String status) {
		content.add(progress);
		content.add(status);
		this.fireTableDataChanged();
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public boolean isCellEditable(int row, int col) {
		return false;
	}

	public Class getColumnClass(int columnIndex) {
		return String.class;
	}

	public void setShowErrorOnly(boolean showErrorOnly) {
		this.showErrorOnly = showErrorOnly;
		this.fireTableDataChanged();
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		try {
			if (columnIndex == 1 && aValue.equals("fail")) {
				errorContent.add(content.get(getColumnCount() * rowIndex));
				errorContent.add(content.get(getColumnCount() * rowIndex + 1));
			}
			content.setElementAt((String) aValue, rowIndex * getColumnCount() + columnIndex);
			fireTableDataChanged();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
