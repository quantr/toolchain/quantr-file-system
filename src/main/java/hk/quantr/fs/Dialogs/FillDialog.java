package hk.quantr.fs.Dialogs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
/**
 * <p>
 * Title: PFS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Petersoft
 * </p>
 *
 * @author Peter
 * @version 2005
 */
public class FillDialog extends JDialog {
	JPanel pMainPanel = new JPanel();
	JPanel pFromCharPanel = new JPanel();
	JPanel pNumberOfBytePanel = new JPanel();
	JLabel pNoOfByteLabel = new JLabel();
	BorderLayout borderLayout1 = new BorderLayout();
	JTextField pNoOfByteTextField = new JTextField();
	JTextField pFromCharTextField = new JTextField();
	JLabel jToLabel = new JLabel();
	JTextField pToCharTextField = new JTextField();
	JPanel pControlPanel = new JPanel();
	JButton pCancelButton = new JButton();
	JButton pOKButton = new JButton();
	boolean cancel = true;
	JPanel JPanel4 = new JPanel();
	BorderLayout borderLayout2 = new BorderLayout();
	GridLayout gridLayout1 = new GridLayout();
	JRadioButton pFromCarRadioButton = new JRadioButton();
	JPanel pFromAsciiPanel = new JPanel();
	JRadioButton pFromAsciiRadioButton = new JRadioButton();
	JPanel JPanel6 = new JPanel();
	JTextField pToAsciiTextField = new JTextField();
	JLabel jLabel1 = new JLabel();
	JTextField pFromAsciiTextField = new JTextField();
	BorderLayout borderLayout3 = new BorderLayout();
	GridLayout gridLayout2 = new GridLayout();
	ButtonGroup buttonGroup1 = new ButtonGroup();
	JPanel JPanel1 = new JPanel();
	JRadioButton pMBRadioButton = new JRadioButton();
	JRadioButton pByteRadioButton = new JRadioButton();
	ButtonGroup buttonGroup2 = new ButtonGroup();
	JRadioButton pKBRadioButton = new JRadioButton();

	public FillDialog() {
		this(new Frame(), "PFillDialog", false);
	}

	public FillDialog(Frame owner, String title, boolean modal) {
		super(owner, title, modal);
		try {
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			jbInit();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	private void jbInit() throws Exception {
		pNoOfByteLabel.setPreferredSize(new Dimension(95, 18));
		pNoOfByteLabel.setText("Count : ");
		pNumberOfBytePanel.setLayout(borderLayout1);
		pFromCharPanel.setLayout(borderLayout2);
		pNoOfByteTextField.setText("1");
		jToLabel.setHorizontalAlignment(SwingConstants.CENTER);
		jToLabel.setText("To");
		pCancelButton.setText("Cancel");
		pCancelButton.addActionListener(new PFillDialog_pCancelButton_actionAdapter(this));
		pOKButton.setText("OK");
		pOKButton.addActionListener(new PFillDialog_pOKButton_actionAdapter(this));
		pFromCharTextField.setText("0");
		pToCharTextField.setText("9");
		JPanel4.setLayout(gridLayout1);
		pFromCarRadioButton.setPreferredSize(new Dimension(95, 18));
		pFromCarRadioButton.setSelected(true);
		pFromCarRadioButton.setText("From char");
		pFromAsciiRadioButton.setPreferredSize(new Dimension(95, 18));
		pFromAsciiRadioButton.setText("From ascii");
		pToAsciiTextField.setText("57");
		jLabel1.setHorizontalAlignment(SwingConstants.CENTER);
		jLabel1.setText("To");
		pFromAsciiTextField.setText("48");
		pFromAsciiPanel.setLayout(borderLayout3);
		JPanel6.setLayout(gridLayout2);
		pMBRadioButton.setText("MB");
		pByteRadioButton.setSelected(true);
		pByteRadioButton.setText("byte");
		pKBRadioButton.setText("KB");
		pNumberOfBytePanel.setPreferredSize(new Dimension(263, 25));
		BoxLayout pMainPanelLayout = new BoxLayout(pMainPanel, javax.swing.BoxLayout.Y_AXIS);
		pMainPanel.setLayout(pMainPanelLayout);
		getContentPane().add(pMainPanel);
		pControlPanel.add(pOKButton);
		pControlPanel.add(pOKButton);
		pControlPanel.add(pCancelButton);

		JPanel6.add(pFromAsciiTextField);
		JPanel6.add(jLabel1);
		JPanel6.add(pToAsciiTextField);
		pMainPanel.add(pNumberOfBytePanel);
		pMainPanel.add(pFromCharPanel);
		pMainPanel.add(pFromAsciiPanel);
		pMainPanel.add(pControlPanel);
		JPanel4.add(pFromCharTextField);
		JPanel4.add(jToLabel);
		JPanel4.add(pToCharTextField);
		pNumberOfBytePanel.add(pNoOfByteTextField, java.awt.BorderLayout.CENTER);
		pNumberOfBytePanel.add(pNoOfByteLabel, java.awt.BorderLayout.WEST);
		pFromCharPanel.add(JPanel4, java.awt.BorderLayout.CENTER);
		pFromCharPanel.add(pFromCarRadioButton, java.awt.BorderLayout.WEST);
		pFromAsciiPanel.add(pFromAsciiRadioButton, java.awt.BorderLayout.WEST);
		pFromAsciiPanel.add(JPanel6, java.awt.BorderLayout.CENTER);
		buttonGroup1.add(pFromCarRadioButton);
		buttonGroup1.add(pFromAsciiRadioButton);
		buttonGroup1.add(pMBRadioButton);
		pNumberOfBytePanel.add(JPanel1, java.awt.BorderLayout.EAST);
		JPanel1.add(pKBRadioButton);
		JPanel1.add(pByteRadioButton);
		JPanel1.add(pKBRadioButton);
		JPanel1.add(pMBRadioButton);
		this.setSize(400, 180);
		buttonGroup2.add(pByteRadioButton);
		buttonGroup2.add(pMBRadioButton);
		buttonGroup2.add(pKBRadioButton);
	}

	public void pCancelButton_actionPerformed(ActionEvent e) {
		this.cancel = false;
		this.setVisible(false);
	}

	public void pOKButton_actionPerformed(ActionEvent e) {
		if (pMBRadioButton.isSelected()) {
			this.pNoOfByteTextField.setText(String.valueOf(getNumberOfByte() * 1024 * 1024));
		} else if (pKBRadioButton.isSelected()) {
			this.pNoOfByteTextField.setText(String.valueOf(getNumberOfByte() * 1024));
		}

		cancel = false;
		this.setVisible(false);
	}

	public boolean isCancel() {
		return cancel;
	}

	public int getNumberOfByte() {
		try {
			return Integer.parseInt(this.pNoOfByteTextField.getText());
		} catch (Exception ex) {
			return 0;
		}
	}

	public char getFromChar() {
		try {
			return this.pFromCharTextField.getText().charAt(0);
		} catch (Exception ex) {
			return ' ';
		}
	}

	public char getToChar() {
		try {
			return this.pToCharTextField.getText().charAt(0);
		} catch (Exception ex) {
			return ' ';
		}
	}

	public char getFromAscii() {
		try {
			return (char) Long.parseLong(this.pFromAsciiTextField.getText());
		} catch (Exception ex) {
			return ' ';
		}
	}

	public char getToAscii() {
		try {
			return (char) Long.parseLong(this.pToAsciiTextField.getText());
		} catch (Exception ex) {
			return ' ';
		}
	}

	public boolean isChar() {
		return this.pFromCarRadioButton.isSelected();
	}

}

class PFillDialog_pCancelButton_actionAdapter implements ActionListener {
	private FillDialog adaptee;

	PFillDialog_pCancelButton_actionAdapter(FillDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pCancelButton_actionPerformed(e);
	}
}

class PFillDialog_pOKButton_actionAdapter implements ActionListener {
	private FillDialog adaptee;

	PFillDialog_pOKButton_actionAdapter(FillDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pOKButton_actionPerformed(e);
	}
}
