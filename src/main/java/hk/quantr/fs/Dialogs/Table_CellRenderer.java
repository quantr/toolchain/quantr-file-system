package hk.quantr.fs.Dialogs;

import java.awt.Component;
import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class Table_CellRenderer extends JLabel implements TableCellRenderer {
	ImageIcon errorImage = new ImageIcon(getClass().getClassLoader().getResource("com/pfsbuilder/images/PTable_CellRenderer/error.png"));

	String value;

	public Table_CellRenderer() {
	}

	public void paint(Graphics g) {
		if (value.startsWith("Error")) {
			//super.paint(g);
			//			g.drawImage(errorImage, 0, 2, null);
			//			int stringHeight = getFont().getSize();
			//			g.drawString(value, 20, getHeight() - ((getHeight() - stringHeight) / 2) - 2);
			this.setIcon(errorImage);
		} else {
			this.setIcon(null);
			super.paint(g);
		}
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		this.value = (String) value;
		return this;
	}
}
