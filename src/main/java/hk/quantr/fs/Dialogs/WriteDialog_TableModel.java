package hk.quantr.fs.Dialogs;

import java.util.Vector;

import javax.swing.table.AbstractTableModel;

/**
 * <p>
 * Title: PFS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Petersoft
 * </p>
 *
 * @author Peter
 * @version 2005
 */
public class WriteDialog_TableModel extends AbstractTableModel {
	private String[] columnNames = { "Progress", "Status" };
	private Vector content = new Vector();

	public WriteDialog_TableModel() {
	}

	public Object getValueAt(int row, int column) {
		return content.get(getColumnCount() * row + column);
	}

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return content.size() / 2;
	}

	public void add(String progress, String status) {
		content.add(progress);
		content.add(status);
		this.fireTableDataChanged();
	}

	public void add(String progress, long status) {
		content.add(progress);
		content.add(status);
		this.fireTableDataChanged();
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public boolean isCellEditable(int row, int col) {
		return false;
	}

	public Class getColumnClass(int columnIndex) {
		return String.class;
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		content.setElementAt(aValue, rowIndex * getColumnCount() + columnIndex);
		fireTableDataChanged();
	}

}
