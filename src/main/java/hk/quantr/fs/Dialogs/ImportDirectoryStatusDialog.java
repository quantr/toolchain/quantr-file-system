package hk.quantr.fs.Dialogs;

import hk.quantr.fs.Global;
import hk.quantr.fs.PExceptionDialog;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;



/**
 * <p>
 * Title: PFS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Petersoft
 * </p>
 *
 * @author Peter
 * @version 2005
 */
public class ImportDirectoryStatusDialog extends JDialog implements Runnable {
	BorderLayout borderLayout1 = new BorderLayout();
	JScrollPane JScrollPane1 = new JScrollPane();
	JTable JTable1 = new JTable();
	JPanel pControlPanel = new JPanel();
	JButton pOKButton = new JButton();
	File path;
	File files[];
	DefaultTableModel tableModel = new DefaultTableModel();
	int globalX = 0;

	public ImportDirectoryStatusDialog(Frame owner, String title, boolean modal, File path, File files[]) {
		super(owner, title, modal);
		this.path = path;
		this.files = files;

		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void jbInit() throws Exception {
		this.getContentPane().setLayout(borderLayout1);
		pOKButton.setText("OK");
		pOKButton.addActionListener(new JImportDirectoryStatusDialog_pOKButton_actionAdapter(this));
		this.getContentPane().add(JScrollPane1, java.awt.BorderLayout.CENTER);
		JScrollPane1.getViewport().add(JTable1);
		this.getContentPane().add(pControlPanel, java.awt.BorderLayout.SOUTH);
		pControlPanel.add(pOKButton);
		JTable1.setModel(tableModel);
		tableModel.addColumn("No.");
		tableModel.addColumn("File");
		tableModel.addColumn("Progress");
		JTable1.getColumnModel().getColumn(0).setPreferredWidth(100);
		JTable1.getColumnModel().getColumn(1).setPreferredWidth(600);
		JTable1.getColumnModel().getColumn(2).setPreferredWidth(100);

		this.setSize(800, 600);
		new Thread(this).start();
	}

	public void importDirectory(File path, File file) {
		if (file.isDirectory()) {
			copyDirectory(file, path);
		} else {
			copyFile(file, new File(path + File.separator + file.getName()));
		}
	}

	private void copyFile(File src, File dest) {
		if (pOKButton.getText() == "OK") {
			return;
		}

		try {
			tableModel.insertRow(0, new Object[] { globalX, src.getAbsolutePath(), 0 });

			FileInputStream fi = new FileInputStream(src);
			FileOutputStream fo = new FileOutputStream(dest);
			byte bytes[] = new byte[4096];
			int x = 0;
			int z = 0;
			do {
				z = fi.read(bytes);
				if (z != -1) {
					fo.write(bytes, 0, z);
					x += z;
					tableModel.setValueAt(x * 100 / src.length() + "%", 0, 2);
				}
				// tableModel.setValueAt(x * 100 / src.length() + "%", globalX,
				// 2);
			} while (x < src.length() && z != -1);
			tableModel.setValueAt("100%", 0, 2);
			globalX++;
			fo.close();
			fi.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void copyDirectory(File src, File dest) {
		if (pOKButton.getText() == "OK") {
			return;
		}
		if (src.isDirectory()) {
			new File(dest.getPath() + File.separatorChar + src.getName()).mkdir();
			for (int i = 0; i < src.listFiles().length; i++) {
				if (src.listFiles()[i].isDirectory()) {
					copyDirectory(src.listFiles()[i], new File(dest + File.separator + src.getName()));
				} else {
					copyFile(src.listFiles()[i], new File(dest + File.separator + src.getName() + File.separator + src.listFiles()[i].getName()));
				}
			}
		} else {
			copyFile(src, dest);
		}
	}

	public void run() {
		pOKButton.setText("Stop");
		globalX = 0;
		for (int x = 0; x < files.length; x++) {
			importDirectory(path, files[x]);
		}
		pOKButton.setText("OK");
	}

	public void pOKButton_actionPerformed(ActionEvent e) {
		if (pOKButton.getText() == "OK") {
			this.dispose();
		} else {
			pOKButton.setText("OK");
		}
	}

}

class JImportDirectoryStatusDialog_pOKButton_actionAdapter implements ActionListener {
	private ImportDirectoryStatusDialog adaptee;

	JImportDirectoryStatusDialog_pOKButton_actionAdapter(ImportDirectoryStatusDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pOKButton_actionPerformed(e);
	}
}
