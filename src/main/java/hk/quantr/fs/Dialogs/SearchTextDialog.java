package hk.quantr.fs.Dialogs;

import hk.quantr.fs.Global;
import hk.quantr.fs.PExceptionDialog;
import hk.quantr.fs.PFSCommonLib;
import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class SearchTextDialog extends JDialog {
	JPanel pEastPanel = new JPanel();
	JPanel pCenterPanel = new JPanel();
	JButton pCloseButton = new JButton();
	JButton pCountAllButton = new JButton();
	JButton pFindNextButton = new JButton();
	JPanel pFindWhatPanel = new JPanel();
	JLabel pFindWhatLabel = new JLabel();
	BorderLayout borderLayout1 = new BorderLayout();
	JComboBox JComboBox = new JComboBox();
	JCheckBox pMatchWholeWordOnluCheckBox = new JCheckBox();
	JCheckBox pMatchCaseCheckBox = new JCheckBox();
	JTextArea jTextArea;
	JButton pFindPrevButton = new JButton();

	public SearchTextDialog(JFrame jMainFrame, JTextArea jTextArea) throws HeadlessException {
		super(jMainFrame, "Search", false);
		this.jTextArea = jTextArea;
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void jbInit() throws Exception {
		pCloseButton.setText("Close");
		pCloseButton.addActionListener(new PSearchTextDialog_pCloseButton_actionAdapter(this));
		pCountAllButton.setText("Count all");
		pCountAllButton.addActionListener(new PSearchTextDialog_pCountAllButton_actionAdapter(this));
		pFindNextButton.setText("Find next");
		pFindNextButton.addActionListener(new PSearchTextDialog_pFindNextButton_actionAdapter(this));
		pFindWhatLabel.setText("Find what : ");
		pFindWhatPanel.setLayout(borderLayout1);
		JComboBox.setEditable(true);
		pMatchWholeWordOnluCheckBox.setText("Match whole word only");
		pMatchCaseCheckBox.setText("Match Case");
		pFindPrevButton.setText("Find prev");
		pFindPrevButton.addActionListener(new PSearchTextDialog_pFindPrevButton_actionAdapter(this));
		BoxLayout pCenterPanelLayout = new BoxLayout(pCenterPanel, javax.swing.BoxLayout.Y_AXIS);
		pCenterPanel.setLayout(pCenterPanelLayout);
		this.getContentPane().add(pCenterPanel, java.awt.BorderLayout.CENTER);
		pCenterPanel.add(pFindWhatPanel);
		pCenterPanel.add(pMatchWholeWordOnluCheckBox);
		BoxLayout pEastPanelLayout = new BoxLayout(pEastPanel, javax.swing.BoxLayout.Y_AXIS);
		pEastPanel.setLayout(pEastPanelLayout);
		this.getContentPane().add(pEastPanel, java.awt.BorderLayout.EAST);
		pEastPanel.add(pFindNextButton);
		pEastPanel.add(pFindPrevButton);
		pEastPanel.add(pCountAllButton);
		pEastPanel.add(pCloseButton);
		pFindWhatPanel.add(JComboBox, java.awt.BorderLayout.CENTER);
		pFindWhatPanel.add(pFindWhatLabel, java.awt.BorderLayout.WEST);
		pCenterPanel.add(pMatchCaseCheckBox);
		setSize(400, 160);
		PFSCommonLib.centerDialog(this);
	}

	public void pCloseButton_actionPerformed(ActionEvent e) {
		setVisible(false);
	}

	public void pCountAllButton_actionPerformed(ActionEvent e) {

	}

	public void pFindNextButton_actionPerformed(ActionEvent e) {
		try {
			String comboBoxText = JComboBox.getSelectedItem().toString();
			for (int x = jTextArea.getCaretPosition(); x <= jTextArea.getText().length() - comboBoxText.length(); x++) {
				String compareText = jTextArea.getText().substring(x, x + comboBoxText.length());
				if ((compareText.equals(comboBoxText) && pMatchCaseCheckBox.isSelected())
						|| (compareText.toLowerCase().equals(comboBoxText.toLowerCase()) && !pMatchCaseCheckBox.isSelected())) {
					if (pMatchWholeWordOnluCheckBox.isSelected()) {
						if (x > 0 && !Character.isSpaceChar(new Character(jTextArea.getText().charAt(x - 1)))) {
							continue;
						}
						if (x < jTextArea.getText().length() - comboBoxText.length() - 1
								&& !Character.isSpaceChar(new Character(jTextArea.getText().charAt(x + comboBoxText.length())))) {
							continue;
						}
					}

					jTextArea.setSelectionStart(x);
					jTextArea.setSelectionEnd(x + comboBoxText.length());
					jTextArea.requestFocus();
					this.requestFocus();
					break;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
			setVisible(false);
		}
	}

	public void pFindPrevButton_actionPerformed(ActionEvent e) {
		try {
			for (int x = jTextArea.getSelectionStart() - 1; x >= 0; x--) {
				String comboBoxText = JComboBox.getSelectedItem().toString();
				if (x > jTextArea.getText().length() - comboBoxText.length()) {
					x = jTextArea.getText().length() - comboBoxText.length();
				}
				String compareText = jTextArea.getText().substring(x, x + comboBoxText.length());
				if ((compareText.equals(comboBoxText) && pMatchCaseCheckBox.isSelected())
						|| (compareText.toLowerCase().equals(comboBoxText.toLowerCase()) && !pMatchCaseCheckBox.isSelected())) {
					if (pMatchWholeWordOnluCheckBox.isSelected()) {
						if (x > 0 && !Character.isSpaceChar(new Character(jTextArea.getText().charAt(x - 1)))) {
							continue;
						}
						if (x < jTextArea.getText().length() - 1 && !Character.isSpaceChar(new Character(jTextArea.getText().charAt(x + comboBoxText.length())))) {
							continue;
						}
					}
					jTextArea.setSelectionStart(x);
					jTextArea.setSelectionEnd(x + comboBoxText.length());
					jTextArea.requestFocus();
					this.requestFocus();
					break;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
			setVisible(false);
		}
	}
}

class PSearchTextDialog_pFindPrevButton_actionAdapter implements ActionListener {
	private SearchTextDialog adaptee;

	PSearchTextDialog_pFindPrevButton_actionAdapter(SearchTextDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pFindPrevButton_actionPerformed(e);
	}
}

class PSearchTextDialog_pFindNextButton_actionAdapter implements ActionListener {
	private SearchTextDialog adaptee;

	PSearchTextDialog_pFindNextButton_actionAdapter(SearchTextDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pFindNextButton_actionPerformed(e);
	}
}

class PSearchTextDialog_pCountAllButton_actionAdapter implements ActionListener {
	private SearchTextDialog adaptee;

	PSearchTextDialog_pCountAllButton_actionAdapter(SearchTextDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pCountAllButton_actionPerformed(e);
	}
}

class PSearchTextDialog_pCloseButton_actionAdapter implements ActionListener {
	private SearchTextDialog adaptee;

	PSearchTextDialog_pCloseButton_actionAdapter(SearchTextDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pCloseButton_actionPerformed(e);
	}
}
