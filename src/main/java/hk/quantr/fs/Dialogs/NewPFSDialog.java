package hk.quantr.fs.Dialogs;

import hk.quantr.fs.Global;
import hk.quantr.fs.PExceptionDialog;
import hk.quantr.fs.PFSCommonLib;
import hk.quantr.fs.PFSSettingConstants;
import hk.quantr.fs.PIni;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

public class NewPFSDialog extends JDialog implements Runnable {
	JPanel panel1 = new JPanel();
	BorderLayout borderLayout1 = new BorderLayout();
	JPanel pCHSPanel = new JPanel();
	JPanel pSizePanel = new JPanel();
	JRadioButton pSizeRadioButton1 = new JRadioButton();
	JRadioButton pCHSRadioButton = new JRadioButton();
	ButtonGroup buttonGroup1 = new ButtonGroup();
	JLabel pSLabel = new JLabel();
	JLabel pHLabel = new JLabel();
	JLabel pCLabel = new JLabel();
	JTextField pHTextField = new JTextField();
	JTextField pCTextField = new JTextField();
	JTextField pSTextField = new JTextField();
	JTextField pPartitionSizeTextField = new JTextField();
	JPanel pControlPanel = new JPanel();
	JButton pCancelButton = new JButton();
	JButton pNewButton = new JButton();
	JPanel pFilepathPanel = new JPanel();
	JLabel pFileLabel = new JLabel();
	JButton pBrowseFileButton = new JButton();
	BorderLayout borderLayout3 = new BorderLayout();
	JProgressBar JProgressBar1 = new JProgressBar();

	JPanel pPartitionNamePanel = new JPanel();
	BorderLayout borderLayout4 = new BorderLayout();
	JLabel pPartitionNameLabel = new JLabel();
	JTextField pPartitionNameTextField = new JTextField();
	JPanel pBlockSizePanel = new JPanel();
	JLabel pBlockSizeLabel = new JLabel();
	JComboBox pBlockSizeComboBox = new JComboBox();
	BorderLayout borderLayout5 = new BorderLayout();
	BorderLayout borderLayout6 = new BorderLayout();
	JPanel pVersionPanel = new JPanel();
	BorderLayout borderLayout7 = new BorderLayout();
	JLabel JLabel1 = new JLabel();
	JComboBox pVersionsComboBox = new JComboBox();
	Border border1 = BorderFactory.createEmptyBorder(10, 10, 10, 10);
	JLabel pFilePathLabel = new JLabel();
	JPanel JPanel1 = new JPanel();
	BorderLayout borderLayout8 = new BorderLayout();
	GridLayout gridLayout1 = new GridLayout();
	String outputFilename;
	String projectFiles;
	String blockSize;
	String version;
	String C, H, S;
	String partitionName;
	String partitionSize;
	String partitionOffset;
	String tempDirectory;
	JPanel pMainPanel = new JPanel();
	JPanel pOffsetPanel = new JPanel();
	JLabel pOffsetLabel = new JLabel();
	BorderLayout borderLayout2 = new BorderLayout();
	JTextField pOffsetTextField = new JTextField();
	JPanel pProjectFilePanel = new JPanel();
	BorderLayout borderLayout9 = new BorderLayout();
	JLabel pProjectFileLeftLabel = new JLabel();
	JLabel pProjectFileLabel = new JLabel();
	JButton pBrowseProjectFileButton = new JButton();
	boolean cancel = true;
	JPanel JPanel2 = new JPanel();
	BorderLayout borderLayout10 = new BorderLayout();
	JPanel JPanel3 = new JPanel();
	JRadioButton pSizeMBRadioButton = new JRadioButton();
	JRadioButton pSizeKBRadioButton = new JRadioButton();
	JRadioButton pSizeByteRadioButton = new JRadioButton();
	ButtonGroup sizeButtonGroup = new ButtonGroup();
	JPanel JPanel4 = new JPanel();
	JPanel JPanel5 = new JPanel();
	BorderLayout borderLayout11 = new BorderLayout();
	JRadioButton pOffsetMBRadioButton = new JRadioButton();
	JRadioButton pOffsetKBRadioButton = new JRadioButton();
	JRadioButton pOffsetBytesRadioButton = new JRadioButton();
	ButtonGroup offsetButtonGroup = new ButtonGroup();
	JPanel pTempDirectoryPanel = new JPanel();
	BorderLayout borderLayout12 = new BorderLayout();
	JLabel pTempDirectoryLeftLabel = new JLabel();
	JLabel pTempDirectoryLabel = new JLabel();
	JButton pBrowseTempDirectoryButton = new JButton();
	PIni ini = new PIni("Setting.ini");
	JRadioButton pSizeGBRadioButton = new JRadioButton();
	JRadioButton pOffsetGBRadioButton = new JRadioButton();

	public NewPFSDialog() {
		this(new Frame(), "JNewPFSDialog", false, true);
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public NewPFSDialog(Frame owner, String title, boolean modal, boolean isNew) {
		super(owner, title, modal);
		try {
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			jbInit();
			pPartitionNameTextField.requestFocus();
			if (!isNew) {
				pNewButton.setText("Change");

				updateFields();
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	private void updateFields() {
		this.pPartitionNameTextField.setText(PFSSettingConstants.partitionName);
		this.pProjectFileLabel.setText(PFSSettingConstants.projectFile);
		this.pFileLabel.setText(PFSSettingConstants.filename);
		if (PFSSettingConstants.partitionSize > 1024 * 1024 && PFSSettingConstants.partitionSize % (1024 * 1024) == 0) {
			this.pPartitionSizeTextField.setText(String.valueOf(PFSSettingConstants.partitionSize / 1024 / 1024));
			this.pSizeMBRadioButton.setSelected(true);
		} else if (PFSSettingConstants.partitionSize > 1024 && PFSSettingConstants.partitionSize % 1024 == 0) {
			this.pPartitionSizeTextField.setText(String.valueOf(PFSSettingConstants.partitionSize / 1024));
			this.pSizeKBRadioButton.setSelected(true);
		} else {
			this.pPartitionSizeTextField.setText(String.valueOf(PFSSettingConstants.partitionSize));
			this.pSizeByteRadioButton.setSelected(true);
		}
		if (PFSSettingConstants.partitionOffset > 1024 * 1024 && PFSSettingConstants.partitionOffset % (1024 * 1024) == 0) {
			this.pOffsetTextField.setText(String.valueOf(PFSSettingConstants.partitionOffset / 1024 / 1024));
			this.pOffsetMBRadioButton.setSelected(true);
		} else if (PFSSettingConstants.partitionOffset > 1024 && PFSSettingConstants.partitionOffset % 1024 == 0) {
			this.pOffsetTextField.setText(String.valueOf(PFSSettingConstants.partitionOffset / 1024));
			this.pOffsetKBRadioButton.setSelected(true);
		} else {
			this.pOffsetTextField.setText(String.valueOf(PFSSettingConstants.partitionOffset));
			this.pOffsetBytesRadioButton.setSelected(true);
		}

		this.pCTextField.setText(String.valueOf(PFSSettingConstants.C));
		this.pHTextField.setText(String.valueOf(PFSSettingConstants.H));
		this.pSTextField.setText(String.valueOf(PFSSettingConstants.S));
		this.pTempDirectoryLabel.setText(PFSSettingConstants.tempDirectory);
	}

	private void jbInit() throws Exception {
		panel1.setLayout(borderLayout1);
		pSizeRadioButton1.setPreferredSize(new Dimension(100, 20));
		pSizeRadioButton1.setSelected(true);
		pSizeRadioButton1.setText("Size");
		pSizeRadioButton1.addActionListener(new JNewPFSDialog_pSizeRadioButton1_actionAdapter(this));
		pCHSRadioButton.setPreferredSize(new Dimension(100, 20));
		pCHSRadioButton.setText("CHS");
		pCHSRadioButton.addActionListener(new JNewPFSDialog_pCHSRadioButton_actionAdapter(this));
		pSizePanel.setLayout(borderLayout6);
		pSLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pSLabel.setText("Sector :");
		pHLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pHLabel.setText("Header :");
		pCLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pCLabel.setText("Cylindar :");
		pHTextField.setEnabled(false);
		pHTextField.setText("1024");
		pCTextField.setEnabled(false);
		pCTextField.setToolTipText("");
		pCTextField.setText("2");
		pSTextField.setEnabled(false);
		pSTextField.setText("10");
		pSTextField.addKeyListener(new JNewPFSDialog_pSTextField_keyAdapter(this));
		pCancelButton.setPreferredSize(new Dimension(71, 27));
		pCancelButton.setText("Cancel");
		pCancelButton.addActionListener(new JNewPFSDialog_pCancelButton_actionAdapter(this));
		pNewButton.setPreferredSize(new Dimension(71, 27));
		pNewButton.setText("New");
		pNewButton.addActionListener(new JNewPFSDialog_pNewButton_actionAdapter(this));
		pFilepathPanel.setLayout(borderLayout3);
		pFileLabel.setHorizontalAlignment(SwingConstants.LEFT);
		pFileLabel.setText("File :");
		pBrowseFileButton.setText("...");
		pBrowseFileButton.addActionListener(new JNewPFSDialog_pBrowseFileButton_actionAdapter(this));
		pPartitionSizeTextField.setText("10");
		pPartitionNamePanel.setLayout(borderLayout4);
		pPartitionNameLabel.setPreferredSize(new Dimension(100, 30));
		pPartitionNameLabel.setHorizontalAlignment(SwingConstants.LEFT);
		pPartitionNameLabel.setText("Partition Name : ");
		pPartitionNameTextField.setText("Peter File System");
		pBlockSizePanel.setLayout(borderLayout5);
		pBlockSizeLabel.setPreferredSize(new Dimension(100, 20));
		pBlockSizeLabel.setHorizontalAlignment(SwingConstants.LEFT);
		pBlockSizeLabel.setText("Block Size : ");
		pVersionPanel.setLayout(borderLayout7);
		JLabel1.setPreferredSize(new Dimension(100, 20));
		JLabel1.setHorizontalAlignment(SwingConstants.LEFT);
		JLabel1.setText("Version : ");
		pPartitionNamePanel.setBorder(border1);
		pCHSPanel.setBorder(border1);
		pCHSPanel.setLayout(borderLayout8);
		pSizePanel.setBorder(border1);
		pSizePanel.setPreferredSize(new Dimension(20, 40));
		pBlockSizePanel.setBorder(border1);
		pFilepathPanel.setBorder(border1);
		pVersionPanel.setBorder(border1);
		pFilePathLabel.setPreferredSize(new Dimension(100, 20));
		pFilePathLabel.setHorizontalAlignment(SwingConstants.LEFT);
		pFilePathLabel.setText("File :");
		JPanel1.setLayout(gridLayout1);
		pOffsetLabel.setPreferredSize(new Dimension(100, 20));
		pOffsetLabel.setHorizontalAlignment(SwingConstants.LEFT);
		pOffsetLabel.setText("Offset : ");
		pOffsetPanel.setLayout(borderLayout2);
		pOffsetPanel.setBorder(border1);
		pOffsetPanel.setPreferredSize(new Dimension(20, 40));
		pOffsetTextField.setText("0");
		pProjectFilePanel.setLayout(borderLayout9);
		pProjectFileLeftLabel.setPreferredSize(new Dimension(100, 20));
		pProjectFileLeftLabel.setText("Project file :");
		pProjectFilePanel.setBorder(border1);
		pBrowseProjectFileButton.setText("...");
		pBrowseProjectFileButton.addActionListener(new JNewPFSDialog_pBrowseProjectFileButton_actionAdapter(this));
		pProjectFileLabel.setText(ini.getStringProperty("New project file", "path", new File("").getAbsolutePath() + File.separator + "project.ini"));
		JPanel2.setLayout(borderLayout10);
		pSizeMBRadioButton.setSelected(true);
		pSizeMBRadioButton.setText("MB");
		pSizeKBRadioButton.setText("KB");
		pSizeByteRadioButton.setText("bytes");
		JPanel4.setLayout(borderLayout11);
		pOffsetMBRadioButton.setSelected(true);
		pOffsetMBRadioButton.setText("MB");
		pOffsetKBRadioButton.setText("KB");
		pOffsetBytesRadioButton.setText("bytes");
		pTempDirectoryPanel.setLayout(borderLayout12);
		pTempDirectoryLeftLabel.setPreferredSize(new Dimension(100, 20));
		pTempDirectoryLeftLabel.setText("Temp Directory :");
		pTempDirectoryPanel.setBorder(border1);
		pBrowseTempDirectoryButton.setText("...");
		pBrowseTempDirectoryButton.addActionListener(new JNewPFSDialog_pBrowseTempDirectoryButton_actionAdapter(this));
		pSizeGBRadioButton.setSelected(true);
		pSizeGBRadioButton.setText("GB");
		pOffsetGBRadioButton.setSelected(true);
		pOffsetGBRadioButton.setText("GB");
		getContentPane().add(panel1);
		buttonGroup1.add(pCHSRadioButton);
		buttonGroup1.add(pSizeRadioButton1);
		pControlPanel.add(pNewButton);
		pControlPanel.add(pCancelButton);
		pFilepathPanel.add(pBrowseFileButton, java.awt.BorderLayout.EAST);
		pFilepathPanel.add(pFileLabel, java.awt.BorderLayout.CENTER);
		pFilepathPanel.add(pFilePathLabel, java.awt.BorderLayout.WEST);
		pProjectFilePanel.add(pProjectFileLeftLabel, java.awt.BorderLayout.WEST);
		pProjectFilePanel.add(pProjectFileLabel, java.awt.BorderLayout.CENTER);
		pMainPanel.add(pTempDirectoryPanel);
		pMainPanel.add(pPartitionNamePanel);
		pPartitionNamePanel.add(pPartitionNameLabel, java.awt.BorderLayout.WEST);
		pPartitionNamePanel.add(pPartitionNameTextField, java.awt.BorderLayout.CENTER);
		pMainPanel.add(pCHSPanel);
		pMainPanel.add(pSizePanel);
		pMainPanel.add(pOffsetPanel);
		pBlockSizePanel.add(pBlockSizeComboBox, java.awt.BorderLayout.CENTER);
		pBlockSizePanel.add(pBlockSizeLabel, java.awt.BorderLayout.WEST);
		pMainPanel.add(pFilepathPanel);
		pSizePanel.add(pSizeRadioButton1, java.awt.BorderLayout.WEST);
		pMainPanel.add(pProjectFilePanel);
		BoxLayout pMainPanelLayout = new BoxLayout(pMainPanel, javax.swing.BoxLayout.Y_AXIS);
		pMainPanel.setLayout(pMainPanelLayout);
		pMainPanel.add(pTempDirectoryPanel);
		pMainPanel.add(pBlockSizePanel);

		pVersionPanel.add(JLabel1, java.awt.BorderLayout.WEST);
		pVersionPanel.add(pVersionsComboBox, java.awt.BorderLayout.CENTER);

		pMainPanel.add(JProgressBar1);

		panel1.add(pControlPanel, java.awt.BorderLayout.SOUTH);
		JPanel1.add(pCLabel, null);
		JPanel1.add(pCTextField, null);
		JPanel1.add(pHLabel, null);
		JPanel1.add(pHTextField, null);
		JPanel1.add(pSLabel);
		JPanel1.add(pSTextField, null);
		pMainPanel.add(pVersionPanel);
		pCHSPanel.add(pCHSRadioButton, java.awt.BorderLayout.WEST);
		pCHSPanel.add(JPanel1, java.awt.BorderLayout.CENTER);

		panel1.add(pMainPanel, java.awt.BorderLayout.CENTER);
		pOffsetPanel.add(pOffsetLabel, java.awt.BorderLayout.WEST);
		pProjectFilePanel.add(pBrowseProjectFileButton, java.awt.BorderLayout.EAST);
		pSizePanel.add(JPanel2, java.awt.BorderLayout.CENTER);
		JPanel2.add(JPanel3, java.awt.BorderLayout.EAST);
		JPanel3.add(pSizeByteRadioButton);
		JPanel3.add(pSizeKBRadioButton);
		JPanel3.add(pSizeMBRadioButton);
		JPanel3.add(pSizeGBRadioButton);
		JPanel2.add(pPartitionSizeTextField, java.awt.BorderLayout.CENTER);
		pFileLabel.setText(ini.getStringProperty("New PFS image", "path", new File("").getAbsolutePath() + File.separator + "harddisk"));
		JProgressBar1.setVisible(false);
		pBlockSizeComboBox.addItem("4 KB");
		pVersionsComboBox.addItem("2006");
		sizeButtonGroup.add(pSizeMBRadioButton);
		sizeButtonGroup.add(pSizeKBRadioButton);
		sizeButtonGroup.add(pSizeByteRadioButton);
		sizeButtonGroup.add(pSizeGBRadioButton);
		pOffsetPanel.add(JPanel4, java.awt.BorderLayout.CENTER);
		JPanel4.add(pOffsetTextField, java.awt.BorderLayout.CENTER);
		JPanel4.add(JPanel5, java.awt.BorderLayout.EAST);
		JPanel5.add(pOffsetBytesRadioButton);
		JPanel5.add(pOffsetKBRadioButton);
		JPanel5.add(pOffsetMBRadioButton);
		JPanel5.add(pOffsetGBRadioButton);
		offsetButtonGroup.add(pOffsetMBRadioButton);
		offsetButtonGroup.add(pOffsetKBRadioButton);
		offsetButtonGroup.add(pOffsetBytesRadioButton);
		offsetButtonGroup.add(pOffsetGBRadioButton);
		pTempDirectoryPanel.add(pTempDirectoryLeftLabel, java.awt.BorderLayout.WEST);
		pTempDirectoryPanel.add(pTempDirectoryLabel, java.awt.BorderLayout.CENTER);
		pTempDirectoryPanel.add(pBrowseTempDirectoryButton, java.awt.BorderLayout.EAST);
		pTempDirectoryLabel.setText(ini.getStringProperty("New temp directory", "path", new File("").getAbsolutePath()));
		setSize(600, 500);
		PFSCommonLib.centerDialog(this);
	}

	public void pCancelButton_actionPerformed(ActionEvent e) {
		this.cancel = true;
		this.setVisible(false);
	}

	public void pNewButton_actionPerformed(ActionEvent e) {
		saveVariables();

		ini.setStringProperty("New project file", "path", pProjectFileLabel.getText(), null);
		ini.setStringProperty("New PFS image", "path", pFileLabel.getText(), null);
		ini.setStringProperty("New temp directory", "path", pTempDirectoryLabel.getText(), null);
		ini.save();

		if (pNewButton.isEnabled()) {
			pNewButton.setEnabled(false);
			if (Long.parseLong(partitionSize) % 512 == 0) {
				if (pNewButton.getText().equals("New")) {
					int x = 0;
					if (new File(pFileLabel.getText()).exists()) {
						x = JOptionPane.showConfirmDialog(this, "File : " + pFileLabel.getText() + " exist, override", "Error", JOptionPane.YES_NO_OPTION);
					}
					if (x == 0) {
						new Thread(this).start();
					}
				} else {
					cancel = false;
					this.setVisible(false);
				}
			} else {
				JOptionPane.showMessageDialog(this, "Filesize must be in 512 bytes unit", "Error", JOptionPane.ERROR_MESSAGE);
			}
			pNewButton.setEnabled(true);
		}
	}

	private void saveVariables() {
		try {
			outputFilename = pFileLabel.getText();
			projectFiles = pProjectFileLabel.getText();
			blockSize = this.pBlockSizeComboBox.getSelectedItem().toString();
			version = this.pVersionsComboBox.getSelectedItem().toString();
			C = this.pCTextField.getText();
			H = this.pHTextField.getText();
			S = this.pSTextField.getText();
			partitionName = this.pPartitionNameTextField.getText();
			if (pCHSRadioButton.isSelected()) {
				partitionSize = String.valueOf(Long.parseLong(C) * Long.parseLong(H) * Long.parseLong(S) * 512);
			} else if (pSizeRadioButton1.isSelected()) {
				if (pSizeGBRadioButton.isSelected()) {
					partitionSize = this.pPartitionSizeTextField.getText();
					partitionSize = String.valueOf(Long.parseLong(partitionSize) * 1024 * 1024 * 1024);
				} else if (pSizeMBRadioButton.isSelected()) {
					partitionSize = this.pPartitionSizeTextField.getText();
					partitionSize = String.valueOf(Long.parseLong(partitionSize) * 1024 * 1024);
				} else if (pSizeKBRadioButton.isSelected()) {
					partitionSize = this.pPartitionSizeTextField.getText();
					partitionSize = String.valueOf(Long.parseLong(partitionSize) * 1024);
				} else {
					partitionSize = this.pPartitionSizeTextField.getText();
				}
			}

			if (pOffsetGBRadioButton.isSelected()) {
				partitionOffset = pOffsetTextField.getText();
				partitionOffset = String.valueOf(Long.parseLong(partitionOffset) * 1024 * 1024 * 1024);
			} else if (pOffsetMBRadioButton.isSelected()) {
				partitionOffset = pOffsetTextField.getText();
				partitionOffset = String.valueOf(Long.parseLong(partitionOffset) * 1024 * 1024);
			} else if (pOffsetKBRadioButton.isSelected()) {
				partitionOffset = pOffsetTextField.getText();
				partitionOffset = String.valueOf(Long.parseLong(partitionOffset) * 1024);
			} else {
				partitionOffset = pOffsetTextField.getText();
			}

			tempDirectory = pTempDirectoryLabel.getText();
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public void run() {
		try {
			if (new File(tempDirectory).exists()) {
				int returnValue = JOptionPane.showConfirmDialog(this, tempDirectory + " exist, remove it?", "Question", JOptionPane.YES_NO_OPTION);
				if (returnValue == JOptionPane.YES_OPTION) {
					PFSCommonLib.deleteDirectory(tempDirectory);
				}
			}
			new File(tempDirectory).mkdir();

			if (new File(this.projectFiles).exists()) {
				int returnValue = JOptionPane.showConfirmDialog(this, projectFiles + " exist, remove it?", "Question", JOptionPane.YES_NO_OPTION);
				if (returnValue == JOptionPane.YES_OPTION) {
					new File(this.projectFiles).delete();
				}
			}

			cancel = false;
			this.setVisible(false);
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public void pBrowseFileButton_actionPerformed(ActionEvent e) {
		PIni ini = new PIni("Setting.ini");
		JFileChooser JFileChooser = new JFileChooser();
		File lastSelectedDirectory = new File(ini.getStringProperty("Last opened PFS image", "path", "."));
		JFileChooser.setCurrentDirectory(lastSelectedDirectory);
		JFileChooser.setSize(500, 500);
		PFSCommonLib.centerDialog(JFileChooser);
		JFileChooser.showSaveDialog(this);

		pFileLabel.setText(JFileChooser.getSelectedFile().getAbsolutePath());

		ini.setStringProperty("Last opened PFS image", "path", JFileChooser.getCurrentDirectory().getAbsolutePath(), null);
		ini.save();
	}

	public void pCHSRadioButton_actionPerformed(ActionEvent e) {
		pCTextField.setEnabled(true);
		pHTextField.setEnabled(true);
		pSTextField.setEnabled(true);

		pPartitionSizeTextField.setEnabled(false);
	}

	public void pSizeRadioButton1_actionPerformed(ActionEvent e) {
		pCTextField.setEnabled(false);
		pHTextField.setEnabled(false);
		pSTextField.setEnabled(false);

		pPartitionSizeTextField.setEnabled(true);
	}

	public void pSTextField_keyReleased(KeyEvent e) {
		try {
			pPartitionSizeTextField
					.setText(String.valueOf(Long.parseLong(pCTextField.getText()) * Long.parseLong(pHTextField.getText()) * Long.parseLong(pSTextField.getText()) * 512));

		} catch (Exception ex) {

		}
	}

	public String getOutputFilename() {
		return outputFilename;
	}

	public String getProjectFile() {
		return projectFiles;
	}

	public String getBlockSize() {
		return blockSize;
	}

	public String getPartitionSize() {
		return partitionSize;
	}

	public String getPartitionName() {
		return partitionName;
	}

	public String getVersion() {
		return version;
	}

	public boolean isCancel() {
		return cancel;
	}

	public String getS() {
		return S;
	}

	public String getH() {
		return H;
	}

	public String getC() {
		return C;
	}

	public String getPartitionOffset() {
		return partitionOffset;
	}

	public String getTempDirectory() {
		return tempDirectory;
	}

	public void pBrowseProjectFileButton_actionPerformed(ActionEvent e) {
		PIni ini = new PIni("Setting.ini");
		JFileChooser fileChooser = new JFileChooser();
		File lastSelectedDirectory = new File(ini.getStringProperty("Last opened project file", "path", "."));
		fileChooser.setCurrentDirectory(lastSelectedDirectory);
		fileChooser.setSize(500, 500);
		PFSCommonLib.centerDialog(fileChooser);
		fileChooser.showSaveDialog(this);

		pProjectFileLabel.setText(fileChooser.getSelectedFile().getAbsolutePath());
		ini.setStringProperty("Last opened project file", "path", fileChooser.getCurrentDirectory().getAbsolutePath(), null);
		ini.save();
	}

	public void pBrowseTempDirectoryButton_actionPerformed(ActionEvent e) {
		PIni ini = new PIni("Setting.ini");
		JFileChooser jFileChooser = new JFileChooser();
		File lastSelectedDirectory = new File(ini.getStringProperty("Last opened temp Directory", "path", "."));
		jFileChooser.setCurrentDirectory(lastSelectedDirectory);
		jFileChooser.setSize(500, 500);
		PFSCommonLib.centerDialog(jFileChooser);
		jFileChooser.setFileSelectionMode(jFileChooser.DIRECTORIES_ONLY);
		jFileChooser.setAcceptAllFileFilterUsed(false);

		if (jFileChooser.showOpenDialog(this) == jFileChooser.APPROVE_OPTION) {
			pTempDirectoryLabel.setText(jFileChooser.getSelectedFile().getAbsolutePath());
			ini.setStringProperty("Last opened temp Directory", "path", jFileChooser.getCurrentDirectory().getAbsolutePath(), null);
			ini.save();
		}
	}
}

class JNewPFSDialog_pBrowseTempDirectoryButton_actionAdapter implements ActionListener {
	private NewPFSDialog adaptee;

	JNewPFSDialog_pBrowseTempDirectoryButton_actionAdapter(NewPFSDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pBrowseTempDirectoryButton_actionPerformed(e);
	}
}

class JNewPFSDialog_pBrowseProjectFileButton_actionAdapter implements ActionListener {
	private NewPFSDialog adaptee;

	JNewPFSDialog_pBrowseProjectFileButton_actionAdapter(NewPFSDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pBrowseProjectFileButton_actionPerformed(e);
	}
}

class JNewPFSDialog_pSTextField_keyAdapter extends KeyAdapter {
	private NewPFSDialog adaptee;

	JNewPFSDialog_pSTextField_keyAdapter(NewPFSDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void keyReleased(KeyEvent e) {
		adaptee.pSTextField_keyReleased(e);
	}
}

class JNewPFSDialog_pSizeRadioButton1_actionAdapter implements ActionListener {
	private NewPFSDialog adaptee;

	JNewPFSDialog_pSizeRadioButton1_actionAdapter(NewPFSDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pSizeRadioButton1_actionPerformed(e);
	}
}

class JNewPFSDialog_pCHSRadioButton_actionAdapter implements ActionListener {
	private NewPFSDialog adaptee;

	JNewPFSDialog_pCHSRadioButton_actionAdapter(NewPFSDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pCHSRadioButton_actionPerformed(e);
	}
}

class JNewPFSDialog_pBrowseFileButton_actionAdapter implements ActionListener {
	private NewPFSDialog adaptee;

	JNewPFSDialog_pBrowseFileButton_actionAdapter(NewPFSDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pBrowseFileButton_actionPerformed(e);
	}
}

class JNewPFSDialog_pNewButton_actionAdapter implements ActionListener {
	private NewPFSDialog adaptee;

	JNewPFSDialog_pNewButton_actionAdapter(NewPFSDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pNewButton_actionPerformed(e);
	}
}

class JNewPFSDialog_pCancelButton_actionAdapter implements ActionListener {
	private NewPFSDialog adaptee;

	JNewPFSDialog_pCancelButton_actionAdapter(NewPFSDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pCancelButton_actionPerformed(e);
	}
}
