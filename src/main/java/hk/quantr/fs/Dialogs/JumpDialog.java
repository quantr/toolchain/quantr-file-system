package hk.quantr.fs.Dialogs;

import hk.quantr.fs.PFSCommonLib;
import hk.quantr.fs.PFSImageConstants;
import hk.quantr.fs.PFSTable;
import hk.quantr.fs.PFSTableModel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;


/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
/**
 * <p>
 * Title: PFS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Petersoft
 * </p>
 *
 * @author Peter
 * @version 2005
 */
public class JumpDialog extends JDialog {
	JPanel panel1 = new JPanel();
	JPanel JPanel1 = new JPanel();
	JPanel JPanel2 = new JPanel();
	JRadioButton pOffsetRadioButton = new JRadioButton();
	JRadioButton pBlockNumberRadioButton = new JRadioButton();
	BorderLayout borderLayout1 = new BorderLayout();
	BorderLayout borderLayout2 = new BorderLayout();
	JTextField pOffsetTextField = new JTextField();
	JTextField pBlockNumberTextField = new JTextField();
	JPanel JPanel3 = new JPanel();
	BorderLayout borderLayout3 = new BorderLayout();
	FlowLayout flowLayout1 = new FlowLayout(FlowLayout.RIGHT);
	JButton pJumJButton = new JButton();
	ButtonGroup buttonGroup1 = new ButtonGroup();
	PFSTable table = new PFSTable();

	public JumpDialog(Frame owner, String title, boolean modal, PFSTable table) {
		super(owner, title, modal);
		try {
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			this.table = table;
			jbInit();
			pack();
			PFSCommonLib.centerDialog(this);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public JumpDialog() {
		this(new Frame(), "Jump to Position", false, null);
	}

	public JumpDialog(PFSTable table) {
		this(new Frame(), "Jump to Position", false, table);
	}

	private void jbInit() throws Exception {
		pOffsetRadioButton.setPreferredSize(new Dimension(120, 25));
		pOffsetRadioButton.setSelected(true);
		pOffsetRadioButton.setText("Offset");
		pOffsetRadioButton.addItemListener(new JJumpDialog_pOffsetRadioButton_itemAdapter(this));
		pBlockNumberRadioButton.setPreferredSize(new Dimension(120, 25));
		pBlockNumberRadioButton.setText("Block Number");
		pBlockNumberRadioButton.addItemListener(new JJumpDialog_pBlockNumberRadioButton_itemAdapter(this));
		JPanel2.setLayout(borderLayout1);
		JPanel1.setLayout(borderLayout2);
		pOffsetTextField.setPreferredSize(new Dimension(100, 25));
		pBlockNumberTextField.setEnabled(false);
		pBlockNumberTextField.setPreferredSize(new Dimension(100, 25));
		JPanel3.setLayout(flowLayout1);
		pJumJButton.setText("Jump");
		pJumJButton.addActionListener(new JJumpDialog_jJumJButton_actionAdapter(this));
		BoxLayout panel1Layout = new BoxLayout(panel1, javax.swing.BoxLayout.Y_AXIS);
		panel1.setLayout(panel1Layout);
		getContentPane().add(panel1);
		panel1.add(JPanel2);
		panel1.add(JPanel1);
		JPanel1.add(pBlockNumberTextField, java.awt.BorderLayout.CENTER);
		panel1.add(JPanel3);
		JPanel3.add(pJumJButton);
		JPanel2.add(pOffsetTextField, java.awt.BorderLayout.CENTER);
		JPanel2.add(pOffsetRadioButton, java.awt.BorderLayout.WEST);
		JPanel1.add(pBlockNumberRadioButton, java.awt.BorderLayout.WEST);
		buttonGroup1.add(pOffsetRadioButton);
		buttonGroup1.add(pBlockNumberRadioButton);
	}

	public void jJumJButton_actionPerformed(ActionEvent e) {
		try {
			int columnCount = table.getColumnCount();
			PFSTableModel pFSTableModel = (PFSTableModel) table.getModel();
			long selectRowNumber = -1;
			long selectColumnNumber = -1;

			if (pOffsetRadioButton.isSelected()) {
				if (pFSTableModel.getMode() == 0) {
					selectRowNumber = Long.parseLong(pOffsetTextField.getText()) / (columnCount - 1);
					selectColumnNumber = Long.parseLong(pOffsetTextField.getText()) - selectRowNumber * (columnCount - 1);
				} else {
					long blockNumber = Long.parseLong(pOffsetTextField.getText()) / PFSImageConstants.blockSize;
					selectRowNumber = blockNumber / (columnCount - 1);
					selectColumnNumber = blockNumber - (selectRowNumber * (columnCount - 1));
				}
			} else if (pBlockNumberRadioButton.isSelected()) {
				if (pFSTableModel.getMode() == 0) {
					selectRowNumber = ((Long.parseLong(pBlockNumberTextField.getText()) + 1) * PFSImageConstants.blockSize) / (columnCount - 1);
					selectColumnNumber = (Long.parseLong(pBlockNumberTextField.getText()) * PFSImageConstants.blockSize) - (selectRowNumber * (columnCount - 1));
				} else {
					long blockNumber = Long.parseLong(pBlockNumberTextField.getText()) + 1;
					selectRowNumber = blockNumber / (columnCount - 1);
					selectColumnNumber = blockNumber - (selectRowNumber * (columnCount - 1));
				}
			}
			if (selectRowNumber != -1 && selectColumnNumber != -1) {
				table.selectAndVisibleRow(selectRowNumber, selectColumnNumber + 1);
			}
		} catch (Exception ex) {
		}
	}

	public void pOffsetRadioButton_itemStateChanged(ItemEvent e) {
		pOffsetTextField.setEnabled(pOffsetRadioButton.isSelected());
	}

	public void pBlockNumberRadioButton_itemStateChanged(ItemEvent e) {
		pBlockNumberTextField.setEnabled(pBlockNumberRadioButton.isSelected());
	}
}

class JJumpDialog_pOffsetRadioButton_itemAdapter implements ItemListener {
	private JumpDialog adaptee;

	JJumpDialog_pOffsetRadioButton_itemAdapter(JumpDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void itemStateChanged(ItemEvent e) {
		adaptee.pOffsetRadioButton_itemStateChanged(e);
	}
}

class JJumpDialog_pBlockNumberRadioButton_itemAdapter implements ItemListener {
	private JumpDialog adaptee;

	JJumpDialog_pBlockNumberRadioButton_itemAdapter(JumpDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void itemStateChanged(ItemEvent e) {
		adaptee.pBlockNumberRadioButton_itemStateChanged(e);
	}
}

class JJumpDialog_jJumJButton_actionAdapter implements ActionListener {
	private JumpDialog adaptee;

	JJumpDialog_jJumJButton_actionAdapter(JumpDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {

		adaptee.jJumJButton_actionPerformed(e);
	}
}
