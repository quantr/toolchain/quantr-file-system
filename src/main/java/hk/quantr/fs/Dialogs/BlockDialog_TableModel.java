package hk.quantr.fs.Dialogs;

import hk.quantr.fs.Global;
import java.io.RandomAccessFile;
import javax.swing.table.DefaultTableModel;
import hk.quantr.fs.PExceptionDialog;
import hk.quantr.fs.PFSImageConstants;

/**
 * <p>
 * Title: PFS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Petersoft
 * </p>
 *
 * @author Peter
 * @version 2005
 */
public class BlockDialog_TableModel extends DefaultTableModel {
	private String[] columnNames = { "Offset", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" };
	private byte data[];
	private boolean showHex = true;
	private boolean showDecimal = false;
	private boolean showOctal = false;
	private boolean showBinary = false;
	private boolean showCharacter = false;
	private long blockNo;

	public BlockDialog_TableModel() {
	}

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		if (data == null) {
			return 0;
		} else {
			return data.length / (this.getColumnCount() - 1);
		}
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public boolean isCellEditable(int row, int col) {
		// if (col == 0) {
		// return false;
		// } else {
		// return true;
		// }
		return false;
	}

	public void setValue(long blockNo) {
		try {
			data = new byte[PFSImageConstants.blockSize];
			RandomAccessFile br = new RandomAccessFile(PFSImageConstants.filename, "r");
			br.seek(PFSImageConstants.partitionOffset + PFSImageConstants.superBlockSize + blockNo * PFSImageConstants.blockSize);
			br.read(data);
			br.close();

			this.blockNo = blockNo;
			this.fireTableDataChanged();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public void setValueAt(Object aValue, int row, int column) {
		try {
			data[row * (getColumnCount() - 1) + column - 1] = Byte.parseByte(aValue.toString());

			RandomAccessFile bw = new RandomAccessFile(PFSImageConstants.filename, "rw");
			bw.seek(PFSImageConstants.partitionOffset + PFSImageConstants.superBlockSize + blockNo * PFSImageConstants.blockSize);
			bw.close();

			fireTableCellUpdated(row, column);
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public Object getValueAt(int row, int column) {
		if (column == 0) {
			return row * 16;
		} else {
			byte c = data[(row * (getColumnCount() - 1)) + column - 1];
			String returnValue = "";
			if (showHex) {
				returnValue += Long.toHexString(c & 0xff);
			}
			if (showDecimal) {
				returnValue += "/" + (c & 0xff);
			}
			if (showOctal) {
				returnValue += "/" + (Long.toOctalString(c & 0xff));
			}
			if (showBinary) {
				returnValue += "/" + (Long.toBinaryString(c & 0xff));
			}
			if (showCharacter) {
				returnValue += "/" + ((char) c);
			}
			if (returnValue.startsWith("/")) {
				returnValue = returnValue.substring(1);
			}

			return returnValue;
		}
	}

	public byte getDecValueAt(int row, int column) {
		if (column == 0) {
			return -1;
		} else {
			byte c = data[(row * (getColumnCount() - 1)) + column - 1];
			return c;
		}
	}

	public void setShowBinary(boolean showBinary) {
		this.showBinary = showBinary;
		this.fireTableDataChanged();
	}

	public void setShowCharacter(boolean showCharacter) {
		this.showCharacter = showCharacter;
		this.fireTableDataChanged();
	}

	public void setShowDecimal(boolean showDecimal) {
		this.showDecimal = showDecimal;
		this.fireTableDataChanged();
	}

	public void setShowHex(boolean showHex) {
		this.showHex = showHex;
		this.fireTableDataChanged();
	}

	public void setShowOctal(boolean showOctal) {
		this.showOctal = showOctal;
		this.fireTableDataChanged();
	}
}
