package hk.quantr.fs.Dialogs;

import hk.quantr.fs.QFSBuilder;
import java.awt.Color;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;

/**
 * <p>
 * Title: PFS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Petersoft
 * </p>
 *
 * @author Peter
 * @version 2005
 */
public class ReadDialog_TableCellRenderer extends JLabel implements TableCellRenderer {

	ImageIcon dirIcon = new ImageIcon(QFSBuilder.class.getResource("images/PFSTable/DIR.png"));
	ImageIcon fileIcon = new ImageIcon(QFSBuilder.class.getResource("images/PFSTable/FILE.png"));
	
	public ReadDialog_TableCellRenderer() {
		super();
		// this.setIsIconZoom(true);
		this.setBorder(new EmptyBorder(5, 5, 5, 5));
		this.setHorizontalAlignment(SwingConstants.LEFT);
	}
	
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (column == 0) {
			if (value.toString().startsWith("File")) {
				this.setIcon(fileIcon);
				this.setText(value.toString().substring("File : ".length()));
			} else if (value.toString().startsWith("Directory : ")) {
				this.setIcon(dirIcon);
				this.setText(value.toString().substring("Directory : ".length()));
			} else {
				this.setText(value.toString());
			}
		} else {
			this.setText(value.toString());
			setIcon(null);
		}
		if (isSelected) {
			this.setBackground(new Color(210, 228, 255));
		} else {
			this.setBackground(Color.white);
		}
		
		return this;
	}
}
