package hk.quantr.Dialogs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import hk.quantr.fs.Dialogs.BlockDialog;
import hk.quantr.fs.Dialogs.SearchDialog_TableModel;
import hk.quantr.fs.Global;
import hk.quantr.fs.PExceptionDialog;
import hk.quantr.fs.PFSCommonLib;
import hk.quantr.fs.PFSImageConstants;

/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class SearchDialog extends JDialog {
	public int SEARCH_BY_FILENAME_OR_DIRECTORY_NAME = 0;
	public int SEARCH_BY_FILE_CONTENT = 1;
	public int SEARCH_BY_BLOCK_NUMBER_RANGE = 2;
	public int SEARCH_BY_BLOCK_NUMBER = 3;
	public int SEARCH_BY_BLOCK_TYPE = 4;

	JPanel JPanel1 = new JPanel();
	JPanel JPanel2 = new JPanel();
	JPanel pFileContainsPanel = new JPanel();
	JPanel pBlockTypePanel = new JPanel();
	JPanel pBlockNumberRangePanel = new JPanel();
	JPanel pBlockNumberPanel = new JPanel();
	JPanel pFilenamePanel = new JPanel();
	BorderLayout borderLayout1 = new BorderLayout();
	JRadioButton pFilenameOrDirectoryNameRadioButton = new JRadioButton();
	JRadioButton pBlockNumberRadioButton = new JRadioButton();
	JRadioButton pBlockNumberRangeRadioButton = new JRadioButton();
	JRadioButton JBlockTypeRadioButton = new JRadioButton();
	JRadioButton pFileContentRadioButton = new JRadioButton();
	BorderLayout borderLayout3 = new BorderLayout();
	BorderLayout borderLayout4 = new BorderLayout();
	BorderLayout borderLayout5 = new BorderLayout();
	BorderLayout borderLayout6 = new BorderLayout();
	JPanel pControlPanel = new JPanel();
	public JTextField pFilenameOrDirectoryNameTextField = new JTextField();
	JTextField pBlockNumberTextField = new JTextField();
	JTextField pBlockNumberToTextField = new JTextField();
	JComboBox pBlockTypeComboBox = new JComboBox();
	JTextField pFileContentFileContentTextField = new JTextField();
	JPanel JPanel4 = new JPanel();
	JLabel JLabel2 = new JLabel();
	JTextField pBlockNumberFromTextField = new JTextField();
	GridLayout gridLayout1 = new GridLayout();
	BorderLayout borderLayout2 = new BorderLayout();
	FlowLayout flowLayout1 = new FlowLayout(FlowLayout.RIGHT);
	JButton pSearchButton = new JButton();
	JPanel JPanel12 = new JPanel();
	BorderLayout borderLayout7 = new BorderLayout();
	JScrollPane JScrollPane1 = new JScrollPane();
	JTable JTable1 = new JTable();
	JSplitPane JSplitPane1 = new JSplitPane();
	ButtonGroup buttonGroup1 = new ButtonGroup();
	int blockSize;
	SearchDialog_TableModel pSearchDialog_TableModel1 = new SearchDialog_TableModel();
	JButton pClearButton = new JButton();
	long showOffset = 0;
	private boolean shouldStop;
	private String relativePath;

	public SearchDialog(Frame owner, String title, boolean modal) {
		super(owner, title, modal);
		try {
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			BoxLayout JPanel2Layout = new BoxLayout(JPanel2, javax.swing.BoxLayout.Y_AXIS);
			JPanel2.setLayout(JPanel2Layout);
			getContentPane().add(JPanel2, BorderLayout.NORTH);
			jbInit();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public SearchDialog() {
		this(new Frame(), "Search", true);
	}

	private void jbInit() throws Exception {
		JPanel1.setLayout(borderLayout1);
		pFilenameOrDirectoryNameRadioButton.setPreferredSize(new Dimension(180, 27));
		pFilenameOrDirectoryNameRadioButton.setText("Filename / Directory name");
		pBlockNumberRadioButton.setPreferredSize(new Dimension(180, 27));
		pBlockNumberRadioButton.setSelected(true);
		pBlockNumberRadioButton.setText("Block number");
		pBlockNumberRangeRadioButton.setPreferredSize(new Dimension(180, 27));
		pBlockNumberRangeRadioButton.setText("Block number between");
		JBlockTypeRadioButton.setPreferredSize(new Dimension(180, 27));
		JBlockTypeRadioButton.setText("Block Type");
		pFileContentRadioButton.setPreferredSize(new Dimension(180, 27));
		pFileContentRadioButton.setText("File contains");
		pFilenamePanel.setLayout(borderLayout2);
		pBlockNumberPanel.setLayout(borderLayout3);
		pBlockNumberRangePanel.setLayout(borderLayout4);
		pBlockTypePanel.setLayout(borderLayout5);
		pFileContainsPanel.setLayout(borderLayout6);
		JPanel4.setLayout(gridLayout1);
		JLabel2.setToolTipText("");
		JLabel2.setHorizontalAlignment(SwingConstants.CENTER);
		JLabel2.setText("and");
		pControlPanel.setLayout(flowLayout1);
		pSearchButton.setText("GO!");
		pSearchButton.addActionListener(new PSearchDialog_pSearchButton_actionAdapter(this));
		JPanel12.setLayout(borderLayout7);
		JSplitPane1.setOrientation(JSplitPane.VERTICAL_SPLIT);
		JTable1.setModel(pSearchDialog_TableModel1);
		JTable1.addMouseListener(new PSearchDialog_JTable1_mouseAdapter(this));
		pClearButton.setText("Clear");
		pClearButton.addActionListener(new PSearchDialog_pClearButton_actionAdapter(this));
		pFileContainsPanel.add(pFileContentRadioButton, java.awt.BorderLayout.WEST);
		pFileContainsPanel.add(pFileContentFileContentTextField, java.awt.BorderLayout.CENTER);
		JPanel2.add(pBlockNumberPanel);
		JPanel2.add(pBlockNumberRangePanel);
		pControlPanel.add(pSearchButton);
		pControlPanel.add(pClearButton);
		JPanel12.add(JScrollPane1, java.awt.BorderLayout.CENTER);
		JSplitPane1.add(JScrollPane1, JSplitPane.TOP);
		JScrollPane1.getViewport().add(JTable1);
		pBlockTypePanel.add(JBlockTypeRadioButton, java.awt.BorderLayout.WEST);
		pBlockTypePanel.add(pBlockTypeComboBox, java.awt.BorderLayout.CENTER);
		JPanel2.add(pFilenamePanel);

		pBlockNumberPanel.add(pBlockNumberRadioButton, java.awt.BorderLayout.WEST);
		pBlockNumberPanel.add(pBlockNumberTextField, java.awt.BorderLayout.CENTER);
		JPanel4.add(pBlockNumberFromTextField, null);
		JPanel4.add(JLabel2, null);
		JPanel4.add(pBlockNumberToTextField, null);
		pBlockNumberRangePanel.add(pBlockNumberRangeRadioButton, java.awt.BorderLayout.WEST);
		JPanel2.add(pBlockTypePanel);
		pBlockNumberRangePanel.add(JPanel4, java.awt.BorderLayout.CENTER);

		pFilenamePanel.add(pFilenameOrDirectoryNameRadioButton, java.awt.BorderLayout.WEST);
		pFilenamePanel.add(pFilenameOrDirectoryNameTextField, java.awt.BorderLayout.CENTER);
		JPanel2.add(pFileContainsPanel);

		getContentPane().add(JPanel1, BorderLayout.CENTER);
		JPanel1.add(JSplitPane1, java.awt.BorderLayout.CENTER);
		buttonGroup1.add(pFilenameOrDirectoryNameRadioButton);
		buttonGroup1.add(pFileContentRadioButton);
		buttonGroup1.add(pBlockNumberRangeRadioButton);
		buttonGroup1.add(JBlockTypeRadioButton);
		buttonGroup1.add(pBlockNumberRadioButton);
		JSplitPane1.add(JPanel12, JSplitPane.BOTTOM);
		JPanel1.add(pControlPanel, java.awt.BorderLayout.SOUTH);
		pBlockTypeComboBox.addItem("Super Block");
		pBlockTypeComboBox.addItem("Free Address Block");
		pBlockTypeComboBox.addItem("Directory Block");
		pBlockTypeComboBox.addItem("Directory Indirect Block - DIB");
		pBlockTypeComboBox.addItem("File Block");
		pBlockTypeComboBox.addItem("File Content Block - FCB");
		this.setSize(500, 500);
		PFSCommonLib.centerDialog(this);
		JSplitPane1.setDividerLocation(200);
	}

	public void pSearchButton_actionPerformed(ActionEvent e) {
		Vector<HashMap<String, String>> vector = new Vector<HashMap<String, String>>();
		if (pFilenameOrDirectoryNameRadioButton.isSelected()) {
			searchPFS(SEARCH_BY_FILENAME_OR_DIRECTORY_NAME, vector);
		} else if (pFileContentRadioButton.isSelected()) {
			searchPFS(SEARCH_BY_FILE_CONTENT, vector);
		} else if (pBlockNumberRadioButton.isSelected()) {
			searchPFS(SEARCH_BY_BLOCK_NUMBER, vector);
		} else if (pBlockNumberRangeRadioButton.isSelected()) {
			searchPFS(SEARCH_BY_BLOCK_NUMBER_RANGE, vector);
		} else if (JBlockTypeRadioButton.isSelected()) {
			searchPFS(SEARCH_BY_BLOCK_TYPE, vector);
		}

		for (int x = 0; x < vector.size(); x++) {
			HashMap<String, String> hashmap = vector.get(x);
			this.pSearchDialog_TableModel1.add(Long.valueOf(hashmap.get("blockNumber").toString()), hashmap.get("type").toString(), hashmap.get("description").toString());
		}
	}

	public void searchPFS(int searchPattern, Vector<HashMap<String, String>> vector) {
		try {
			shouldStop = false;
			relativePath = "/";
			if (PFSImageConstants.filename != null) {
				RandomAccessFile br = new RandomAccessFile(PFSImageConstants.filename, "r");
				byte superBlock[] = new byte[PFSImageConstants.superBlockSize];
				br.seek(PFSImageConstants.partitionOffset);
				br.read(superBlock);

				if (superBlock[0] != 'S' || superBlock[1] != 'B') {
					JOptionPane.showMessageDialog(this, "Wrong fs format", "Error", JOptionPane.ERROR_MESSAGE);
					br.close();
				} else {
					long rootDirectoryLink = (long) (((long) (superBlock[520]) & 0xff) + (((long) (superBlock[521]) & 0xff) << 8) + (((long) (superBlock[522]) & 0xff) << 16)
							+ (((long) (superBlock[523]) & 0xff) << 24) + (((long) (superBlock[524]) & 0xff) << 32) + (((long) (superBlock[525]) & 0xff) << 40)
							+ (((long) (superBlock[526]) & 0xff) << 48) + (((long) (superBlock[527]) & 0xff) << 56));

					blockSize = (superBlock[544] + (superBlock[545] << 8)) * 1024;

					long numberOfFreeAddressBlock = (long) (((long) (superBlock[546]) & 0xff) + (((long) (superBlock[547]) & 0xff) << 8) + (((long) (superBlock[548]) & 0xff) << 16)
							+ (((long) (superBlock[549]) & 0xff) << 24) + (((long) (superBlock[550]) & 0xff) << 32) + (((long) (superBlock[551]) & 0xff) << 40)
							+ (((long) (superBlock[552]) & 0xff) << 48) + (((long) (superBlock[553]) & 0xff) << 56));

					byte fab[] = new byte[(int) (PFSImageConstants.superBlockSize * numberOfFreeAddressBlock)];
					br.read(fab);
					br.close();

					// search
					if (searchPattern == SEARCH_BY_BLOCK_NUMBER_RANGE) {
						if (numberOfFreeAddressBlock > Long.valueOf(pBlockNumberFromTextField.getText())
								&& numberOfFreeAddressBlock <= Long.valueOf(pBlockNumberToTextField.getText()) + 1) {
							for (int x = 0; x < numberOfFreeAddressBlock; x++) {
								// this.pSearchDialog_TableModel1.add(x, "FAB",
								// "");
								HashMap<String, String> hashmap = new HashMap<String, String>();
								hashmap.put("blockNumber", String.valueOf(x));
								hashmap.put("type", "FAB");
								hashmap.put("description", "");
								vector.add(hashmap);
							}
						}
					} else if (searchPattern == SEARCH_BY_BLOCK_NUMBER) {
						if (numberOfFreeAddressBlock > Long.valueOf(pBlockNumberTextField.getText())) {
							// this.pSearchDialog_TableModel1.add(Long
							// .valueOf(pBlockNumberTextField.getText()),
							// "FAB", "");
							//
							HashMap<String, String> hashmap = new HashMap<String, String>();
							hashmap.put("blockNumber", pBlockNumberTextField.getText());
							hashmap.put("type", "FAB");
							hashmap.put("description", "");
							vector.add(hashmap);
						}
					} else if (searchPattern == SEARCH_BY_BLOCK_TYPE) {
						if (pBlockTypeComboBox.getSelectedItem().toString().equals("Free Address Block")) {
							for (int x = 0; x < numberOfFreeAddressBlock; x++) {
								// this.pSearchDialog_TableModel1.add(x, "FAB",
								// "");
								HashMap<String, String> hashmap = new HashMap<String, String>();
								hashmap.put("blockNumber", String.valueOf(x));
								hashmap.put("type", "FAB");
								hashmap.put("description", "");
								vector.add(hashmap);
							}
						}
					}
					// end search

					readDirectory(rootDirectoryLink, "", searchPattern, vector);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void readDirectory(long blockNumber, String currentPath, int searchPattern, Vector<HashMap<String, String>> vector) {
		String dirName_entry = "";
		try {
			RandomAccessFile br = new RandomAccessFile(PFSImageConstants.filename, "r");
			byte data[] = new byte[PFSImageConstants.superBlockSize];
			br.seek(PFSImageConstants.partitionOffset + PFSImageConstants.superBlockSize + blockNumber * blockSize);
			br.read(data);
			br.close();
			// /////////////// directory name //////////////////////////
			dirName_entry = new String(data).substring(3, 503).trim();
			// /////////////// numberOfFile //////////////////////////
			long numberOfFile = (long) (((long) (data[503]) & 0xff) + (((long) (data[504]) & 0xff) << 8) + (((long) (data[505]) & 0xff) << 16)
					+ (((long) (data[506]) & 0xff) << 24));
			// /////////////// numberOfDirectory //////////////////////////
			long numberOfDirectory = (long) (((long) (data[507]) & 0xff) + (((long) (data[508]) & 0xff) << 8) + (((long) (data[509]) & 0xff) << 16)
					+ (((long) (data[510]) & 0xff) << 24));
			// /////////////// Permission //////////////////////////
			String permission = String.valueOf((char) data[511] & 0xff) + String.valueOf((char) data[512] & 0xff) + String.valueOf((char) data[513] & 0xff)
					+ String.valueOf((char) data[514] & 0xff) + String.valueOf((char) data[515] & 0xff) + String.valueOf((char) data[516] & 0xff)
					+ String.valueOf((char) data[517] & 0xff) + String.valueOf((char) data[518] & 0xff) + String.valueOf((char) data[519] & 0xff);
			// /////////////// createTime //////////////////////////
			long createTime = (long) ((long) ((data[520]) & 0xff) + ((long) ((data[521]) & 0xff) << 8) + ((long) ((data[522]) & 0xff) << 16) + ((long) ((data[523]) & 0xff) << 24)
					+ ((long) ((data[524]) & 0xff) << 32) + ((long) ((data[525]) & 0xff) << 40) + ((long) ((data[526]) & 0xff) << 48) + (((long) (data[527]) & 0xff) << 56));
			// /////////////// lastModifiedTime //////////////////////////
			long lastModifiedTime = (long) (((long) (data[528]) & 0xff) + (((long) (data[529]) & 0xff) << 8) + (((long) (data[530]) & 0xff) << 16)
					+ (((long) (data[531]) & 0xff) << 24) + (((long) (data[532]) & 0xff) << 32) + (((long) (data[533]) & 0xff) << 40) + (((long) (data[534]) & 0xff) << 48)
					+ (((long) (data[535]) & 0xff) << 56));
			// /////////////// directoryIndirectBlock //////////////////////////
			long directoryIndirectBlock = (long) (((long) (data[536]) & 0xff) + (((long) (data[537]) & 0xff) << 8) + (((long) (data[538]) & 0xff) << 16)
					+ (((long) (data[539]) & 0xff) << 24) + (((long) (data[540]) & 0xff) << 32) + (((long) (data[541]) & 0xff) << 40) + (((long) (data[542]) & 0xff) << 48)
					+ (((long) (data[543]) & 0xff) << 56));
			if (directoryIndirectBlock > 0) {
				readDIB(directoryIndirectBlock, currentPath, searchPattern, vector);
			}
			// create directory
			// new File(currentPath + File.separator + dirName_entry).mkdir();
			// create all the files

			// search
			if (searchPattern == SEARCH_BY_FILENAME_OR_DIRECTORY_NAME) {
				if (pFilenameOrDirectoryNameTextField.getText().startsWith("/")) {
					if ((currentPath + "/" + dirName_entry).equals(pFilenameOrDirectoryNameTextField.getText())) {
						// this.pSearchDialog_TableModel1.add(blockNumber,
						// "Directory", dirName_entry);
						HashMap<String, String> hashmap = new HashMap<String, String>();
						hashmap.put("blockNumber", String.valueOf(blockNumber));
						hashmap.put("type", "Directory");
						hashmap.put("description", dirName_entry);
						vector.add(hashmap);

						shouldStop = true;
						return;
					}
				} else {
					if (dirName_entry.equals(pFilenameOrDirectoryNameTextField.getText())) {
						// this.pSearchDialog_TableModel1.add(blockNumber,
						// "Directory", dirName_entry);
						HashMap<String, String> hashmap = new HashMap<String, String>();
						hashmap.put("blockNumber", String.valueOf(blockNumber));
						hashmap.put("type", "Directory");
						hashmap.put("description", dirName_entry);
						vector.add(hashmap);
					}
				}
			} else if (searchPattern == SEARCH_BY_BLOCK_NUMBER_RANGE) {
				if (blockNumber >= Long.valueOf(pBlockNumberFromTextField.getText()) && blockNumber <= Long.valueOf(pBlockNumberToTextField.getText())) {
					// this.pSearchDialog_TableModel1.add(blockNumber,
					// "Directory", dirName_entry);
					HashMap<String, String> hashmap = new HashMap<String, String>();
					hashmap.put("blockNumber", String.valueOf(blockNumber));
					hashmap.put("type", "Directory");
					hashmap.put("description", dirName_entry);
					vector.add(hashmap);
				}
			} else if (searchPattern == SEARCH_BY_BLOCK_NUMBER) {
				if (blockNumber == Long.valueOf(pBlockNumberTextField.getText())) {
					// this.pSearchDialog_TableModel1.add(blockNumber,
					// "Directory", dirName_entry);
					HashMap<String, String> hashmap = new HashMap<String, String>();
					hashmap.put("blockNumber", String.valueOf(blockNumber));
					hashmap.put("type", "Directory");
					hashmap.put("description", dirName_entry);
					vector.add(hashmap);
				}
			} else if (searchPattern == SEARCH_BY_BLOCK_TYPE) {
				if (pBlockTypeComboBox.getSelectedItem().toString().equals("Directory Block")) {
					// this.pSearchDialog_TableModel1.add(blockNumber,
					// "Directory", dirName_entry);
					HashMap<String, String> hashmap = new HashMap<String, String>();
					hashmap.put("blockNumber", String.valueOf(blockNumber));
					hashmap.put("type", "Directory");
					hashmap.put("description", dirName_entry);
					vector.add(hashmap);
				}
			}
			// end search

			// pReadDialog_TableModel.add("Directory", currentPath +
			// File.separator + dirName_entry);
			for (int x = 544, z = 0; x < (data.length - 9); x += 9, z++) {
				long link = (long) ((long) ((data[x + 1]) & 0xff) + ((long) ((data[x + 2]) & 0xff) << 8) + ((long) ((data[x + 3]) & 0xff) << 16)
						+ ((long) ((data[x + 4]) & 0xff) << 24) + ((long) ((data[x + 5]) & 0xff) << 32) + ((long) ((data[x + 6]) & 0xff) << 40)
						+ ((long) ((data[x + 7]) & 0xff) << 48) + ((long) ((data[x + 8]) & 0xff) << 56));
				if (link != 0) {
					if (data[x] == 0) {
						readFile(link, currentPath + "/" + dirName_entry, searchPattern, vector);
					} else {
						readDirectory(link, currentPath + "/" + dirName_entry, searchPattern, vector);
					}
				}
				if (shouldStop) {
					return;
				}
			}
			// create all the sub-directories
		} catch (Exception ex) {
			// pReadDialog_TableModel.add("Error in create directory",
			// currentPath + File.separator + dirName_entry);
			// ex.printStackTrace();if (!pfs.PFSBuilder.Globals.noGUI){new
			// PExceptionDialog(
			// ex,pfs.PFSBuilder.Globals.sessionNumber).setVisible(true);}
		}
	}

	private void readDIB(long blockNumber, String currentPath, int searchPattern, Vector<HashMap<String, String>> vector) {
		try {
			RandomAccessFile br = new RandomAccessFile(PFSImageConstants.filename, "r");
			byte data[] = new byte[PFSImageConstants.superBlockSize];
			br.seek(PFSImageConstants.partitionOffset + PFSImageConstants.superBlockSize + blockNumber * blockSize);
			br.read(data);
			br.close();

			if (searchPattern == SEARCH_BY_BLOCK_NUMBER_RANGE) {
				if (blockNumber >= Long.valueOf(pBlockNumberFromTextField.getText()) && blockNumber <= Long.valueOf(pBlockNumberToTextField.getText())) {
					// this.pSearchDialog_TableModel1.add(blockNumber, "DIB",
					// "");
					HashMap<String, String> hashmap = new HashMap<String, String>();
					hashmap.put("blockNumber", String.valueOf(blockNumber));
					hashmap.put("type", "DIB");
					hashmap.put("description", "");
					vector.add(hashmap);
				}
			} else if (searchPattern == SEARCH_BY_BLOCK_NUMBER) {
				if (blockNumber == Long.valueOf(pBlockNumberTextField.getText())) {
					// this.pSearchDialog_TableModel1.add(blockNumber, "DIB",
					// "");
					HashMap<String, String> hashmap = new HashMap<String, String>();
					hashmap.put("blockNumber", String.valueOf(blockNumber));
					hashmap.put("type", "DIB");
					hashmap.put("description", "");
					vector.add(hashmap);
				}
			} else if (searchPattern == SEARCH_BY_BLOCK_TYPE) {
				if (pBlockTypeComboBox.getSelectedItem().toString().equals("Directory Indirect Block - DIB")) {
					// this.pSearchDialog_TableModel1.add(blockNumber, "DIB",
					// "");
					HashMap<String, String> hashmap = new HashMap<String, String>();
					hashmap.put("blockNumber", String.valueOf(blockNumber));
					hashmap.put("type", "DIB");
					hashmap.put("description", "");
					vector.add(hashmap);
				}
			}

			for (int x = 542, z = 0; x < (data.length - 9); x += 9, z++) {
				long link = (long) (((long) (data[x + 1]) & 0xff) + (((long) (data[x + 2]) & 0xff) << 8) + (((long) (data[x + 3]) & 0xff) << 16)
						+ (((long) (data[x + 4]) & 0xff) << 24) + (((long) (data[x + 5]) & 0xff) << 32) + (((long) (data[x + 6]) & 0xff) << 40)
						+ (((long) (data[x + 7]) & 0xff) << 48) + (((long) (data[x + 8]) & 0xff) << 56));
				if (link != 0) {
					if (data[x] == 0) {
						readFile(link, currentPath, searchPattern, vector);
					} else {
						readDirectory(link, currentPath, searchPattern, vector);
					}
				}
			}
		} catch (Exception ex) {
			// pReadDialog_TableModel.add("Error in reading DIB",
			// "block number = " + blockNumber);
		}
	}

	private void readFile(long blockNumber, String currentPath, int searchPattern, Vector<HashMap<String, String>> vector) {
		try {
			RandomAccessFile br = new RandomAccessFile(PFSImageConstants.filename, "r");
			byte data[] = new byte[PFSImageConstants.superBlockSize];
			br.seek(PFSImageConstants.partitionOffset + PFSImageConstants.superBlockSize + blockNumber * blockSize);
			br.read(data);
			br.close();
			// /////////////// directory name //////////////////////////
			String filename_entry = new String(data).substring(4, 504).trim();
			// /////////////// Permission //////////////////////////
			String permission = String.valueOf((char) data[504] & 0xff) + String.valueOf((char) data[505] & 0xff) + String.valueOf((char) data[506] & 0xff)
					+ String.valueOf((char) data[507] & 0xff) + String.valueOf((char) data[508] & 0xff) + String.valueOf((char) data[509] & 0xff)
					+ String.valueOf((char) data[510] & 0xff) + String.valueOf((char) data[511] & 0xff) + String.valueOf((char) data[512] & 0xff);
			// /////////////// createTime //////////////////////////
			long createTime = (long) (((long) (data[513]) & 0xff) + (((long) (data[514]) & 0xff) << 8) + (((long) (data[515]) & 0xff) << 16) + ((long) ((data[516]) & 0xff) << 24)
					+ (((long) (data[517]) & 0xff) << 32) + (((long) (data[518]) & 0xff) << 40) + (((long) (data[519]) & 0xff) << 48) + (((long) (data[520]) & 0xff) << 56));
			// /////////////// lastModifiedTime //////////////////////////
			long lastModifiedTime = (long) (((long) (data[521]) & 0xff) + (((long) (data[522]) & 0xff) << 8) + (((long) (data[523]) & 0xff) << 16)
					+ (((long) (data[524]) & 0xff) << 24) + (((long) (data[525]) & 0xff) << 32) + ((long) ((data[526]) & 0xff) << 40) + (((long) (data[527]) & 0xff) << 48)
					+ (((long) (data[528]) & 0xff) << 56));
			// /////////////// fileSize //////////////////////////
			long fileSize = (long) (((long) (data[529]) & 0xff) + (((long) (data[530]) & 0xff) << 8) + (((long) (data[531]) & 0xff) << 16) + (((long) (data[532]) & 0xff) << 24)
					+ (((long) (data[533]) & 0xff) << 32) + (((long) (data[534]) & 0xff) << 40) + (((long) (data[535]) & 0xff) << 48) + (((long) (data[536]) & 0xff) << 56));
			// /////////////// fibLink //////////////////////////
			long fibLink = (long) (((long) (data[537]) & 0xff) + (((long) (data[538]) & 0xff) << 8) + (((long) (data[539]) & 0xff) << 16) + (((long) (data[540]) & 0xff) << 24)
					+ (((long) (data[541]) & 0xff) << 32) + (((long) (data[542]) & 0xff) << 40) + (((long) (data[543]) & 0xff) << 48) + (((long) (data[544]) & 0xff) << 56));
					// /////////////// links //////////////////////////
					// new File(currentPath + File.separator +
					// filename_entry).createNewFile();

			// search
			if (searchPattern == SEARCH_BY_FILENAME_OR_DIRECTORY_NAME) {
				if (pFilenameOrDirectoryNameTextField.getText().startsWith("/")) {
					if ((currentPath + "/" + filename_entry).equals(pFilenameOrDirectoryNameTextField.getText())) {
						// this.pSearchDialog_TableModel1.add(blockNumber,
						// "File",
						// filename_entry);
						HashMap<String, String> hashmap = new HashMap<String, String>();
						hashmap.put("blockNumber", String.valueOf(blockNumber));
						hashmap.put("type", "File");
						hashmap.put("description", filename_entry);
						vector.add(hashmap);
						shouldStop = true;
						return;
					}
				} else {
					if (filename_entry.equals(pFilenameOrDirectoryNameTextField.getText())) {
						// this.pSearchDialog_TableModel1.add(blockNumber,
						// "File",
						// filename_entry);
						HashMap<String, String> hashmap = new HashMap<String, String>();
						hashmap.put("blockNumber", String.valueOf(blockNumber));
						hashmap.put("type", "File");
						hashmap.put("description", filename_entry);
						vector.add(hashmap);
					}
				}
			} else if (searchPattern == SEARCH_BY_BLOCK_NUMBER_RANGE) {
				if (blockNumber >= Long.valueOf(pBlockNumberFromTextField.getText()) && blockNumber <= Long.valueOf(pBlockNumberToTextField.getText())) {
					// this.pSearchDialog_TableModel1.add(blockNumber, "File",
					// filename_entry);
					HashMap<String, String> hashmap = new HashMap<String, String>();
					hashmap.put("blockNumber", String.valueOf(blockNumber));
					hashmap.put("type", "File");
					hashmap.put("description", filename_entry);
					vector.add(hashmap);
				}
			} else if (searchPattern == SEARCH_BY_BLOCK_NUMBER) {
				if (blockNumber == Long.valueOf(pBlockNumberTextField.getText())) {
					// this.pSearchDialog_TableModel1.add(blockNumber, "File",
					// filename_entry);
					HashMap<String, String> hashmap = new HashMap<String, String>();
					hashmap.put("blockNumber", String.valueOf(blockNumber));
					hashmap.put("type", "File");
					hashmap.put("description", filename_entry);
					vector.add(hashmap);
				}
			} else if (searchPattern == SEARCH_BY_BLOCK_TYPE) {
				if (pBlockTypeComboBox.getSelectedItem().toString().equals("File Block")) {
					// this.pSearchDialog_TableModel1.add(blockNumber, "File",
					// filename_entry);
					HashMap<String, String> hashmap = new HashMap<String, String>();
					hashmap.put("blockNumber", String.valueOf(blockNumber));
					hashmap.put("type", "File");
					hashmap.put("description", filename_entry);
					vector.add(hashmap);
				}
			}

			// end search

			// if (searchPattern == this.SEARCH_BY_FILE_CONTENT) {
			int offset = 0;
			for (int x = 545, z = 0; x < (data.length - 8); x += 8, z++) {
				long link = (long) (((long) (data[x]) & 0xff) + (((long) (data[x + 1]) & 0xff) << 8) + (((long) (data[x + 2]) & 0xff) << 16) + (((long) (data[x + 3]) & 0xff) << 24)
						+ (((long) (data[x + 4]) & 0xff) << 32) + (((long) (data[x + 5]) & 0xff) << 40) + (((long) (data[x + 6]) & 0xff) << 48)
						+ (((long) (data[x + 7]) & 0xff) << 56));

				if (link != 0) {
					readFileContent(link, currentPath + "/" + filename_entry, offset, searchPattern, vector);
					offset += blockSize;
				}
			}
			// }

			// pReadDialog_TableModel.add("File", currentPath + File.separator +
			// filename_entry);
		} catch (Exception ex) {
			// pReadDialog_TableModel.add("Error in create file", currentPath +
			// File.separator + filename_entry);
			// ex.printStackTrace();if (!pfs.PFSBuilder.Globals.noGUI){new
			// PExceptionDialog(
			// ex,pfs.PFSBuilder.Globals.sessionNumber).setVisible(true);}
		}
	}

	private void readFileContent(long blockNumber, String filePath, long offset, int searchPattern, Vector<HashMap<String, String>> vector) {
		try {
			RandomAccessFile br = new RandomAccessFile(PFSImageConstants.filename, "r");
			byte data[] = new byte[PFSImageConstants.superBlockSize];
			br.seek(PFSImageConstants.partitionOffset + PFSImageConstants.superBlockSize + blockNumber * blockSize);
			br.read(data);
			br.close();

			if (searchPattern == SEARCH_BY_FILE_CONTENT) {
				if (new String(data).indexOf(pFileContentFileContentTextField.getText()) != -1) {
					// this.pSearchDialog_TableModel1.add(blockNumber,
					// "FCBlock",
					// filePath);
					HashMap<String, String> hashmap = new HashMap<String, String>();
					hashmap.put("blockNumber", String.valueOf(blockNumber));
					hashmap.put("type", "FCBlock");
					hashmap.put("description", filePath);
					vector.add(hashmap);
				}
			} else if (searchPattern == SEARCH_BY_BLOCK_NUMBER_RANGE) {
				if (blockNumber >= Long.valueOf(pBlockNumberFromTextField.getText()) && blockNumber <= Long.valueOf(pBlockNumberToTextField.getText())) {
					// this.pSearchDialog_TableModel1.add(blockNumber,
					// "FCBlock",
					// filePath);
					HashMap<String, String> hashmap = new HashMap<String, String>();
					hashmap.put("blockNumber", String.valueOf(blockNumber));
					hashmap.put("type", "FCBlock");
					hashmap.put("description", filePath);
					vector.add(hashmap);
				}
			} else if (searchPattern == SEARCH_BY_BLOCK_NUMBER) {
				if (blockNumber == Long.valueOf(pBlockNumberTextField.getText())) {
					// this.pSearchDialog_TableModel1.add(blockNumber,
					// "FCBlock",
					// filePath);
					HashMap<String, String> hashmap = new HashMap<String, String>();
					hashmap.put("blockNumber", String.valueOf(blockNumber));
					hashmap.put("type", "FCBlock");
					hashmap.put("description", filePath);
					vector.add(hashmap);
				}
			} else if (searchPattern == SEARCH_BY_BLOCK_TYPE) {
				if (pBlockTypeComboBox.getSelectedItem().toString().equals("File Content Block - FCB")) {
					// this.pSearchDialog_TableModel1.add(blockNumber,
					// "FCBlock",
					// filePath);
					HashMap<String, String> hashmap = new HashMap<String, String>();
					hashmap.put("blockNumber", String.valueOf(blockNumber));
					hashmap.put("type", "FCBlock");
					hashmap.put("description", filePath);
					vector.add(hashmap);
				}
			}

			// RandomAccessFile bw = new RandomAccessFile(filePath, "rw");
			// bw.seek(offset);
			// bw.write(data);
			// bw.close();
		} catch (Exception ex) {
			// pReadDialog_TableModel.add("Error in reading file content",
			// "block number = " + blockNumber);
		}
	}

	public void pClearButton_actionPerformed(ActionEvent e) {
		this.pSearchDialog_TableModel1.clear();
	}

	public void JTable1_mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			long blockNumber = Long.parseLong(JTable1.getValueAt(JTable1.getSelectedRow(), 0).toString());
			byte data[] = new byte[PFSImageConstants.blockSize];
			long offset = showOffset + PFSImageConstants.superBlockSize + blockNumber * PFSImageConstants.blockSize;
			data = PFSCommonLib.readFile(PFSImageConstants.filename, PFSImageConstants.partitionOffset + offset, PFSImageConstants.blockSize);
			new BlockDialog(this, String.valueOf(blockNumber), data).setVisible(true);
		}
	}
}

class PSearchDialog_JTable1_mouseAdapter extends MouseAdapter {
	private SearchDialog adaptee;

	PSearchDialog_JTable1_mouseAdapter(SearchDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void mouseClicked(MouseEvent e) {
		adaptee.JTable1_mouseClicked(e);
	}
}

class PSearchDialog_pClearButton_actionAdapter implements ActionListener {
	private SearchDialog adaptee;

	PSearchDialog_pClearButton_actionAdapter(SearchDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pClearButton_actionPerformed(e);
	}
}

class PSearchDialog_pSearchButton_actionAdapter implements ActionListener {
	private SearchDialog adaptee;

	PSearchDialog_pSearchButton_actionAdapter(SearchDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pSearchButton_actionPerformed(e);
	}
}
