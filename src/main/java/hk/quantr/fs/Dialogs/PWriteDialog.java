package hk.quantr.fs.Dialogs;

import hk.quantr.fs.Global;
import hk.quantr.fs.PExceptionDialog;
import hk.quantr.fs.PFSCommonLib;
import hk.quantr.fs.PFSSettingConstants;
import hk.quantr.fs.PFSWriteImageAlgorithm;
import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * <p>
 * Title: PFS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Petersoft
 * </p>
 *
 * @author Peter
 * @version 2005
 */

public class PWriteDialog extends JDialog implements Runnable {
	JPanel pMainPanel = new JPanel();
	BorderLayout borderLayout1 = new BorderLayout();
	JScrollPane JScrollPane1 = new JScrollPane();
	JTable JTable1 = new JTable();
	JPanel JPanel1 = new JPanel();
	JPanel pControlPanel = new JPanel();
	BorderLayout borderLayout2 = new BorderLayout();
	JProgressBar JProgressBar1 = new JProgressBar();
	WriteDialog_TableModel pWriteDialog_TableModel = new WriteDialog_TableModel();
	JButton pPauseButton = new JButton();
	JButton pCloseButton = new JButton();
	PFSWriteImageAlgorithm pFSWriteImageAlgorithm = new PFSWriteImageAlgorithm();
	JLabel pInfoLabel = new JLabel();

	public PWriteDialog() {
		try {
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			jbInit();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public PWriteDialog(Frame owner, String title, boolean modal) {
		super(owner, title, modal);
		init();
	}

	public PWriteDialog(Dialog owner, String title, boolean modal, String filename, int partitionOffset, String partitionName, int partitionSize, int blockSize, File projectFile) {
		super(owner, title, modal);
		init();
	}

	private void init() {
		try {
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			jbInit();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	private void jbInit() throws Exception {
		pMainPanel.setLayout(borderLayout1);
		JPanel1.setLayout(borderLayout2);
		pPauseButton.setText("Pause");
		pPauseButton.addActionListener(new PWriteDialog_pPauseButton_actionAdapter(this));
		pCloseButton.setText("Close");
		pCloseButton.addActionListener(new PWriteDialog_pCloseButton_actionAdapter(this));
		this.getContentPane().add(pMainPanel, java.awt.BorderLayout.CENTER);
		JScrollPane1.getViewport().add(JTable1);
		pControlPanel.add(pInfoLabel);
		pControlPanel.add(pPauseButton);
		pControlPanel.add(pCloseButton);
		JPanel1.add(JScrollPane1, java.awt.BorderLayout.CENTER);
		JPanel1.add(JProgressBar1, java.awt.BorderLayout.SOUTH);
		pMainPanel.add(JPanel1, java.awt.BorderLayout.CENTER);
		pMainPanel.add(pControlPanel, java.awt.BorderLayout.SOUTH);
		JTable1.setModel(pWriteDialog_TableModel);

		JTable1.getColumnModel().getColumn(0).setPreferredWidth(100);
		setSize(800, 550);
		PFSCommonLib.centerDialog(this);
		new Thread(this).start();
	}

	public void run() {
		try {
			if (Global.GUI) {
				pFSWriteImageAlgorithm.createPFS(this, this.JTable1, this.pWriteDialog_TableModel, this.JScrollPane1, this.JProgressBar1,
						new File(PFSSettingConstants.tempDirectory), this.pPauseButton, this.pInfoLabel);
			} else {
				pFSWriteImageAlgorithm.createPFS(new File(PFSSettingConstants.tempDirectory));
			}
			pCloseButton.setText("Finish");
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public void pPauseButton_actionPerformed(ActionEvent e) {
		if (pPauseButton.getText().equals("Pause")) {
			pPauseButton.setText("Resume");
		} else {
			pPauseButton.setText("Pause");
		}
	}

	public void pCloseButton_actionPerformed(ActionEvent e) {
		pCloseButton.setEnabled(false);
		pFSWriteImageAlgorithm.shouldStop = true;
		while (pPauseButton.isVisible() && !pPauseButton.getText().equals("Resume")) {
			try {
				Thread.currentThread().sleep(1000);
			} catch (Exception ex) {
				ex.printStackTrace();
				if (Global.GUI) {
					new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
				}
			}
		}
		this.dispose();
	}

	public void showResult(String columneOneString, long columneTwoString) {
		showResult(columneOneString, String.valueOf(columneTwoString));
	}

	public void showResult(String columneOneString, String columneTwoString) {
		if (Global.GUI) {
			System.out.println(columneOneString + " " + columneTwoString);
		} else {
			pWriteDialog_TableModel.add(columneOneString, columneTwoString);
			JScrollPane1.getViewport().setViewPosition(new Point(0, JTable1.getMaximumSize().height));
		}
	}
}

class PWriteDialog_pCloseButton_actionAdapter implements ActionListener {
	private PWriteDialog adaptee;

	PWriteDialog_pCloseButton_actionAdapter(PWriteDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pCloseButton_actionPerformed(e);
	}
}

class PWriteDialog_pPauseButton_actionAdapter implements ActionListener {
	private PWriteDialog adaptee;

	PWriteDialog_pPauseButton_actionAdapter(PWriteDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pPauseButton_actionPerformed(e);
	}
}
