package hk.quantr.fs.Dialogs;

import hk.quantr.fs.Global;
import hk.quantr.fs.PExceptionDialog;
import hk.quantr.fs.PFSCommonLib;
import hk.quantr.fs.PFSImageConstants;
import hk.quantr.fs.PFSSettingConstants;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.RandomAccessFile;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class EditByteDialog extends JDialog {
	BorderLayout borderLayout1 = new BorderLayout();
	JPanel JPanel1 = new JPanel();
	JButton pCancelButton = new JButton();
	JButton pOKButton = new JButton();
	JPanel pMainPanel = new JPanel();
	JPanel JPanel2 = new JPanel();
	JPanel JPanel3 = new JPanel();
	JPanel JPanel4 = new JPanel();
	JPanel JPanel5 = new JPanel();
	JLabel JLabel1 = new JLabel();
	JLabel JLabel2 = new JLabel();
	JLabel JLabel3 = new JLabel();
	JLabel JLabel4 = new JLabel();
	JTextField pBinaryTextField = new JTextField();
	JTextField pOctTextField = new JTextField();
	JTextField pDecTextField = new JTextField();
	JTextField pHexTextField = new JTextField();
	BorderLayout borderLayout2 = new BorderLayout();
	BorderLayout borderLayout3 = new BorderLayout();
	BorderLayout borderLayout4 = new BorderLayout();
	BorderLayout borderLayout5 = new BorderLayout();

	long blockNo;
	JPanel JPanel6 = new JPanel();
	JTextField pCharacterTextField = new JTextField();
	JLabel JLabel5 = new JLabel();
	BorderLayout borderLayout6 = new BorderLayout();

	public EditByteDialog() {
		super();
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public EditByteDialog(JDialog parent, long blockNo, byte decValue) {
		super(parent, "Edit Byte", true);
		this.blockNo = blockNo;
		try {
			jbInit();
			this.pCharacterTextField.setText(String.valueOf((char) decValue));
			this.pBinaryTextField.setText(Long.toBinaryString(decValue));
			this.pOctTextField.setText(Long.toOctalString(decValue));
			this.pDecTextField.setText(String.valueOf(decValue));
			this.pHexTextField.setText(Long.toHexString(decValue));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void jbInit() throws Exception {
		this.getContentPane().setLayout(borderLayout1);
		JLabel1.setPreferredSize(new Dimension(100, 18));
		JLabel1.setText("Hex");
		JLabel2.setPreferredSize(new Dimension(100, 18));
		JLabel2.setText("Dec");
		JLabel3.setPreferredSize(new Dimension(100, 18));
		JLabel3.setText("Oct");
		JLabel4.setPreferredSize(new Dimension(100, 18));
		JLabel4.setText("Binary");
		JPanel5.setLayout(borderLayout2);
		JPanel4.setLayout(borderLayout3);
		JPanel3.setLayout(borderLayout4);
		JPanel2.setLayout(borderLayout5);
		pOKButton.addActionListener(new JEditByteDialog_pOKButton_actionAdapter(this));
		pCancelButton.addActionListener(new JEditByteDialog_pCancelButton_actionAdapter(this));
		pDecTextField.addKeyListener(new JEditByteDialog_pDecTextField_keyAdapter(this));
		JLabel5.setPreferredSize(new Dimension(100, 18));
		JLabel5.setText("Character");
		JPanel6.setLayout(borderLayout6);
		pHexTextField.addKeyListener(new JEditByteDialog_pHexTextField_keyAdapter(this));
		pOctTextField.addKeyListener(new JEditByteDialog_pOctTextField_keyAdapter(this));
		pBinaryTextField.addKeyListener(new JEditByteDialog_pBinaryTextField_keyAdapter(this));
		pCharacterTextField.addKeyListener(new JEditByteDialog_pCharacterTextField_keyAdapter(this));
		this.getContentPane().add(JPanel1, java.awt.BorderLayout.SOUTH);
		pOKButton.setText("OK");
		JPanel1.add(pOKButton);
		JPanel1.add(pCancelButton);
		BoxLayout pMainPanelLayout = new BoxLayout(pMainPanel, javax.swing.BoxLayout.Y_AXIS);
		pMainPanel.setLayout(pMainPanelLayout);
		this.getContentPane().add(pMainPanel, java.awt.BorderLayout.CENTER);
		pMainPanel.add(JPanel3);
		pMainPanel.add(JPanel2);
		pMainPanel.add(JPanel6);
		pMainPanel.add(JPanel5);
		pMainPanel.add(JPanel4);
		JPanel5.add(pBinaryTextField, java.awt.BorderLayout.CENTER);
		JPanel5.add(JLabel4, java.awt.BorderLayout.WEST);
		JPanel4.add(pOctTextField, java.awt.BorderLayout.CENTER);
		JPanel4.add(JLabel3, java.awt.BorderLayout.WEST);
		JPanel3.add(pDecTextField, java.awt.BorderLayout.CENTER);
		JPanel3.add(JLabel2, java.awt.BorderLayout.WEST);
		JPanel2.add(pHexTextField, java.awt.BorderLayout.CENTER);
		JPanel2.add(JLabel1, java.awt.BorderLayout.WEST);
		JPanel6.add(pCharacterTextField, java.awt.BorderLayout.CENTER);
		JPanel6.add(JLabel5, java.awt.BorderLayout.WEST);
		pCancelButton.setText("Cancel");

		setSize(400, 250);
		PFSCommonLib.centerDialog(this);
	}

	public void pOKButton_actionPerformed(ActionEvent e) {
		try {
			RandomAccessFile bw = new RandomAccessFile(PFSSettingConstants.filename, "rw");
			bw.seek(PFSImageConstants.partitionOffset + PFSImageConstants.superBlockSize + blockNo * PFSImageConstants.blockSize);
			bw.writeByte(Integer.parseInt(pDecTextField.getText()));
			bw.close();
			setVisible(false);
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public void pCancelButton_actionPerformed(ActionEvent e) {
		setVisible(false);
	}

	public void pDecTextField_keyReleased(KeyEvent e) {
		try {
			this.pBinaryTextField.setText(Long.toBinaryString(Integer.parseInt(pDecTextField.getText())));
			this.pOctTextField.setText(Long.toOctalString(Integer.parseInt(pDecTextField.getText())));
			this.pHexTextField.setText(Long.toHexString(Integer.parseInt(pDecTextField.getText())));
			this.pCharacterTextField.setText(String.valueOf((char) Integer.parseInt(pDecTextField.getText())));
		} catch (Exception ex) {
		}
	}

	public void pHexTextField_keyReleased(KeyEvent e) {
		try {
			this.pDecTextField.setText(String.valueOf(Integer.parseInt(this.pHexTextField.getText(), 16)));
			pDecTextField_keyReleased(null);
		} catch (Exception ex) {
		}
	}

	public void pOctTextField_keyReleased(KeyEvent e) {
		try {
			this.pDecTextField.setText(String.valueOf(Integer.parseInt(this.pOctTextField.getText(), 8)));
			pDecTextField_keyReleased(null);
		} catch (Exception ex) {
		}
	}

	public void pBinaryTextField_keyReleased(KeyEvent e) {
		try {
			this.pDecTextField.setText(String.valueOf(Integer.parseInt(this.pBinaryTextField.getText(), 2)));
			pDecTextField_keyReleased(null);
		} catch (Exception ex) {
		}
	}

	public void pCharacterTextField_keyReleased(KeyEvent e) {
		try {
			char ascii = this.pCharacterTextField.getText().charAt(0);
			this.pDecTextField.setText(String.valueOf((int) ascii));
			pDecTextField_keyReleased(null);
		} catch (Exception ex) {
		}
	}
}

class JEditByteDialog_pCharacterTextField_keyAdapter extends KeyAdapter {
	private EditByteDialog adaptee;

	JEditByteDialog_pCharacterTextField_keyAdapter(EditByteDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void keyReleased(KeyEvent e) {
		adaptee.pCharacterTextField_keyReleased(e);
	}
}

class JEditByteDialog_pBinaryTextField_keyAdapter extends KeyAdapter {
	private EditByteDialog adaptee;

	JEditByteDialog_pBinaryTextField_keyAdapter(EditByteDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void keyReleased(KeyEvent e) {
		adaptee.pBinaryTextField_keyReleased(e);
	}
}

class JEditByteDialog_pOctTextField_keyAdapter extends KeyAdapter {
	private EditByteDialog adaptee;

	JEditByteDialog_pOctTextField_keyAdapter(EditByteDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void keyReleased(KeyEvent e) {
		adaptee.pOctTextField_keyReleased(e);
	}
}

class JEditByteDialog_pHexTextField_keyAdapter extends KeyAdapter {
	private EditByteDialog adaptee;

	JEditByteDialog_pHexTextField_keyAdapter(EditByteDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void keyReleased(KeyEvent e) {
		adaptee.pHexTextField_keyReleased(e);
	}
}

class JEditByteDialog_pDecTextField_keyAdapter extends KeyAdapter {
	private EditByteDialog adaptee;

	JEditByteDialog_pDecTextField_keyAdapter(EditByteDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void keyReleased(KeyEvent e) {
		adaptee.pDecTextField_keyReleased(e);
	}
}

class JEditByteDialog_pCancelButton_actionAdapter implements ActionListener {
	private EditByteDialog adaptee;

	JEditByteDialog_pCancelButton_actionAdapter(EditByteDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pCancelButton_actionPerformed(e);
	}
}

class JEditByteDialog_pOKButton_actionAdapter implements ActionListener {
	private EditByteDialog adaptee;

	JEditByteDialog_pOKButton_actionAdapter(EditByteDialog adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pOKButton_actionPerformed(e);
	}
}
