package hk.quantr.fs.algorithm;

public class PFSFile {
	public String name;
	public String permission;
	public long createTime;
	public long lastModifiedTime;
	public long size;
	public byte contents[];
}
