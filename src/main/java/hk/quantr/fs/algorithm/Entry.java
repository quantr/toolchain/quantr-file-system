package hk.quantr.fs.algorithm;

public class Entry {
	public boolean isFile;
	public long blockNo;

	public Entry(boolean isFile, long blockNo) {
		this.isFile = isFile;
		this.blockNo = blockNo;
	}
}
