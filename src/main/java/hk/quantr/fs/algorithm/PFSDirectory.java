package hk.quantr.fs.algorithm;

public class PFSDirectory {
	public String name;
	public long numberOfFile;
	public long numberOfDirectory;
	public long createTime;
	public long lastModifiedTime;
	public long directoryIndirectBlock;
	public String permission;
}
