package hk.quantr.fs;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import javax.swing.JProgressBar;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

/**
 * Title: PFS Description: Copyright: Copyright (c) 2005 Company: Petersoft
 * 
 * @author Peter
 * @version 2005
 */
public class FileLoader extends Thread {
	Document doc;
	File f;
	String pre;
	String post;
	JProgressBar JProgressBar;

	FileLoader(File f, Document doc, String pre, String post, JProgressBar JProgressBar) {
		setPriority(4);
		this.f = f;
		this.doc = doc;
		this.pre = pre;
		this.post = post;
		this.JProgressBar = JProgressBar;
	}

	public void run() {
		try {
			StringBuffer str = new StringBuffer("");
			JProgressBar.setMinimum(0);
			JProgressBar.setMaximum((int) f.length());
			JProgressBar.setVisible(true);

			// try to start reading
			Reader in = new FileReader(f);
			char[] buff = new char[4096];
			int nch;

			while ((nch = in.read(buff, 0, buff.length)) != -1) {
				// doc.insertString(doc.getLength(), new String(buff, 0, nch),
				// null);
				str.append(new String(buff, 0, nch));
				JProgressBar.setValue(JProgressBar.getValue() + nch);
			}

			// we are done... get rid of progressbar
			doc.insertString(0, pre, null);
			doc.insertString(pre.length(), new String(str), null);
			doc.insertString(pre.length() + str.length(), post, null);

			JProgressBar.setVisible(false);
		} catch (IOException e) {
			System.err.println(e.toString());
		} catch (BadLocationException e) {
			System.err.println(e.getMessage());
		}
	}
}
