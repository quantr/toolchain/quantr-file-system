package hk.quantr.fs;

import javax.swing.table.AbstractTableModel;

/**
 * @author Peter
 * @version 2005
 */
public class HexTableModel extends AbstractTableModel {

	private String[] columnNames = {"Offset", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "string"};
	private String qw[][] = new String[31][0];

	private int row_count = 0;

	public HexTableModel() {
	}

	public Object getValueAt(int row, int column) {
		if (row < row_count && column < 31) {
			return qw[column][row];
		} else {
			return "";
		}
	}

	public void set(String qw[][], int row_count) {
		this.qw = qw;
		this.row_count = row_count;
	}

	public void setValueAt(Object newVal, int row, int column) {
		if (row < row_count && column < 31) {
			qw[column][row] = (String) newVal;
			this.fireTableCellUpdated(column, row);
		}
	}

	public int getColumnCount() {
		return 18;
	}

	public int getRowCount() {
		return row_count;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public boolean isCellEditable(int row, int col) {
		return !(col == 0 || col > 16);
	}

	public Class getColumnClass(int columnIndex) {
		return String.class;
	}

}
