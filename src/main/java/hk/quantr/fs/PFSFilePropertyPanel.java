package hk.quantr.fs;

import hk.quantr.Dialogs.SearchDialog;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.table.TableColumn;
import javax.swing.undo.UndoManager;

import hk.quantr.fs.Dialogs.FillDialog;
import hk.quantr.fs.Dialogs.SearchTextDialog;

/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
/**
 * @author Peter
 * @version 2005
 */
public class PFSFilePropertyPanel extends JPanel implements Runnable, ClipboardOwner {
	ImageIcon editorSaveImage = new ImageIcon(QFSBuilder.class.getResource("images/editor/save.png"));
	ImageIcon editorSaveDisableImage = new ImageIcon(QFSBuilder.class.getResource("images/editor/saveDisable.png"));
	ImageIcon editorRevertImage = new ImageIcon(QFSBuilder.class.getResource("images/editor/revert.png"));
	ImageIcon editorRevertDisableImage = new ImageIcon(QFSBuilder.class.getResource("images/editor/revertDisable.png"));
	ImageIcon editorCopyImage = new ImageIcon(QFSBuilder.class.getResource("images/editor/copy.png"));
	ImageIcon editorCutImage = new ImageIcon(QFSBuilder.class.getResource("images/editor/cut.png"));
	ImageIcon editorPasteDisableImage = new ImageIcon(QFSBuilder.class.getResource("images/editor/paste.png"));
	ImageIcon editorFillImage = new ImageIcon(QFSBuilder.class.getResource("images/editor/fill.png"));
	ImageIcon editorUndoImage = new ImageIcon(QFSBuilder.class.getResource("images/editor/undo.png"));
	ImageIcon editorUndoDisableImage = new ImageIcon(QFSBuilder.class.getResource("images/editor/undoDisable.png"));
	ImageIcon editorRedoImage = new ImageIcon(QFSBuilder.class.getResource("images/editor/redo.png"));
	ImageIcon editorRedoDisableImage = new ImageIcon(QFSBuilder.class.getResource("images/editor/redoDisable.png"));
	ImageIcon searchImage = new ImageIcon(QFSBuilder.class.getResource("images/upperToolbar/search.png"));
	ImageIcon searchDisableImage = new ImageIcon(QFSBuilder.class.getResource("images/upperToolbar/searchDisable.png"));
	ImageIcon lineWrapImage = new ImageIcon(QFSBuilder.class.getResource("images/editor/lineWrap.png"));
	ImageIcon lineWrapImage2 = new ImageIcon(QFSBuilder.class.getResource("images/editor/lineWrap2.png"));

	BorderLayout borderLayout1 = new BorderLayout();
	JPanel pFileLoadingPanel = new JPanel();
	JProgressBar JProgressBar1 = new JProgressBar();
	JButton JButton1 = new JButton();
	JTabbedPane pTextTabbedPane = new JTabbedPane();
	JScrollPane JScrollPane1 = new JScrollPane();
	JPanel pCharPanel = new JPanel();
	JTextArea JTextArea = new JTextArea();
	JToolBar JToolBar1 = new JToolBar();
	JButton pRevertToolBarButton = new JButton();
	JButton pSaveToolBarButton = new JButton();
	JButton pUndoToolBarButton = new JButton();
	JButton pRedoToolBarButton = new JButton();
	JButton pSearchToolBarButton = new JButton();
	JButton pFillToolBarButton = new JButton();
	JScrollPane JScrollPane2 = new JScrollPane();
	JTable pHexTable = new JTable();
	BorderLayout borderLayout2 = new BorderLayout(10, 10);
	BorderLayout borderLayout3 = new BorderLayout();
	UndoManager m_undoManager = new UndoManager();
	File selectedFile;
	boolean stopFileLoading;
	JLabel pFileLabel = new JLabel();
	public boolean shouldSave;
	JFrame jMainFrame = new JFrame();
	HexTableModel jHexTableModel = new HexTableModel();
	JPanel pFilePropertyPanel = new JPanel();
	PFSPermissionPanel pFSPermissionPanel1 = new PFSPermissionPanel();
	PFSOwnerPanel pFSOwnerPanel1 = new PFSOwnerPanel();
	PFSCreateTimeAndLastModifiedTimePanel pFSCreateTimeAndLastModifiedTimePanel1 = new PFSCreateTimeAndLastModifiedTimePanel();
	JButton pPasteToolBarButton = new JButton();
	private JLabel pFilesizeLabel;
	private JLabel pNodeNoLabel;
	private JPanel pNodeNoPanel;
	JButton pCutToolBarButton = new JButton();
	JButton pCopyToolBarButton = new JButton();
	SearchTextDialog pSearchTextDialog = new SearchTextDialog(jMainFrame, JTextArea);
	JToggleButton pLineWrapToggleButton = new JToggleButton();

	public PFSFilePropertyPanel() {
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}
	
	public PFSFilePropertyPanel(MainFrame jMainFrame, String projectFile, File selectedFile) {
		this(jMainFrame, new File(projectFile), selectedFile);
	}

	public PFSFilePropertyPanel(MainFrame jMainFrame, File projectFile, File selectedFile) {
		super();
		this.jMainFrame = jMainFrame;
		this.selectedFile = selectedFile;
		pFSPermissionPanel1 = new PFSPermissionPanel(projectFile, selectedFile);
		pFSOwnerPanel1 = new PFSOwnerPanel(jMainFrame, projectFile, selectedFile);
		pFSCreateTimeAndLastModifiedTimePanel1 = new PFSCreateTimeAndLastModifiedTimePanel(projectFile, selectedFile);

		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}

		// search for the node number
		SearchDialog pSearchDialog = new SearchDialog();
		Vector<HashMap<String, String>> vector = new Vector<HashMap<String, String>>();
		pSearchDialog.pFilenameOrDirectoryNameTextField.setText("/" + new File(PFSSettingConstants.tempDirectory).getName()
				+ selectedFile.getAbsolutePath().substring(PFSSettingConstants.tempDirectory.length()).replaceAll("\\\\", "/"));
		pSearchDialog.searchPFS(pSearchDialog.SEARCH_BY_FILENAME_OR_DIRECTORY_NAME, vector);
		if (vector.size() > 0) {
			this.pNodeNoLabel.setText("Node number : " + vector.get(0).get("blockNumber"));
		}
		// end search for the node number

	}

	private void jbInit() throws Exception {
		this.setLayout(borderLayout1);
		pHexTable.setModel(jHexTableModel);
		JButton1.setText("Cancel");
		pRevertToolBarButton.setMaximumSize(new Dimension(45, 35));
		pRevertToolBarButton.setMinimumSize(new Dimension(45, 35));
		pRevertToolBarButton.setPreferredSize(new Dimension(45, 35));
		pRevertToolBarButton.setText("Revert");
		pRevertToolBarButton.setDisabledIcon(editorRevertDisableImage);
		pRevertToolBarButton.setIcon(editorRevertImage);
		pRevertToolBarButton.addActionListener(new PFSFilePropertyPanel_pRevertToolBarButton_actionAdapter(this));
		pSaveToolBarButton.setMaximumSize(new Dimension(40, 35));
		pSaveToolBarButton.setMinimumSize(new Dimension(40, 35));
		pSaveToolBarButton.setPreferredSize(new Dimension(45, 35));
		pSaveToolBarButton.setToolTipText("");
		pSaveToolBarButton.setText("Save");
		pSaveToolBarButton.setDisabledIcon(editorSaveDisableImage);
		pSaveToolBarButton.setIcon(editorSaveImage);
		pSaveToolBarButton.addActionListener(new PFSFilePropertyPanel_pSaveToolBarButton_actionAdapter(this));
		pUndoToolBarButton.setMaximumSize(new Dimension(45, 35));
		pUndoToolBarButton.setMinimumSize(new Dimension(45, 35));
		pUndoToolBarButton.setPreferredSize(new Dimension(45, 35));
		pUndoToolBarButton.setText("Undo");
		pUndoToolBarButton.setDisabledIcon(editorUndoDisableImage);
		pUndoToolBarButton.setIcon(editorUndoImage);
		pUndoToolBarButton.addActionListener(new PFSFilePropertyPanel_pUndoToolBarButton_actionAdapter(this));
		pRedoToolBarButton.setMaximumSize(new Dimension(45, 35));
		pRedoToolBarButton.setMinimumSize(new Dimension(45, 35));
		pRedoToolBarButton.setPreferredSize(new Dimension(45, 35));
		pRedoToolBarButton.setText("Redo");
		pRedoToolBarButton.setDisabledIcon(editorRedoDisableImage);
		pRedoToolBarButton.setIcon(editorRedoImage);
		pRedoToolBarButton.addActionListener(new PFSFilePropertyPanel_pRedoToolBarButton_actionAdapter(this));
		pSearchToolBarButton.setMaximumSize(new Dimension(45, 35));
		pSearchToolBarButton.setMinimumSize(new Dimension(45, 35));
		pSearchToolBarButton.setPreferredSize(new Dimension(45, 35));
		pSearchToolBarButton.setText("Search");
		pSearchToolBarButton.setDisabledIcon(searchDisableImage);
		pSearchToolBarButton.setIcon(searchImage);
		pSearchToolBarButton.addActionListener(new PFSFilePropertyPanel_pSearchToolBarButton_actionAdapter(this));
		pFillToolBarButton.setMaximumSize(new Dimension(45, 35));
		pFillToolBarButton.setMinimumSize(new Dimension(45, 35));
		pFillToolBarButton.setPreferredSize(new Dimension(45, 35));
		pFillToolBarButton.setText("Fill");
		pFillToolBarButton.setIcon(editorFillImage);
		pFillToolBarButton.addActionListener(new PFSFilePropertyPanel_pFillToolBarButton_actionAdapter(this));
		pFileLoadingPanel.setLayout(borderLayout2);
		pFileLoadingPanel.setVisible(false);
		pCharPanel.setLayout(borderLayout3);
		pTextTabbedPane.addMouseListener(new PFSFilePropertyPanel_jTabbedPane1_mouseAdapter(this));
		JTextArea.addKeyListener(new PFSFilePropertyPanel_JTextArea_keyAdapter(this));
		pFileLabel.setText("file size: 0");
		pPasteToolBarButton.setMaximumSize(new Dimension(45, 35));
		pPasteToolBarButton.setMinimumSize(new Dimension(45, 35));
		pPasteToolBarButton.setPreferredSize(new Dimension(45, 35));
		pPasteToolBarButton.setToolTipText("");
		pPasteToolBarButton.setIcon(editorPasteDisableImage);
		pPasteToolBarButton.setText("Paste");
		pPasteToolBarButton.addActionListener(new PFSFilePropertyPanel_pPasteToolBarButton_actionAdapter(this));
		pCutToolBarButton.setMaximumSize(new Dimension(45, 35));
		pCutToolBarButton.setMinimumSize(new Dimension(45, 35));
		pCutToolBarButton.setPreferredSize(new Dimension(45, 35));
		pCutToolBarButton.setIcon(editorCutImage);
		pCutToolBarButton.setText("Cut");
		pCutToolBarButton.addActionListener(new PFSFilePropertyPanel_pCutToolBarButton_actionAdapter(this));
		pCopyToolBarButton.setMaximumSize(new Dimension(45, 35));
		pCopyToolBarButton.setMinimumSize(new Dimension(45, 35));
		pCopyToolBarButton.setPreferredSize(new Dimension(45, 35));
		pCopyToolBarButton.setIcon(editorCopyImage);
		pCopyToolBarButton.setText("Copy");
		pCopyToolBarButton.addActionListener(new PFSFilePropertyPanel_pCopyToolBarButton_actionAdapter(this));
		pLineWrapToggleButton.setIcon(lineWrapImage2);
		pLineWrapToggleButton.setSelectedIcon(lineWrapImage);
		pLineWrapToggleButton.setMaximumSize(new Dimension(45, 35));
		pLineWrapToggleButton.setMinimumSize(new Dimension(45, 35));
		pLineWrapToggleButton.setPreferredSize(new Dimension(45, 35));
		pLineWrapToggleButton.setText("Line Wrap");
		pLineWrapToggleButton.addActionListener(new PFSFilePropertyPanel_pLineWrapToggleButton_actionAdapter(this));
		JToolBar1.add(pRevertToolBarButton);
		JToolBar1.add(pSaveToolBarButton);
		JToolBar1.add(pCopyToolBarButton);
		JToolBar1.add(pCutToolBarButton);
		JToolBar1.add(pPasteToolBarButton);
		JToolBar1.add(pUndoToolBarButton);
		JToolBar1.add(pRedoToolBarButton);
		JToolBar1.add(pSearchToolBarButton);
		JToolBar1.add(pFillToolBarButton);
		JToolBar1.add(pLineWrapToggleButton);
		JScrollPane1.setViewportView(JTextArea);
		pFileLoadingPanel.add(JProgressBar1, java.awt.BorderLayout.CENTER);
		this.add(pTextTabbedPane, java.awt.BorderLayout.CENTER);
		pTextTabbedPane.add(pCharPanel, "Char");
		pTextTabbedPane.add(JScrollPane2, "Hex");
		JScrollPane2.setViewportView(pHexTable);
		pCharPanel.add(JScrollPane1, java.awt.BorderLayout.CENTER);
		pFileLoadingPanel.add(pFileLabel, java.awt.BorderLayout.SOUTH);
		BoxLayout pFilePropertyPanelLayout = new BoxLayout(pFilePropertyPanel, javax.swing.BoxLayout.Y_AXIS);
		pFilePropertyPanel.setLayout(pFilePropertyPanelLayout);
		pFilePropertyPanel.add(pFSPermissionPanel1);
		pFilePropertyPanel.add(pFSOwnerPanel1);
		pFilePropertyPanel.add(pFSCreateTimeAndLastModifiedTimePanel1);
		pFilePropertyPanel.add(getPNodeNoPanel());
		pFilePropertyPanel.add(getJLabel1());
		pTextTabbedPane.add(pFilePropertyPanel, "Properties");
		pFileLoadingPanel.add(JButton1, java.awt.BorderLayout.EAST);
		pCharPanel.add(JToolBar1, java.awt.BorderLayout.NORTH);
		this.add(pFileLoadingPanel, java.awt.BorderLayout.SOUTH);
		JToolBar1.add(pLineWrapToggleButton);
		JTextArea.getDocument().addUndoableEditListener(new MyUndoableEditListener());
	}

	public void pFillToolBarButton_actionPerformed(ActionEvent e) {
		FillDialog pFillDialog = new FillDialog(jMainFrame, "Fill", true);
		PFSCommonLib.centerDialog(pFillDialog);
		pFillDialog.setVisible(true);
		if (pFillDialog.isCancel() == false) {
			char from = 0;
			char to = 0;
			if (pFillDialog.isChar()) {
				from = pFillDialog.getFromChar();
				to = pFillDialog.getToChar();
			} else {
				from = (char) pFillDialog.getFromAscii();
				to = (char) pFillDialog.getToAscii();
			}
			char c = from;
			StringBuffer buffer = new StringBuffer(pFillDialog.getNumberOfByte());
			buffer.append("");
			for (int x = 0; x < pFillDialog.getNumberOfByte(); x++) {
				buffer.append(c);
				c++;
				if (c == to + 1) {
					c = from;
				}
			}

			JTextArea.setText(JTextArea.getText().substring(0, JTextArea.getSelectionStart()) + buffer + JTextArea.getText().substring(JTextArea.getSelectionStart()));
			JTextArea_keyReleased(null);
		}
	}

	public void pSearchToolBarButton_actionPerformed(ActionEvent e) {
		try {
			// String searchText = JOptionPane.showInputDialog(this, "Text : ",
			// "Search",
			// JOptionPane.INFORMATION_MESSAGE);
			// if (searchText.length() > 0) {
			// for (int x = 0; x < jEditorPane.getText().length() -
			// searchText.length(); x++) {
			// if (jEditorPane.getText().substring(x, x + searchText.length() -
			// 1).equals(searchText)) {
			// jEditorPane.setSelectionStart(x);
			// jEditorPane.setSelectionEnd(x + searchText.length() - 1);
			// return;
			// }
			// }
			// }
			pSearchTextDialog.setVisible(true);

		} catch (Exception ex) {

		}
	}

	public void pRedoToolBarButton_actionPerformed(ActionEvent e) {
		try {
			m_undoManager.redo();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		updateUndoButtons();
	}

	public void pUndoToolBarButton_actionPerformed(ActionEvent e) {
		try {
			m_undoManager.undo();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
		updateUndoButtons();
	}

	public void pSaveToolBarButton_actionPerformed(ActionEvent e) {
		saveFile();
	}

	public void pRevertToolBarButton_actionPerformed(ActionEvent e) {
		if (selectedFile != null) {
			stopFileLoading = false;
			new Thread(this).start();
		}
	}

	private void updateUndoButtons() {
		System.out.println("a");
		pUndoToolBarButton.setText(m_undoManager.getUndoPresentationName());
		System.out.println("b");
		pRedoToolBarButton.setText(m_undoManager.getRedoPresentationName());
		System.out.println("c");
		pUndoToolBarButton.setEnabled(m_undoManager.canUndo());
		System.out.println("d");
		pRedoToolBarButton.setEnabled(m_undoManager.canRedo());
		System.out.println("e");
	}

	public void run() {
		try {
			if (selectedFile.isFile()) {
				JProgressBar1.setValue(0);
				JProgressBar1.setMaximum((int) selectedFile.length());

				BufferedReader br = new BufferedReader(new FileReader(selectedFile));
				pFileLoadingPanel.setVisible(true);
				JTextArea.setEnabled(false);
				JTextArea.setText(null);

				// /////////////////////////////////
				// FileInputStream fis = new FileInputStream(selectedFile);
				// DefaultEditorKit k = new DefaultEditorKit();
				// DefaultStyledDocument doc = new DefaultStyledDocument();
				// k.read(fis, doc, 0);
				// jEditorPane1.setDocument(doc);
				// /////////////////////////////////
				StringBuffer str = new StringBuffer((int) selectedFile.length());
				char tempC[] = new char[4096];
				int len;
				while ((len = br.read(tempC)) > 0 && !stopFileLoading) {
					JProgressBar1.setValue(JProgressBar1.getValue() + str.length());
					str.append(new String(tempC, 0, len));
				}
				JTextArea.setText(str.toString());
				JTextArea.setEnabled(true);
				pFileLoadingPanel.setVisible(false);
				br.close();
				pFileLabel.setText(String.valueOf(JTextArea.getText().length()));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public void saveFile() {
		try {
			if (selectedFile != null) {
				BufferedWriter bw = new BufferedWriter(new FileWriter(selectedFile));
				bw.write(JTextArea.getText());
				bw.close();
				shouldSave = false;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public void loadFile() {
		new Thread(this).start();
	}

	public void jTabbedPane1_mouseClicked(MouseEvent e) {
		if (pTextTabbedPane.getSelectedIndex() == 1) {
			binary();
		}
	}

	public void binary() {
		Thread binary_thread = new Thread() {
			public void run() {
				try {
					int row = JTextArea.getText().length() / 15;
					int x, y;
					int left = 0, con;
					String buffer = "";

					RandomAccessFile br = new RandomAccessFile(selectedFile, "r");
					byte data[] = new byte[(int) selectedFile.length()];
					br.read(data);
					br.close();

					TableColumn column = null;

					int max = (int) selectedFile.length(); // qwe.length();
					String qw[][];
					if ((int) selectedFile.length() % 16 == 0) {
						qw = new String[18][(int) selectedFile.length() / 16];
					} else {
						qw = new String[18][(int) selectedFile.length() / 16 + 1];
					}

					JProgressBar1.setMaximum(max * 2);
					JProgressBar1.setValue(0);
					pFileLoadingPanel.setVisible(true);
					for (con = 0, x = 0, y = 0; con < max;) {
						switch (x) {
						case 0:
							qw[x][y] = (String) Integer.toHexString(left);
							left += 0x10;
							x++;
							break;
						case 17:
							qw[x][y] = buffer;
							buffer = "";
							y++;
							x = 0;
							JProgressBar1.setValue(JProgressBar1.getValue() + 15);
							break;
						default:
							buffer += (char) data[con];
							qw[x][y] = (String) Integer.toHexString((byte) data[con]);
							con++;
							x++;
						}
					}
					qw[17][qw[0].length - 1] = buffer;

					if ((int) selectedFile.length() % 16 == 0) {
						jHexTableModel.set(qw, (int) selectedFile.length() / 16);
					} else {
						jHexTableModel.set(qw, (int) selectedFile.length() / 16 + 1);
					}
					jHexTableModel.fireTableDataChanged();
					JProgressBar1.setValue(JProgressBar1.getMaximum());
					pFileLoadingPanel.setVisible(false);
					for (int i = 0; i <= 16; i++) {
						column = pHexTable.getColumnModel().getColumn(i);
						if (i == 0) {
							column.setPreferredWidth(100);
						} else {
							column.setPreferredWidth(50);
						}
					}
				} catch (Exception eee) {
					eee.printStackTrace();
				}
			}
		};
		binary_thread.start();
	}

	public void renameFileTo(File newFile) {
		this.selectedFile.renameTo(newFile);
		this.selectedFile = newFile;
	}

	public void JTextArea_keyReleased(KeyEvent e) {
		shouldSave = true;
		pFileLabel.setText("file size: " + JTextArea.getText().length());
	}

	public void pCopyToolBarButton_actionPerformed(ActionEvent e) {
		setClipboardContents(JTextArea.getSelectedText());
	}

	public void pCutToolBarButton_actionPerformed(ActionEvent e) {
		setClipboardContents(JTextArea.getSelectedText());
		JTextArea.replaceSelection("");
	}

	public void pPasteToolBarButton_actionPerformed(ActionEvent e) {
		JTextArea.replaceSelection(getClipboardContents());
	}

	public void lostOwnership(Clipboard aClipboard, Transferable aContents) {
		// do nothing
	}

	public void setClipboardContents(String aString) {
		StringSelection stringSelection = new StringSelection(aString);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, this);
	}

	public String getClipboardContents() {
		String result = "";
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		// odd: the Object param of getContents is not currently used
		Transferable contents = clipboard.getContents(null);
		boolean hasTransferableText = (contents != null) && contents.isDataFlavorSupported(DataFlavor.stringFlavor);
		if (hasTransferableText) {
			try {
				result = (String) contents.getTransferData(DataFlavor.stringFlavor);
			} catch (UnsupportedFlavorException ex) {
				// highly unlikely since we are using a standard DataFlavor
				ex.printStackTrace();
				if (Global.GUI) {
					new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
				}
			} catch (IOException ex) {
				ex.printStackTrace();
				if (Global.GUI) {
					new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
				}
			}
		}
		return result;
	}

	public void pLineWrapToggleButton_actionPerformed(ActionEvent e) {
		JTextArea.setLineWrap(!pLineWrapToggleButton.isSelected());
	}

	private JPanel getPNodeNoPanel() {
		if (pNodeNoPanel == null) {
			pNodeNoPanel = new JPanel();
			FlowLayout pNodeNoPanelLayout = new FlowLayout();
			pNodeNoPanelLayout.setAlignment(FlowLayout.LEFT);
			pNodeNoPanel.setLayout(pNodeNoPanelLayout);
			pNodeNoPanel.add(getPNodeNoLabel());
		}
		return pNodeNoPanel;
	}

	private JLabel getPNodeNoLabel() {
		if (pNodeNoLabel == null) {
			pNodeNoLabel = new JLabel();
			pNodeNoLabel.setText("");
		}
		return pNodeNoLabel;
	}

	private JLabel getJLabel1() {
		if (pFilesizeLabel == null) {
			pFilesizeLabel = new JLabel();
			pFilesizeLabel.setText("File size : " + selectedFile.length() + " , " + PFSCommonLib.convertFilesize(selectedFile.length()));
		}
		return pFilesizeLabel;
	}

	protected class MyUndoableEditListener implements UndoableEditListener {
		public void undoableEditHappened(UndoableEditEvent e) {
			// Remember the edit and update the menus
			if (!JTextArea.getText().equals("")) {
				m_undoManager.addEdit(e.getEdit());

			}
		}
	}
}

class PFSFilePropertyPanel_pLineWrapToggleButton_actionAdapter implements ActionListener {
	private PFSFilePropertyPanel adaptee;

	PFSFilePropertyPanel_pLineWrapToggleButton_actionAdapter(PFSFilePropertyPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pLineWrapToggleButton_actionPerformed(e);
	}
}

class PFSFilePropertyPanel_pPasteToolBarButton_actionAdapter implements ActionListener {
	private PFSFilePropertyPanel adaptee;

	PFSFilePropertyPanel_pPasteToolBarButton_actionAdapter(PFSFilePropertyPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pPasteToolBarButton_actionPerformed(e);
	}
}

class PFSFilePropertyPanel_pCutToolBarButton_actionAdapter implements ActionListener {
	private PFSFilePropertyPanel adaptee;

	PFSFilePropertyPanel_pCutToolBarButton_actionAdapter(PFSFilePropertyPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pCutToolBarButton_actionPerformed(e);
	}
}

class PFSFilePropertyPanel_pCopyToolBarButton_actionAdapter implements ActionListener {
	private PFSFilePropertyPanel adaptee;

	PFSFilePropertyPanel_pCopyToolBarButton_actionAdapter(PFSFilePropertyPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pCopyToolBarButton_actionPerformed(e);
	}
}

class PFSFilePropertyPanel_JTextArea_keyAdapter extends KeyAdapter {
	private PFSFilePropertyPanel adaptee;

	PFSFilePropertyPanel_JTextArea_keyAdapter(PFSFilePropertyPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void keyReleased(KeyEvent e) {
		adaptee.JTextArea_keyReleased(e);
	}
}

class PFSFilePropertyPanel_jTabbedPane1_mouseAdapter extends MouseAdapter {
	private PFSFilePropertyPanel adaptee;

	PFSFilePropertyPanel_jTabbedPane1_mouseAdapter(PFSFilePropertyPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void mouseClicked(MouseEvent e) {
		adaptee.jTabbedPane1_mouseClicked(e);
	}
}

class PFSFilePropertyPanel_pRevertToolBarButton_actionAdapter implements ActionListener {
	private PFSFilePropertyPanel adaptee;

	PFSFilePropertyPanel_pRevertToolBarButton_actionAdapter(PFSFilePropertyPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pRevertToolBarButton_actionPerformed(e);
	}
}

class PFSFilePropertyPanel_pSaveToolBarButton_actionAdapter implements ActionListener {
	private PFSFilePropertyPanel adaptee;

	PFSFilePropertyPanel_pSaveToolBarButton_actionAdapter(PFSFilePropertyPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pSaveToolBarButton_actionPerformed(e);
	}
}

class PFSFilePropertyPanel_pUndoToolBarButton_actionAdapter implements ActionListener {
	private PFSFilePropertyPanel adaptee;

	PFSFilePropertyPanel_pUndoToolBarButton_actionAdapter(PFSFilePropertyPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pUndoToolBarButton_actionPerformed(e);
	}
}

class PFSFilePropertyPanel_pRedoToolBarButton_actionAdapter implements ActionListener {
	private PFSFilePropertyPanel adaptee;

	PFSFilePropertyPanel_pRedoToolBarButton_actionAdapter(PFSFilePropertyPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pRedoToolBarButton_actionPerformed(e);
	}
}

class PFSFilePropertyPanel_pSearchToolBarButton_actionAdapter implements ActionListener {
	private PFSFilePropertyPanel adaptee;

	PFSFilePropertyPanel_pSearchToolBarButton_actionAdapter(PFSFilePropertyPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pSearchToolBarButton_actionPerformed(e);
	}
}

class PFSFilePropertyPanel_pFillToolBarButton_actionAdapter implements ActionListener {
	private PFSFilePropertyPanel adaptee;

	PFSFilePropertyPanel_pFillToolBarButton_actionAdapter(PFSFilePropertyPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pFillToolBarButton_actionPerformed(e);
	}
}
