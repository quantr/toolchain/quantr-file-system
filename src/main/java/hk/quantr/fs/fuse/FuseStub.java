package hk.quantr.fs.fuse;

import java.util.Vector;

import hk.quantr.fs.algorithm.Entry;
import hk.quantr.fs.algorithm.PFSDirectory;
import hk.quantr.fs.algorithm.PFSFile;
import hk.quantr.fs.algorithm.PFSReadImageAlgorithm2;

public class FuseStub {

	static PFSReadImageAlgorithm2 algo;

	private static void init() {
		if (algo == null) {
			algo = new PFSReadImageAlgorithm2("qfs.img");
		}
	}

	public static String test1(String str) {
		System.out.println("I am test1");
		return str + str;
	}

	public static String[] test2(String str) {
		System.out.println("I am test2");
		return new String[]{"string 1", "string 2", "string 3"};
	}

	public static String[] readdir(String path) {
		init();
		System.out.println("path = " + path);
		System.out.println("partiton name = " + algo.partitionName);
		System.out.println("version = " + algo.version);
		System.out.println("block size = " + algo.blockSize);
		System.out.println("number of free addr block = " + algo.numberOfFreeAddressBlock);
		long blockNo = getBlockNo(path);
		Vector<Entry> entries = algo.readDirectoryEntries(blockNo);
		Vector<String> names = new Vector<String>();
		for (Entry entry : entries) {
			System.out.println("\t" + entry.blockNo + ", file=" + entry.isFile);
			if (entry.isFile) {
				PFSFile file = algo.readFile(entry.blockNo);
				names.add(file.name);
				System.out.println("\t\t" + file.name + ", size=" + file.size + ", permission=" + file.permission);
			} else {
				PFSDirectory dir = algo.readDirectory(entry.blockNo);
				names.add(dir.name);
				System.out.println("\t\t" + dir.name + ", no of file=" + dir.numberOfFile + ", no of dir=" + dir.numberOfDirectory + ", permission=" + dir.permission);
			}
		}
		return names.toArray(new String[0]);
	}

	public static long getBlockNo(String path) {
		init();
		return algo.getBlockNo(path);
	}

	public static boolean isFile(String path) {
		init();
		return algo.isFile(path);
	}

	public static byte[] readFileContent(long blockNumber) {
		init();
		return algo.readFileContent(blockNumber);
	}

	public static long getFileLength(long blockNumber) {
		init();
		return algo.getFileLength(blockNumber);
	}
}
