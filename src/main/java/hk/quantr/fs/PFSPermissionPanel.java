package hk.quantr.fs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class PFSPermissionPanel extends JPanel {
	JPanel JPanel2 = new JPanel();
	JCheckBox pOwnerCheckBoxX = new JCheckBox();
	JCheckBox pOwnerCheckBoxR = new JCheckBox();
	JCheckBox pOwnerCheckBoxW = new JCheckBox();
	JCheckBox pGrouJCheckBoxR = new JCheckBox();
	JCheckBox pGrouJCheckBoxW = new JCheckBox();
	JCheckBox pGrouJCheckBoxX = new JCheckBox();
	JCheckBox pOtherCheckBoxR = new JCheckBox();
	JCheckBox pOtherCheckBoxW = new JCheckBox();
	JCheckBox pOtherCheckBoxX = new JCheckBox();
	JLabel JLabel1 = new JLabel();
	BorderLayout borderLayout2 = new BorderLayout();
	File selectedFile;
	FlowLayout flowLayout1 = new FlowLayout(FlowLayout.LEFT);
	File projectFile;

	public PFSPermissionPanel() {
		super();
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public PFSPermissionPanel(File projectFile, File selectedFile) {
		super();
		this.projectFile = projectFile;
		this.selectedFile = selectedFile;
		try {
			jbInit();
			initData();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void initData() {
		try {
			PIni ini = new PIni(projectFile);
			String value = ini.getStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner read", "");
			if (value.equals("true")) {
				pOwnerCheckBoxR.setSelected(true);
			}
			value = ini.getStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner write", "");
			if (value.equals("true")) {
				pOwnerCheckBoxW.setSelected(true);
			}
			value = ini.getStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner execute", "");
			if (value.equals("true")) {
				pOwnerCheckBoxX.setSelected(true);
			}
			value = ini.getStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group read", "");
			if (value.equals("true")) {
				pGrouJCheckBoxR.setSelected(true);
			}
			value = ini.getStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group write", "");
			if (value.equals("true")) {
				pGrouJCheckBoxW.setSelected(true);
			}
			value = ini.getStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group execute", "");
			if (value.equals("true")) {
				pGrouJCheckBoxX.setSelected(true);
			}
			value = ini.getStringProperty(PFSCommonLib.getRelativePath(selectedFile), "other read", "");
			if (value.equals("true")) {
				pOtherCheckBoxR.setSelected(true);
			}
			value = ini.getStringProperty(PFSCommonLib.getRelativePath(selectedFile), "other write", "");
			if (value.equals("true")) {
				pOtherCheckBoxW.setSelected(true);
			}
			value = ini.getStringProperty(PFSCommonLib.getRelativePath(selectedFile), "other execute", "");
			if (value.equals("true")) {
				pOtherCheckBoxX.setSelected(true);
			}
		} catch (Exception ex) {

		}
	}

	private void jbInit() throws Exception {
		pOwnerCheckBoxX.setText("x");
		pOwnerCheckBoxX.addActionListener(new PFSPermissionPanel_pOwnerCheckBoxX_actionAdapter(this));
		pOwnerCheckBoxR.setText("r");
		pOwnerCheckBoxR.addActionListener(new PFSPermissionPanel_pOwnerCheckBoxR_actionAdapter(this));
		pOwnerCheckBoxW.setToolTipText("");
		pOwnerCheckBoxW.setText("w");
		pOwnerCheckBoxW.addActionListener(new PFSPermissionPanel_pOwnerCheckBoxW_actionAdapter(this));
		pGrouJCheckBoxR.setText("r");
		pGrouJCheckBoxR.addActionListener(new PFSPermissionPanel_pGrouJCheckBoxR_actionAdapter(this));
		pGrouJCheckBoxW.setText("w");
		pGrouJCheckBoxW.addActionListener(new PFSPermissionPanel_pGrouJCheckBoxW_actionAdapter(this));
		pGrouJCheckBoxX.setText("x");
		pGrouJCheckBoxX.addActionListener(new PFSPermissionPanel_pGrouJCheckBoxX_actionAdapter(this));
		pOtherCheckBoxR.setText("r");
		pOtherCheckBoxR.addActionListener(new PFSPermissionPanel_pOtherCheckBoxR_actionAdapter(this));
		pOtherCheckBoxW.setToolTipText("");
		pOtherCheckBoxW.setText("w");
		pOtherCheckBoxW.addActionListener(new PFSPermissionPanel_pOtherCheckBoxW_actionAdapter(this));
		pOtherCheckBoxX.setText("x");
		pOtherCheckBoxX.addActionListener(new PFSPermissionPanel_pOtherCheckBoxX_actionAdapter(this));
		JLabel1.setPreferredSize(new Dimension(120, 18));
		JLabel1.setText(" Permission : ");
		this.setLayout(borderLayout2);
		JPanel2.setLayout(flowLayout1);
		JPanel2.add(pOwnerCheckBoxR);
		JPanel2.add(pOwnerCheckBoxW);
		JPanel2.add(pOwnerCheckBoxX);
		JPanel2.add(pGrouJCheckBoxR);
		JPanel2.add(pGrouJCheckBoxW);
		JPanel2.add(pGrouJCheckBoxX);
		JPanel2.add(pOtherCheckBoxR);
		JPanel2.add(pOtherCheckBoxW);
		JPanel2.add(pOtherCheckBoxX);
		this.add(JLabel1, java.awt.BorderLayout.WEST);
		this.add(JPanel2, java.awt.BorderLayout.CENTER);
	}

	public String getPermission() {
		String returnValue = "";
		if (pOwnerCheckBoxR.isSelected()) {
			returnValue += "R";
		} else {
			returnValue += "-";
		}
		if (pOwnerCheckBoxW.isSelected()) {
			returnValue += "W";
		} else {
			returnValue += "-";
		}
		if (pOwnerCheckBoxX.isSelected()) {
			returnValue += "X";
		} else {
			returnValue += "-";
		}
		if (pGrouJCheckBoxR.isSelected()) {
			returnValue += "R";
		} else {
			returnValue += "-";
		}
		if (pGrouJCheckBoxW.isSelected()) {
			returnValue += "W";
		} else {
			returnValue += "-";
		}
		if (pGrouJCheckBoxX.isSelected()) {
			returnValue += "X";
		} else {
			returnValue += "-";
		}
		if (pOtherCheckBoxR.isSelected()) {
			returnValue += "R";
		} else {
			returnValue += "-";
		}
		if (pOtherCheckBoxX.isSelected()) {
			returnValue += "W";
		} else {
			returnValue += "-";
		}
		if (pOtherCheckBoxX.isSelected()) {
			returnValue += "X";
		} else {
			returnValue += "-";
		}

		return returnValue;
	}

	public void writeIni(String selectedFile) {
		// pOwnerCheckBoxR_actionPerformed(null);
		// pOwnerCheckBoxW_actionPerformed(null);
		// pOwnerCheckBoxX_actionPerformed(null);
		// pGrouJCheckBoxR_actionPerformed(null);
		// pGrouJCheckBoxW_actionPerformed(null);
		// pGrouJCheckBoxX_actionPerformed(null);
		// pOtherCheckBoxR_actionPerformed(null);
		// pOtherCheckBoxW_actionPerformed(null);
		// pOtherCheckBoxX_actionPerformed(null);
		PIni ini = new PIni(projectFile);
		if (selectedFile != null) {
			if (pOwnerCheckBoxR.isSelected()) {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner read", "true", null);
			} else {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner read", "false", null);
			}
		}
		if (selectedFile != null) {
			if (pOwnerCheckBoxW.isSelected()) {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner write", "true", null);
			} else {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner write", "false", null);
			}
		}
		if (selectedFile != null) {
			if (pOwnerCheckBoxX.isSelected()) {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner execute", "true", null);
			} else {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner execute", "false", null);
			}
		}
		if (selectedFile != null) {
			if (pGrouJCheckBoxR.isSelected()) {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group read", "true", null);
			} else {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group read", "false", null);
			}
		}
		if (selectedFile != null) {
			if (pGrouJCheckBoxW.isSelected()) {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group write", "true", null);
			} else {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group write", "false", null);
			}
		}
		if (selectedFile != null) {
			if (pGrouJCheckBoxX.isSelected()) {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group execute", "true", null);
			} else {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group execute", "false", null);
			}
		}
		if (selectedFile != null) {
			if (pOtherCheckBoxR.isSelected()) {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "other read", "true", null);
			} else {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "other read", "false", null);
			}
		}
		if (selectedFile != null) {
			if (pOtherCheckBoxW.isSelected()) {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "other write", "true", null);
			} else {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "other write", "false", null);
			}
		}
		if (selectedFile != null) {
			if (pOtherCheckBoxX.isSelected()) {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "other execute", "true", null);
			} else {
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "other execute", "false", null);
			}
		}
		ini.save();
	}

	public void pOwnerCheckBoxR_actionPerformed(ActionEvent e) {
		try {
			if (selectedFile != null) {
				PIni ini = new PIni(projectFile);
				if (pOwnerCheckBoxR.isSelected()) {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner read", "true", null);
				} else {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner read", "false", null);
				}
				ini.save();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public void pOwnerCheckBoxW_actionPerformed(ActionEvent e) {
		try {
			if (selectedFile != null) {
				PIni ini = new PIni(projectFile);
				if (pOwnerCheckBoxW.isSelected()) {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner write", "true", null);
				} else {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner write", "false", null);
				}
				ini.save();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public void pOwnerCheckBoxX_actionPerformed(ActionEvent e) {
		try {
			if (selectedFile != null) {
				PIni ini = new PIni(projectFile);
				if (pOwnerCheckBoxX.isSelected()) {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner execute", "true", null);
				} else {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner execute", "false", null);
				}
				ini.save();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public void pGrouJCheckBoxR_actionPerformed(ActionEvent e) {
		try {
			if (selectedFile != null) {
				PIni ini = new PIni(projectFile);
				if (pGrouJCheckBoxR.isSelected()) {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group read", "true", null);
				} else {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group read", "false", null);
				}
				ini.save();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public void pGrouJCheckBoxW_actionPerformed(ActionEvent e) {
		try {
			if (selectedFile != null) {
				PIni ini = new PIni(projectFile);
				if (pGrouJCheckBoxW.isSelected()) {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group write", "true", null);
				} else {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group write", "false", null);
				}
				ini.save();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void pGrouJCheckBoxX_actionPerformed(ActionEvent e) {
		try {
			if (selectedFile != null) {
				PIni ini = new PIni(projectFile);
				if (pGrouJCheckBoxX.isSelected()) {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group execute", "true", null);
				} else {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group execute", "false", null);
				}
				ini.save();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public void pOtherCheckBoxR_actionPerformed(ActionEvent e) {
		try {
			if (selectedFile != null) {
				PIni ini = new PIni(projectFile);
				if (pOtherCheckBoxR.isSelected()) {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "other read", "true", null);
				} else {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "other read", "false", null);
				}
				ini.save();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void pOtherCheckBoxW_actionPerformed(ActionEvent e) {
		try {
			if (selectedFile != null) {
				PIni ini = new PIni(projectFile);
				if (pOtherCheckBoxW.isSelected()) {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "other write", "true", null);
				} else {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "other write", "false", null);
				}
				ini.save();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public void pOtherCheckBoxX_actionPerformed(ActionEvent e) {
		try {
			if (selectedFile != null) {
				PIni ini = new PIni(projectFile);
				if (pOtherCheckBoxX.isSelected()) {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "other execute", "true", null);
				} else {
					ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "other execute", "false", null);
				}
				ini.save();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}
}

class PFSPermissionPanel_pOtherCheckBoxX_actionAdapter implements ActionListener {
	private PFSPermissionPanel adaptee;

	PFSPermissionPanel_pOtherCheckBoxX_actionAdapter(PFSPermissionPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pOtherCheckBoxX_actionPerformed(e);
	}
}

class PFSPermissionPanel_pOtherCheckBoxW_actionAdapter implements ActionListener {
	private PFSPermissionPanel adaptee;

	PFSPermissionPanel_pOtherCheckBoxW_actionAdapter(PFSPermissionPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pOtherCheckBoxW_actionPerformed(e);
	}
}

class PFSPermissionPanel_pOtherCheckBoxR_actionAdapter implements ActionListener {
	private PFSPermissionPanel adaptee;

	PFSPermissionPanel_pOtherCheckBoxR_actionAdapter(PFSPermissionPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pOtherCheckBoxR_actionPerformed(e);
	}
}

class PFSPermissionPanel_pGrouJCheckBoxX_actionAdapter implements ActionListener {
	private PFSPermissionPanel adaptee;

	PFSPermissionPanel_pGrouJCheckBoxX_actionAdapter(PFSPermissionPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pGrouJCheckBoxX_actionPerformed(e);
	}
}

class PFSPermissionPanel_pGrouJCheckBoxW_actionAdapter implements ActionListener {
	private PFSPermissionPanel adaptee;

	PFSPermissionPanel_pGrouJCheckBoxW_actionAdapter(PFSPermissionPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pGrouJCheckBoxW_actionPerformed(e);
	}
}

class PFSPermissionPanel_pGrouJCheckBoxR_actionAdapter implements ActionListener {
	private PFSPermissionPanel adaptee;

	PFSPermissionPanel_pGrouJCheckBoxR_actionAdapter(PFSPermissionPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pGrouJCheckBoxR_actionPerformed(e);
	}
}

class PFSPermissionPanel_pOwnerCheckBoxX_actionAdapter implements ActionListener {
	private PFSPermissionPanel adaptee;

	PFSPermissionPanel_pOwnerCheckBoxX_actionAdapter(PFSPermissionPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pOwnerCheckBoxX_actionPerformed(e);
	}
}

class PFSPermissionPanel_pOwnerCheckBoxW_actionAdapter implements ActionListener {
	private PFSPermissionPanel adaptee;

	PFSPermissionPanel_pOwnerCheckBoxW_actionAdapter(PFSPermissionPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pOwnerCheckBoxW_actionPerformed(e);
	}
}

class PFSPermissionPanel_pOwnerCheckBoxR_actionAdapter implements ActionListener {
	private PFSPermissionPanel adaptee;

	PFSPermissionPanel_pOwnerCheckBoxR_actionAdapter(PFSPermissionPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pOwnerCheckBoxR_actionPerformed(e);
	}
}
