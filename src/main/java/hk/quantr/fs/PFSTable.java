package hk.quantr.fs;

import javax.swing.JTable;

/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class PFSTable extends JTable {
	public PFSTable() {
		super();
	}

	public PFSTable(Object[][] rowData, Object[] columnNames) {
		super(rowData, columnNames);
	}

	// public int getSelectedRow() {
	// return 1;
	// }
	//
	// public int getSelectedColumn() {
	// return 1;
	// }

	// public boolean isCellSelected(int row, int column) {
	// return false;
	// if (mode == 0) {
	// if (row * (getColumnCount() - 1) + (column - 1) >= offset &&
	// row * (getColumnCount() - 1) + (column - 1) <= (offset +
	// PFSConstants.blockSize) &&
	// column != 0) {
	// return true;
	// } else {
	// return false;
	// }
	// } else {
	// PFSTableModel model = (PFSTableModel)this.getModel();
	// if (model.getShowOffset()==0){
	//
	// }else{
	//
	// }
	// return false;
	// }
	// if (column == 0) {
	// return false;
	// } else {
	// return super.isCellSelected(row, column);
	// }
	// }

	public void changeSelection(int rowIndex, int columnIndex, boolean toggle, boolean extend) {
		if (columnIndex == 0) {
			super.changeSelection(rowIndex, columnIndex + 1, toggle, extend);
		} else {
			super.changeSelection(rowIndex, columnIndex, toggle, extend);
		}
	}

	public void selectAndVisibleRow(long rowNumber, long column) {
		setRowSelectionInterval((int) rowNumber, (int) rowNumber);
		setColumnSelectionInterval((int) column, (int) column);

		int numberOfVisibleRow = getVisibleRect().height / getRowHeight();
		if (rowNumber > getSelectedRow()) {
			scrollRectToVisible(getCellRect((int) rowNumber + numberOfVisibleRow - 1, 0, true));
		} else {
			scrollRectToVisible(getCellRect((int) rowNumber, 0, true));
		}
	}

	// public void setSelectedBlockOffset(long offset) {
	// this.offset = offset;
	// }
	//
	// public long getSelectedBlockOffset() {
	// return offset;
	// }
	//
	// public void setMode(int mode) {
	// this.mode = mode;
	// }
}
