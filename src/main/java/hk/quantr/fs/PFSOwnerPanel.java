package hk.quantr.fs;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class PFSOwnerPanel extends JPanel {
	File selectedFile;
	JLabel pOwnerLabel = new JLabel();
	JTextField pOwnerTextField = new JTextField();
	JButton JButton1 = new JButton();
	JLabel pGrouJLabel = new JLabel();
	JTextField pGrouJTextField = new JTextField();
	JButton JButton2 = new JButton();
	FlowLayout flowLayout1 = new FlowLayout(FlowLayout.LEFT);
	Object parent;
	boolean isParentJFrame;
	File projectFile;

	public PFSOwnerPanel() {
		super();
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public PFSOwnerPanel(JFrame parent, File projectFile, File selectedFile) {
		super();
		this.parent = parent;
		this.selectedFile = selectedFile;
		this.projectFile = projectFile;
		isParentJFrame = true;
		try {
			jbInit();
			initData();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public PFSOwnerPanel(JDialog parent, File projectFile, File selectedFile) {
		super();
		this.parent = parent;
		this.projectFile = projectFile;
		this.selectedFile = selectedFile;
		isParentJFrame = false;
		try {
			jbInit();
			initData();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void jbInit() throws Exception {
		pOwnerLabel.setPreferredSize(new Dimension(120, 18));
		pOwnerLabel.setText("Owner : ");
		JButton1.setText("...");
		JButton1.addActionListener(new PFSOwnerPanel_JButton1_actionAdapter(this));
		pGrouJLabel.setText("Group : ");
		JButton2.setText("...");
		JButton2.addActionListener(new PFSOwnerPanel_JButton2_actionAdapter(this));
		this.setLayout(flowLayout1);
		pOwnerTextField.setPreferredSize(new Dimension(150, 21));
		pOwnerTextField.addKeyListener(new PFSOwnerPanel_pOwnerTextField_keyAdapter(this));
		pGrouJTextField.setPreferredSize(new Dimension(150, 21));
		pGrouJTextField.addKeyListener(new PFSOwnerPanel_pGrouJTextField_keyAdapter(this));
		this.add(pOwnerLabel);
		this.add(pOwnerTextField);
		this.add(JButton1);
		this.add(pGrouJLabel);
		this.add(pGrouJTextField);
		this.add(JButton2);
		if (!new File(PFSSettingConstants.tempDirectory + File.separator + "config" + File.separator + "passwords").exists()) {
			JButton1.setEnabled(false);
			JButton2.setEnabled(false);
		}
	}

	private void initData() {
		try {
			if (selectedFile != null) {
				PIni ini = new PIni(projectFile);
				pOwnerTextField.setText(ini.getStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner"));
				pGrouJTextField.setText(ini.getStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public void writeIni(String selectedFile) {
		PIni ini = new PIni(projectFile);
		if (selectedFile != null) {
			ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner", pOwnerTextField.getText(), null);
		}
		if (selectedFile != null) {
			ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group", pGrouJTextField.getText(), null);
		}
		ini.save();
	}

	public void pOwnerTextField_keyReleased(KeyEvent e) {
		saveOwnerChange();
	}

	public void pGrouJTextField_keyReleased(KeyEvent e) {
		saveGroupChange();
	}

	public void JButton1_actionPerformed(ActionEvent e) {
		PFSUserSelectorDialog pFSUserSelectorDialog;
		if (isParentJFrame) {
			pFSUserSelectorDialog = new PFSUserSelectorDialog((JFrame) parent);
		} else {
			pFSUserSelectorDialog = new PFSUserSelectorDialog((JDialog) parent);
		}
		pFSUserSelectorDialog.setVisible(true);
		if (pFSUserSelectorDialog.isCancel() == false) {
			pOwnerTextField.setText(pFSUserSelectorDialog.getSelectedUser());
			saveOwnerChange();
		}
	}

	public void JButton2_actionPerformed(ActionEvent e) {
		PFSUserSelectorDialog pFSUserSelectorDialog;
		if (isParentJFrame) {
			pFSUserSelectorDialog = new PFSUserSelectorDialog((JFrame) parent);
		} else {
			pFSUserSelectorDialog = new PFSUserSelectorDialog((JDialog) parent);
		}
		pFSUserSelectorDialog.setVisible(true);
		if (pFSUserSelectorDialog.isCancel() == false) {
			pGrouJTextField.setText(pFSUserSelectorDialog.getSelectedUser());
			saveGroupChange();
		}
	}

	private void saveOwnerChange() {
		if (selectedFile != null) {
			PIni ini = new PIni(projectFile);
			ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "owner", pOwnerTextField.getText(), null);
			ini.save();
		}
	}

	private void saveGroupChange() {
		if (selectedFile != null) {
			PIni ini = new PIni(projectFile);
			ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "group", pGrouJTextField.getText(), null);
			ini.save();
		}
	}
}

class PFSOwnerPanel_JButton2_actionAdapter implements ActionListener {
	private PFSOwnerPanel adaptee;

	PFSOwnerPanel_JButton2_actionAdapter(PFSOwnerPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.JButton2_actionPerformed(e);
	}
}

class PFSOwnerPanel_JButton1_actionAdapter implements ActionListener {
	private PFSOwnerPanel adaptee;

	PFSOwnerPanel_JButton1_actionAdapter(PFSOwnerPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.JButton1_actionPerformed(e);
	}
}

class PFSOwnerPanel_pOwnerTextField_keyAdapter extends KeyAdapter {
	private PFSOwnerPanel adaptee;

	PFSOwnerPanel_pOwnerTextField_keyAdapter(PFSOwnerPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void keyReleased(KeyEvent e) {
		adaptee.pOwnerTextField_keyReleased(e);
	}
}

class PFSOwnerPanel_pGrouJTextField_keyAdapter extends KeyAdapter {
	private PFSOwnerPanel adaptee;

	PFSOwnerPanel_pGrouJTextField_keyAdapter(PFSOwnerPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void keyReleased(KeyEvent e) {
		adaptee.pGrouJTextField_keyReleased(e);
	}
}
