package hk.quantr.fs;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.Properties;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.table.DefaultTableModel;

public class AboutBox extends JDialog implements ActionListener {
	JPanel panel1 = new JPanel();
	JButton pOKButton = new JButton();
	JLabel imageLabel = new JLabel();
	String product = "PFS";
	String version = "2005";
	String copyright = "Copyright (c) 2005";
	String comments = "";
	JButton pVMButton = new JButton();
	JTable pVMTable = new JTable();
	JScrollPane JScrollPane1 = new JScrollPane();
	JPanel jPanel1 = new JPanel();
	CardLayout cardLayout1 = new CardLayout();
	JPanel jPanel2 = new JPanel();
	JLabel pVersionLabel = new JLabel();

	public AboutBox() {
		super();
		try {
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			jbInit();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public AboutBox(Frame parent) {
		super(parent, true);
		try {
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			jbInit();
			// JScrollPane1.setVisible(false);
		} catch (Exception exception) {
			exception.printStackTrace();
			new PExceptionDialog(exception, Global.sessionNumber).setVisible(true);
		}
	}

	private void jbInit() throws Exception {
		this.getContentPane().setBackground(Color.white);
		setTitle("About PFSBuilder " + PropertyUtil.getProperty("version"));
		pOKButton.setText("OK");
		pOKButton.addActionListener(this);
		pVMButton.setText("VM");
		pVMButton.addActionListener(new JMainFrame_AboutBox_pVMButton_actionAdapter(this));
		jPanel1.setLayout(cardLayout1);
		pVersionLabel.setText("Version : ");
		GroupLayout panel1Layout = new GroupLayout((JComponent) panel1);
		panel1.setLayout(panel1Layout);
		panel1Layout.setHorizontalGroup(panel1Layout.createSequentialGroup().addContainerGap(51, 51).addGroup(panel1Layout.createParallelGroup()
				.addComponent(jPanel1, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 536, GroupLayout.PREFERRED_SIZE)
				.addGroup(panel1Layout.createSequentialGroup().addGap(211).addComponent(pOKButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(pVMButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
				.addContainerGap(51, 51));
		panel1Layout.setVerticalGroup(panel1Layout.createSequentialGroup().addContainerGap(5, 5)
				.addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(pOKButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(pVMButton, GroupLayout.Alignment.BASELINE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 403, GroupLayout.PREFERRED_SIZE)
				.addContainerGap(92, 92));
		getContentPane().add(panel1, null);
		GroupLayout jPanel2Layout = new GroupLayout((JComponent) jPanel2);
		jPanel2.setLayout(jPanel2Layout);
		jPanel1.add(jPanel2, "jPanel2");
		jPanel2.setPreferredSize(new java.awt.Dimension(558, 403));
		jPanel1.add(JScrollPane1, "JScrollPane1");
		JScrollPane1.getViewport().add(pVMTable);
		imageLabel.setIcon(new ImageIcon(getClass().getClassLoader().getResource("com/pfsbuilder/images/AboutDialog/AboutDialog.png")));
		pVersionLabel.setText("Version : " + PropertyUtil.getProperty("version"));
		jPanel2Layout.setVerticalGroup(
				jPanel2Layout.createSequentialGroup().addContainerGap().addComponent(imageLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
						.addComponent(pVersionLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE).addContainerGap(165, Short.MAX_VALUE));
		jPanel2Layout
				.setHorizontalGroup(jPanel2Layout.createSequentialGroup().addContainerGap()
						.addGroup(jPanel2Layout.createParallelGroup()
								.addGroup(jPanel2Layout.createSequentialGroup().addComponent(imageLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE,
										GroupLayout.PREFERRED_SIZE))
				.addGroup(GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup().addGap(259).addComponent(pVersionLabel, GroupLayout.PREFERRED_SIZE,
						GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE))).addContainerGap(134, Short.MAX_VALUE));
		initVMTable();
		setResizable(false);
		setSize(640, 550);
		PFSCommonLib.centerDialog(this);
	}

	private void initVMTable() {
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("property");
		model.addColumn("value");
		Properties prop = new Properties();
		String key = null;
		prop = System.getProperties();
		for (Enumeration e = prop.propertyNames(); e.hasMoreElements();) {
			key = e.nextElement().toString();
			model.addRow(new Object[] { key, prop.getProperty(key) });
		}
		pVMTable.setModel(model);
	}

	public void actionPerformed(ActionEvent actionEvent) {
		if (actionEvent.getSource() == pOKButton) {
			dispose();
		}
	}

	public void pVMButton_actionPerformed(ActionEvent e) {
		JScrollPane1.setVisible(!JScrollPane1.isVisible());
		cardLayout1.next(jPanel1);
	}
}

class JMainFrame_AboutBox_pVMButton_actionAdapter implements ActionListener {
	private AboutBox adaptee;

	JMainFrame_AboutBox_pVMButton_actionAdapter(AboutBox adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pVMButton_actionPerformed(e);
	}
}
