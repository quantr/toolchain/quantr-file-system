package hk.quantr.fs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import net.sourceforge.jdatepicker.JDateComponentFactory;
import net.sourceforge.jdatepicker.impl.JDatePickerImpl;

/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class PFSCreateTimeAndLastModifiedTimePanel extends JPanel {
	File projectFile;
	File selectedFile;
	GroupLayout groupLayout = new GroupLayout((JComponent) this);
	JLabel JLabel1 = new JLabel();
	JLabel JLabel2 = new JLabel();
	JDatePickerImpl jLastModifiedDatePicker = (JDatePickerImpl) JDateComponentFactory.createJDatePicker();
	JDatePickerImpl jCreateDatePicker = (JDatePickerImpl) JDateComponentFactory.createJDatePicker();

	public PFSCreateTimeAndLastModifiedTimePanel() {
		super();
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(JLabel2).addGap(104).addComponent(JLabel1).addContainerGap(121, Short.MAX_VALUE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap()
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(JLabel2).addComponent(JLabel1)).addGap(278)));
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public PFSCreateTimeAndLastModifiedTimePanel(String projectFile, String selectedFile) {
		this(new File(projectFile), new File(selectedFile));
	}

	public PFSCreateTimeAndLastModifiedTimePanel(File projectFile, File selectedFile) {
		this();
		this.projectFile = projectFile;
		this.selectedFile = selectedFile;
		try {
			jbInit();
			initData();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void jbInit() throws Exception {
		this.setLayout(groupLayout);
		JLabel1.setText("Last Modified Time : ");
		JLabel2.setText("Create Time : ");

		this.add(jCreateDatePicker);
		this.add(jLastModifiedDatePicker);

		jCreateDatePicker.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PIni ini = new PIni(projectFile);
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "create time", new SimpleDateFormat("yyyy/MM/dd")
						.format(jCreateDatePicker.getModel().getYear() + "/" + jCreateDatePicker.getModel().getMonth() + "/" + jCreateDatePicker.getModel().getDay()), null);
				ini.save();
			}
		});

		jLastModifiedDatePicker.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PIni ini = new PIni(projectFile);
				ini.setStringProperty(PFSCommonLib.getRelativePath(selectedFile), "last modified time", new SimpleDateFormat("yyyy/MM/dd").format(
						jLastModifiedDatePicker.getModel().getYear() + "/" + jLastModifiedDatePicker.getModel().getMonth() + "/" + jLastModifiedDatePicker.getModel().getDay()),
						null);
				ini.save();
			}
		});
	}

	private void initData() {
		try {
			PIni ini = new PIni(projectFile);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");

			Date date1 = simpleDateFormat.parse(ini.getStringProperty(PFSCommonLib.getRelativePath(selectedFile), "create time", simpleDateFormat.format(new Date())));
			jCreateDatePicker.getModel().setYear(date1.getYear());
			jCreateDatePicker.getModel().setMonth(date1.getMonth());
			jCreateDatePicker.getModel().setDay(date1.getDay());

			Date date2 = simpleDateFormat.parse(ini.getStringProperty(PFSCommonLib.getRelativePath(selectedFile), "last modified time", simpleDateFormat.format(new Date())));
			jLastModifiedDatePicker.getModel().setYear(date2.getYear());
			jLastModifiedDatePicker.getModel().setMonth(date2.getMonth());
			jLastModifiedDatePicker.getModel().setDay(date2.getDay());
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}
}
