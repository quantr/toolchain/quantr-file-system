package hk.quantr.fs.PFSPanels;

import hk.quantr.fs.Dialogs.BlockDialog;
import hk.quantr.fs.Dialogs.BlockDialogPanel;
import hk.quantr.fs.Global;
import hk.quantr.fs.MainFrame;
import hk.quantr.fs.PExceptionDialog;
import hk.quantr.fs.PFSCommonLib;
import hk.quantr.fs.PFSImageConstants;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;

/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
/**
 * <p>
 * Title: PFS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Petersoft
 * </p>
 *
 * @author Peter
 * @version 2005
 */
public class FILEPanel extends JPanel {
	JPanel pIDPanel = new JPanel();
	JLabel pIDLabel = new JLabel();
	JLabel pIDContentLabel = new JLabel();
	JPanel pPermissionPanel = new JPanel();
	JLabel pPermissionLabel = new JLabel();
	JLabel pPermissionContentLabel = new JLabel();
	JPanel pFileNamePanel = new JPanel();
	JLabel pFileNameLabel = new JLabel();
	JLabel pFileNameContentLabel = new JLabel();
	JPanel pFileSizePanel = new JPanel();
	JLabel pFileSizeLabel = new JLabel();
	JLabel pFileSizeContentLabel = new JLabel();
	JPanel pCreateTimePanel = new JPanel();
	JLabel pCreateTimeLabel = new JLabel();
	JLabel pCreateTimeContentLabel = new JLabel();
	JPanel pLastModifiedTimePanel = new JPanel();
	JLabel pLastModifiedTimeLabel = new JLabel();
	JLabel pLastModifiedTimeContentLabel = new JLabel();
	BorderLayout borderLayout1 = new BorderLayout();
	BorderLayout borderLayout2 = new BorderLayout();
	BorderLayout borderLayout3 = new BorderLayout();
	BorderLayout borderLayout4 = new BorderLayout();
	BorderLayout borderLayout6 = new BorderLayout();
	BorderLayout borderLayout7 = new BorderLayout();
	JPanel pFileContentBlockPanel = new JPanel();
	JLabel pFileContentBlockLabel = new JLabel();
	BorderLayout borderLayout9 = new BorderLayout();
	NonEditableDefaultTableModel tableModel = new NonEditableDefaultTableModel();
	byte data[];
	BorderLayout borderLayout5 = new BorderLayout();
	JScrollPane JScrollPane1 = new JScrollPane();
	JTable JTable1 = new JTable();
	JPanel pFIBLinkPanel = new JPanel();
	JLabel pFIBLinkLabel = new JLabel();
	JButton pFIBLinkContentLabel = new JButton();
	BorderLayout borderLayout8 = new BorderLayout();
	JPanel pMainPanel = new JPanel();
	ImageIcon mainMenuFileIcon = new ImageIcon(MainFrame.class.getResource("images/mainMenu/file.png"));
	BlockDialog jBlockDialog;
	JCheckBox pShowNullCheckBox = new JCheckBox();
	JPanel JPanel1 = new JPanel();

	public FILEPanel() {
		super();
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public FILEPanel(BlockDialog jBlockDialog, byte data[]) {
		super();
		this.jBlockDialog = jBlockDialog;
		this.data = data;
		try {
			jbInit();
			this.pIDContentLabel.setText(String.valueOf(data[0]) + "," + String.valueOf(data[1]) + "," + String.valueOf(data[2]) + "," + String.valueOf(data[3]));
			this.pIDContentLabel.setIcon(mainMenuFileIcon);
			this.pFileNameContentLabel.setText(new String(data).substring(4, 504));
			this.pPermissionContentLabel.setText(String.valueOf((char) data[504]) + String.valueOf((char) data[505]) + String.valueOf((char) data[506])
					+ String.valueOf((char) data[507]) + String.valueOf((char) data[508]) + String.valueOf((char) data[509]) + String.valueOf((char) data[510])
					+ String.valueOf((char) data[511]) + String.valueOf((char) data[512]));
			long createTime = (long) (((long) (data[513]) & 0xff) + (((long) (data[514]) & 0xff) << 8) + (((long) (data[515]) & 0xff) << 16) + ((long) ((data[516]) & 0xff) << 24)
					+ (((long) (data[517]) & 0xff) << 32) + (((long) (data[518]) & 0xff) << 40) + (((long) (data[519]) & 0xff) << 48) + (((long) (data[520]) & 0xff) << 56));
			this.pCreateTimeContentLabel.setText(String.valueOf(createTime));
			long lastModifiedTime = (long) (((long) (data[521]) & 0xff) + (((long) (data[522]) & 0xff) << 8) + (((long) (data[523]) & 0xff) << 16)
					+ (((long) (data[524]) & 0xff) << 24) + (((long) (data[525]) & 0xff) << 32) + ((long) ((data[526]) & 0xff) << 40) + (((long) (data[527]) & 0xff) << 48)
					+ (((long) (data[528]) & 0xff) << 56));
			this.pLastModifiedTimeContentLabel.setText(String.valueOf(lastModifiedTime));
			long fileSize = (long) (((long) (data[529]) & 0xff) + (((long) (data[530]) & 0xff) << 8) + (((long) (data[531]) & 0xff) << 16) + (((long) (data[532]) & 0xff) << 24)
					+ (((long) (data[533]) & 0xff) << 32) + (((long) (data[534]) & 0xff) << 40) + (((long) (data[535]) & 0xff) << 48) + (((long) (data[536]) & 0xff) << 56));
			this.pFileSizeContentLabel.setText(String.valueOf(fileSize));
			long fibLink = (long) (((long) (data[537]) & 0xff) + (((long) (data[538]) & 0xff) << 8) + (((long) (data[539]) & 0xff) << 16) + (((long) (data[540]) & 0xff) << 24)
					+ (((long) (data[541]) & 0xff) << 32) + (((long) (data[542]) & 0xff) << 40) + (((long) (data[543]) & 0xff) << 48) + (((long) (data[544]) & 0xff) << 56));
			this.pFIBLinkContentLabel.setText(String.valueOf(fibLink));

			addLinks();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void addLinks() {
		tableModel = new NonEditableDefaultTableModel();
		tableModel.addColumn("No. ");
		tableModel.addColumn("Block number");
		for (int x = 545, z = 0; x < (data.length - 8); x += 8, z++) {
			long link = (long) (((long) (data[x]) & 0xff) + (((long) (data[x + 1]) & 0xff) << 8) + (((long) (data[x + 2]) & 0xff) << 16) + (((long) (data[x + 3]) & 0xff) << 24)
					+ (((long) (data[x + 4]) & 0xff) << 32) + (((long) (data[x + 5]) & 0xff) << 40) + (((long) (data[x + 6]) & 0xff) << 48)
					+ (((long) (data[x + 7]) & 0xff) << 56));
			if (pShowNullCheckBox.isSelected() || (!pShowNullCheckBox.isSelected() && link != 0)) {
				tableModel.addRow(new Object[] { z, String.valueOf(link) });
			}
		}
		// tableModel.fireTableDataChanged();
		JTable1.setModel(tableModel);
	}

	private void jbInit() throws Exception {
		pIDLabel.setPreferredSize(new Dimension(200, 20));
		pIDLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pIDLabel.setText("ID : ");
		pPermissionLabel.setPreferredSize(new Dimension(200, 20));
		pPermissionLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pPermissionLabel.setText("Permission : ");
		pPermissionContentLabel.setToolTipText("");
		pFileNameLabel.setPreferredSize(new Dimension(200, 20));
		pFileNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pFileNameLabel.setText("File name : ");
		pFileSizeLabel.setPreferredSize(new Dimension(200, 20));
		pFileSizeLabel.setToolTipText("");
		pFileSizeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pFileSizeLabel.setText("File size : ");
		pCreateTimeLabel.setPreferredSize(new Dimension(200, 20));
		pCreateTimeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pCreateTimeLabel.setText("Create Time :");
		pLastModifiedTimeLabel.setPreferredSize(new Dimension(200, 20));
		pLastModifiedTimeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pLastModifiedTimeLabel.setText("Last modified time : ");
		pIDPanel.setLayout(borderLayout1);
		pPermissionPanel.setLayout(borderLayout2);
		pFileNamePanel.setLayout(borderLayout3);
		pFileSizePanel.setLayout(borderLayout4);
		pCreateTimePanel.setLayout(borderLayout6);
		pLastModifiedTimePanel.setLayout(borderLayout7);
		this.setLayout(borderLayout5);
		pFileContentBlockLabel.setPreferredSize(new Dimension(200, 20));
		pFileContentBlockLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pFileContentBlockLabel.setText("File content block : ");
		pFileContentBlockLabel.setVerticalAlignment(SwingConstants.TOP);
		pFileContentBlockPanel.setLayout(borderLayout9);
		pFIBLinkLabel.setPreferredSize(new Dimension(200, 20));
		pFIBLinkLabel.setToolTipText("");
		pFIBLinkLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pFIBLinkLabel.setText("FIB link : ");
		pFIBLinkPanel.setLayout(borderLayout8);
		JScrollPane1.setPreferredSize(new Dimension(50, 200));
		JTable1.addMouseListener(new FILEPanel_JTable1_mouseAdapter(this));
		pShowNullCheckBox.setText("Show null");
		pShowNullCheckBox.addActionListener(new FILEPanel_pShowNullCheckBox_actionAdapter(this));
		pFIBLinkContentLabel.addActionListener(new FILEPanel_pFIBLinkContentLabel_actionAdapter(this));
		pIDPanel.add(pIDLabel, java.awt.BorderLayout.WEST);
		pIDPanel.add(pIDContentLabel, java.awt.BorderLayout.CENTER);
		pPermissionPanel.add(pPermissionLabel, java.awt.BorderLayout.WEST);
		pPermissionPanel.add(pPermissionContentLabel, java.awt.BorderLayout.CENTER);

		pFileNamePanel.add(pFileNameLabel, java.awt.BorderLayout.WEST);
		pFileNamePanel.add(pFileNameContentLabel, java.awt.BorderLayout.CENTER);

		pFileSizePanel.add(pFileSizeLabel, java.awt.BorderLayout.WEST);
		pFileSizePanel.add(pFileSizeContentLabel, java.awt.BorderLayout.CENTER);

		pCreateTimePanel.add(pCreateTimeLabel, java.awt.BorderLayout.WEST);
		pCreateTimePanel.add(pCreateTimeContentLabel, java.awt.BorderLayout.CENTER);

		pLastModifiedTimePanel.add(pLastModifiedTimeLabel, java.awt.BorderLayout.WEST);
		pLastModifiedTimePanel.add(pLastModifiedTimeContentLabel, java.awt.BorderLayout.CENTER);

		BoxLayout pMainPanelLayout = new BoxLayout(pMainPanel, javax.swing.BoxLayout.Y_AXIS);
		pMainPanel.setLayout(pMainPanelLayout);
		pMainPanel.add(pFileNamePanel);
		pMainPanel.add(pIDPanel);
		pMainPanel.add(pFileSizePanel);
		pMainPanel.add(pLastModifiedTimePanel);
		pMainPanel.add(pCreateTimePanel);
		pMainPanel.add(pPermissionPanel);
		pMainPanel.add(pFIBLinkPanel);
		pFIBLinkPanel.add(pFIBLinkLabel, java.awt.BorderLayout.WEST);
		pFIBLinkPanel.add(pFIBLinkContentLabel, java.awt.BorderLayout.CENTER);
		pFileContentBlockPanel.add(pFileContentBlockLabel, java.awt.BorderLayout.WEST);
		JScrollPane1.getViewport().add(JTable1);
		pFileContentBlockPanel.add(JScrollPane1, java.awt.BorderLayout.CENTER);
		this.add(pMainPanel, java.awt.BorderLayout.NORTH);
		this.add(pFileContentBlockPanel, java.awt.BorderLayout.CENTER);
		JPanel1.add(pShowNullCheckBox);
		pFileContentBlockPanel.add(JPanel1, java.awt.BorderLayout.SOUTH);
		JTable1.setModel(tableModel);
	}

	public void JTable1_mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			Long blockNo = Long.parseLong(JTable1.getValueAt(JTable1.getSelectedRow(), 1).toString());
			if (blockNo != 0) {
				long offset = PFSImageConstants.partitionOffset + PFSImageConstants.superBlockSize + blockNo * PFSImageConstants.blockSize;
				byte data[] = PFSCommonLib.readFile(PFSImageConstants.filename, offset, PFSImageConstants.blockSize);
				jBlockDialog.jTabbedPane1.add(new BlockDialogPanel(jBlockDialog, new FILEContentPanel(data), blockNo), "FILE Content:" + blockNo);
				jBlockDialog.setTitle("Block Number : " + blockNo);
				jBlockDialog.jTabbedPane1.setSelectedIndex(jBlockDialog.jTabbedPane1.getTabCount() - 1);
				// new JBlockDialog(jBlockDialog, blockNo,
				// data).setVisible(true);
			}
		}
	}

	public void pShowNullCheckBox_actionPerformed(ActionEvent e) {
		addLinks();
	}

	public void pFIBLinkContentLabel_actionPerformed(ActionEvent e) {
		if (!pFIBLinkContentLabel.getText().equals("0")) {
			long blockNo = Long.valueOf(pFIBLinkContentLabel.getText());
			long offset = PFSImageConstants.partitionOffset + PFSImageConstants.superBlockSize + blockNo * PFSImageConstants.blockSize;
			byte data[] = PFSCommonLib.readFile(PFSImageConstants.filename, offset, PFSImageConstants.blockSize);
			jBlockDialog.jTabbedPane1.add(new BlockDialogPanel(jBlockDialog, new FIBPanel(jBlockDialog, data), blockNo), "FIB:" + pFIBLinkContentLabel.getText());
			jBlockDialog.setTitle("Block Number : " + blockNo);
			jBlockDialog.jTabbedPane1.setSelectedIndex(jBlockDialog.jTabbedPane1.getTabCount() - 1);
			// new JBlockDialog(jBlockDialog, pFIBLinkContentLabel.getText(),
			// data).setVisible(true);
		}
	}
}

class FILEPanel_pFIBLinkContentLabel_actionAdapter implements ActionListener {
	private FILEPanel adaptee;

	FILEPanel_pFIBLinkContentLabel_actionAdapter(FILEPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pFIBLinkContentLabel_actionPerformed(e);
	}
}

class FILEPanel_pShowNullCheckBox_actionAdapter implements ActionListener {
	private FILEPanel adaptee;

	FILEPanel_pShowNullCheckBox_actionAdapter(FILEPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pShowNullCheckBox_actionPerformed(e);
	}
}

class FILEPanel_JTable1_mouseAdapter extends MouseAdapter {
	private FILEPanel adaptee;

	FILEPanel_JTable1_mouseAdapter(FILEPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void mouseClicked(MouseEvent e) {
		adaptee.JTable1_mouseClicked(e);
	}
}
