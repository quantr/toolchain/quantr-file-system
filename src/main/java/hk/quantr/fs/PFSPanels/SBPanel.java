package hk.quantr.fs.PFSPanels;

import hk.quantr.fs.Dialogs.BlockDialog;
import hk.quantr.fs.Dialogs.BlockDialogPanel;
import hk.quantr.fs.Global;
import hk.quantr.fs.PExceptionDialog;
import hk.quantr.fs.PFSCommonLib;
import hk.quantr.fs.PFSImageConstants;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
/**
 * <p>
 * Title: PFS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Petersoft
 * </p>
 *
 * @author Peter
 * @version 2005
 */
public class SBPanel extends JPanel {
	JPanel pIDPanel = new JPanel();
	JLabel pIDLabel = new JLabel();
	JLabel pIDContentLabel = new JLabel();
	JPanel pLastModifiedTimePanel = new JPanel();
	JLabel pLastModifiedTimeLabel = new JLabel();
	JLabel pLastModifiedTimeContentLabel = new JLabel();
	JPanel pPartitionNamePanel = new JPanel();
	JLabel pPartitionNameLabel = new JLabel();
	JLabel pPartitionNameContentLabel = new JLabel();
	JPanel pUnusedPanel4 = new JPanel();
	JLabel pUnusedLabel = new JLabel();
	JLabel pUnusedContentLabel = new JLabel();
	JPanel pCreateTimePanel = new JPanel();
	JLabel pCreateTimeLabel = new JLabel();
	JLabel pCreateTimeContentLabel = new JLabel();
	JPanel pBlockSizePanel = new JPanel();
	JLabel pBlockSizeLabel = new JLabel();
	JLabel pBlockSizeContentLabel = new JLabel();
	JPanel pNumberOfFressAddressBlockPanel = new JPanel();
	JLabel pNumberOfFressAddressBlockLabel = new JLabel();
	JLabel pNumberOfFressAddressBlockContentLabel = new JLabel();
	JPanel pRootDirectoryLinkPanel = new JPanel();
	JLabel pRootDirectoryLinkLabel = new JLabel();
	JButton pRootDirectoryLinkContentLabel = new JButton();
	BorderLayout borderLayout1 = new BorderLayout();
	BorderLayout borderLayout2 = new BorderLayout();
	BorderLayout borderLayout3 = new BorderLayout();
	BorderLayout borderLayout4 = new BorderLayout();
	BorderLayout borderLayout5 = new BorderLayout();
	BorderLayout borderLayout6 = new BorderLayout();
	BorderLayout borderLayout7 = new BorderLayout();
	BorderLayout borderLayout8 = new BorderLayout();
	byte data[];
	BlockDialog jBlockDialog;

	public SBPanel() {
		super();
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public SBPanel(BlockDialog jBlockDialog, byte data[]) {
		super();
		this.jBlockDialog = jBlockDialog;
		this.data = data;
		try {
			jbInit();
			this.pIDContentLabel.setText(String.valueOf(data[0]) + "," + String.valueOf(data[1]));
			this.pPartitionNameContentLabel.setText(new String(data).substring(19, 519));
			long rootDirectoryLink = (long) (((long) (data[520]) & 0xff) + (((long) (data[521]) & 0xff) << 8) + (((long) (data[522]) & 0xff) << 16)
					+ (((long) (data[523]) & 0xff) << 24) + (((long) (data[524]) & 0xff) << 32) + (((long) (data[525]) & 0xff) << 40) + (((long) (data[526]) & 0xff) << 48)
					+ (((long) (data[527]) & 0xff) << 56));
			this.pRootDirectoryLinkContentLabel.setText(String.valueOf(rootDirectoryLink));
			long createTime = (long) (((long) (data[528]) & 0xff) + (((long) (data[529]) & 0xff) << 8) + (((long) (data[530]) & 0xff) << 16) + (((long) (data[531]) & 0xff) << 24)
					+ (((long) (data[532]) & 0xff) << 32) + (((long) (data[533]) & 0xff) << 40) + (((long) (data[534]) & 0xff) << 48) + (((long) (data[535]) & 0xff) << 56));
			this.pCreateTimeContentLabel.setText(String.valueOf(createTime));
			long lastModifiedTime = (long) (((long) (data[536]) & 0xff) + (((long) (data[537]) & 0xff) << 8) + (((long) (data[538]) & 0xff) << 16)
					+ (((long) (data[539]) & 0xff) << 24) + (((long) (data[540]) & 0xff) << 32) + (((long) (data[541]) & 0xff) << 40) + (((long) (data[542]) & 0xff) << 48)
					+ (((long) (data[543]) & 0xff) << 56));
			this.pLastModifiedTimeContentLabel.setText(String.valueOf(lastModifiedTime));
			this.pBlockSizeContentLabel.setText(String.valueOf(data[544] + (data[545] & 0xff << 8)) + "KB");
			long numberOfFreeAddress = (long) (((long) (data[546]) & 0xff) + (((long) (data[547]) & 0xff) << 8) + (((long) (data[548]) & 0xff) << 16)
					+ (((long) (data[549]) & 0xff) << 24) + (((long) (data[550]) & 0xff) << 32) + (((long) (data[551]) & 0xff) << 40) + (((long) (data[552]) & 0xff) << 48)
					+ (((long) (data[553]) & 0xff) << 56));
			this.pNumberOfFressAddressBlockContentLabel.setText(String.valueOf(numberOfFreeAddress));
			this.pUnusedContentLabel.setText(new String(data).substring(554));
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void jbInit() throws Exception {
		pIDLabel.setPreferredSize(new Dimension(200, 20));
		pIDLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pIDLabel.setText("ID : ");
		pLastModifiedTimeLabel.setPreferredSize(new Dimension(200, 20));
		pLastModifiedTimeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pLastModifiedTimeLabel.setText("Last Modified Time : ");
		pLastModifiedTimeContentLabel.setToolTipText("");
		pPartitionNameLabel.setPreferredSize(new Dimension(200, 20));
		pPartitionNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pPartitionNameLabel.setText("Partition Name : ");
		pUnusedLabel.setPreferredSize(new Dimension(200, 20));
		pUnusedLabel.setToolTipText("");
		pUnusedLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pUnusedLabel.setText("Unused : ");
		pCreateTimeLabel.setPreferredSize(new Dimension(200, 20));
		pCreateTimeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pCreateTimeLabel.setText("Create Time : ");
		pBlockSizeLabel.setPreferredSize(new Dimension(200, 20));
		pBlockSizeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pBlockSizeLabel.setText("Block Size : ");
		pNumberOfFressAddressBlockLabel.setPreferredSize(new Dimension(200, 20));
		pNumberOfFressAddressBlockLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pNumberOfFressAddressBlockLabel.setText("Number of Free Address Block : ");
		pRootDirectoryLinkLabel.setPreferredSize(new Dimension(200, 20));
		pRootDirectoryLinkLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pRootDirectoryLinkLabel.setText("Root Directory Link : ");
		pIDPanel.setLayout(borderLayout1);
		pLastModifiedTimePanel.setLayout(borderLayout2);
		pPartitionNamePanel.setLayout(borderLayout3);
		pUnusedPanel4.setLayout(borderLayout4);
		pCreateTimePanel.setLayout(borderLayout5);
		pBlockSizePanel.setLayout(borderLayout6);
		pNumberOfFressAddressBlockPanel.setLayout(borderLayout7);
		pRootDirectoryLinkPanel.setLayout(borderLayout8);
		pRootDirectoryLinkContentLabel.addActionListener(new SBPanel_pRootDirectoryLinkContentLabel_actionAdapter(this));
		pIDPanel.add(pIDLabel, java.awt.BorderLayout.WEST);
		pIDPanel.add(pIDContentLabel, java.awt.BorderLayout.CENTER);
		pLastModifiedTimePanel.add(pLastModifiedTimeLabel, java.awt.BorderLayout.WEST);
		pLastModifiedTimePanel.add(pLastModifiedTimeContentLabel, java.awt.BorderLayout.CENTER);

		pPartitionNamePanel.add(pPartitionNameLabel, java.awt.BorderLayout.WEST);
		pPartitionNamePanel.add(pPartitionNameContentLabel, java.awt.BorderLayout.CENTER);

		pUnusedPanel4.add(pUnusedLabel, java.awt.BorderLayout.WEST);
		pUnusedPanel4.add(pUnusedContentLabel, java.awt.BorderLayout.CENTER);

		pCreateTimePanel.add(pCreateTimeLabel, java.awt.BorderLayout.WEST);
		pCreateTimePanel.add(pCreateTimeContentLabel, java.awt.BorderLayout.CENTER);

		pBlockSizePanel.add(pBlockSizeLabel, java.awt.BorderLayout.WEST);
		pBlockSizePanel.add(pBlockSizeContentLabel, java.awt.BorderLayout.CENTER);

		pNumberOfFressAddressBlockPanel.add(pNumberOfFressAddressBlockLabel, java.awt.BorderLayout.WEST);
		pNumberOfFressAddressBlockPanel.add(pNumberOfFressAddressBlockContentLabel, java.awt.BorderLayout.CENTER);

		pRootDirectoryLinkPanel.add(pRootDirectoryLinkLabel, java.awt.BorderLayout.WEST);
		pRootDirectoryLinkPanel.add(pRootDirectoryLinkContentLabel, java.awt.BorderLayout.CENTER);

		BoxLayout thisLayout = new BoxLayout(this, javax.swing.BoxLayout.Y_AXIS);
		this.setLayout(thisLayout);
		this.add(pPartitionNamePanel);
		this.add(pIDPanel);
		this.add(pRootDirectoryLinkPanel);
		this.add(pCreateTimePanel);
		this.add(pLastModifiedTimePanel);
		this.add(pBlockSizePanel);
		this.add(pNumberOfFressAddressBlockPanel);
		this.add(pUnusedPanel4);
	}

	public void pRootDirectoryLinkContentLabel_actionPerformed(ActionEvent e) {
		long blockNo = Long.valueOf(pRootDirectoryLinkContentLabel.getText());
		long offset = PFSImageConstants.partitionOffset + PFSImageConstants.superBlockSize + blockNo * PFSImageConstants.blockSize;
		byte data[] = PFSCommonLib.readFile(PFSImageConstants.filename, offset, PFSImageConstants.blockSize);
		jBlockDialog.jTabbedPane1.add(new BlockDialogPanel(jBlockDialog, new DIRPanel(jBlockDialog, data), blockNo), "Dir:" + pRootDirectoryLinkContentLabel.getText());
		jBlockDialog.setTitle("Block Number : " + blockNo);
		jBlockDialog.jTabbedPane1.setSelectedIndex(jBlockDialog.jTabbedPane1.getTabCount() - 1);
		// new JBlockDialog(dialog, pRootDirectoryLinkContentLabel.getText(),
		// data).setVisible(true);
	}
}

class SBPanel_pRootDirectoryLinkContentLabel_actionAdapter implements ActionListener {
	private SBPanel adaptee;

	SBPanel_pRootDirectoryLinkContentLabel_actionAdapter(SBPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pRootDirectoryLinkContentLabel_actionPerformed(e);
	}
}
