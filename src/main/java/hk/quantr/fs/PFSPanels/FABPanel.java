package hk.quantr.fs.PFSPanels;

import hk.quantr.fs.Global;
import hk.quantr.fs.PExceptionDialog;
import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;

/**
 * <p>
 * Title: PFS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Petersoft
 * </p>
 *
 * @author Peter
 * @version 2005
 */
public class FABPanel extends JPanel {
	JPanel pIDPanel = new JPanel();
	JLabel pIDLabel = new JLabel();
	JLabel pIDContentLabel = new JLabel();
	BorderLayout borderLayout1 = new BorderLayout();
	JScrollPane JScrollPane1 = new JScrollPane();
	BorderLayout borderLayout2 = new BorderLayout();
	NonEditableDefaultTableModel tableModel = new NonEditableDefaultTableModel();
	JTable JTable1 = new JTable();
	byte data[];

	public FABPanel() {
		super();
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public FABPanel(byte data[]) {
		super();
		this.data = data;
		try {
			jbInit();
			this.pIDContentLabel.setText(String.valueOf(data[0]) + "," + String.valueOf(data[1]) + "," + String.valueOf(data[2]));

			tableModel.addColumn("Block No.");
			tableModel.addColumn("Is Free?");
			for (int x = 3; x < data.length; x++) {
				if (data[x] == 0) {
					tableModel.addRow(new Object[] { x - 3, "yes" });
				} else {
					tableModel.addRow(new Object[] { x - 3, "no" });
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void jbInit() throws Exception {
		pIDLabel.setPreferredSize(new Dimension(200, 20));
		pIDLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pIDLabel.setText("ID : ");
		pIDPanel.setLayout(borderLayout1);
		this.setLayout(borderLayout2);
		pIDPanel.add(pIDLabel, java.awt.BorderLayout.WEST);
		pIDPanel.add(pIDContentLabel, java.awt.BorderLayout.CENTER);
		this.add(JScrollPane1, java.awt.BorderLayout.CENTER);
		JScrollPane1.getViewport().add(JTable1);
		this.add(pIDPanel, java.awt.BorderLayout.NORTH);
		JTable1.setModel(tableModel);
	}
}
