package hk.quantr.fs.PFSPanels;

import java.util.Vector;

import javax.swing.table.DefaultTableModel;

public class NonEditableDefaultTableModel extends DefaultTableModel {
	public NonEditableDefaultTableModel() {
		super();
	}

	public NonEditableDefaultTableModel(int rowCount, int columnCount) {
		super(rowCount, columnCount);
	}

	public NonEditableDefaultTableModel(Vector columnNames, int rowCount) {
		super(columnNames, rowCount);
	}

	public NonEditableDefaultTableModel(Object[] columnNames, int rowCount) {
		super(columnNames, rowCount);
	}

	public NonEditableDefaultTableModel(Vector data, Vector columnNames) {
		super(data, columnNames);
	}

	public NonEditableDefaultTableModel(Object[][] data, Object[] columnNames) {
		super(data, columnNames);
	}

	public boolean isCellEditable(int row, int column) {
		return false;
	}
}
