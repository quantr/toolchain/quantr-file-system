package hk.quantr.fs.PFSPanels;

import hk.quantr.fs.Dialogs.BlockDialog;
import hk.quantr.fs.Dialogs.BlockDialogPanel;
import hk.quantr.fs.Global;
import hk.quantr.fs.PExceptionDialog;
import hk.quantr.fs.PFSCommonLib;
import hk.quantr.fs.PFSImageConstants;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
/**
 * This code was edited or generated using CloudGarden's Jigloo
 * SWT/Swing GUI Builder, which is free for non-commercial
 * use. If Jigloo is being used commercially (ie, by a corporation,
 * company or business for any purpose whatever) then you
 * should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details.
 * Use of Jigloo implies acceptance of these licensing terms.
 * A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
 * THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
 * LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class FIBPanel extends JPanel {
	JPanel pIDPanel = new JPanel();
	JLabel pIDLabel = new JLabel();
	JLabel pIDContentLabel = new JLabel();
	BorderLayout borderLayout1 = new BorderLayout();
	JPanel pFileContentBlockPanel = new JPanel();
	JLabel pFileContentBlockLabel = new JLabel();
	BorderLayout borderLayout9 = new BorderLayout();
	NonEditableDefaultTableModel tableModel = new NonEditableDefaultTableModel();
	byte data[];
	BorderLayout borderLayout5 = new BorderLayout();
	JScrollPane JScrollPane1 = new JScrollPane();
	JTable JTable1 = new JTable();
	JPanel pFIBLinkPanel = new JPanel();
	JLabel pFIBLinkLabel = new JLabel();
	JButton pFIBLinkContentLabel = new JButton();
	BorderLayout borderLayout8 = new BorderLayout();
	JPanel pMainPanel = new JPanel();
	BlockDialog jBlockDialog;

	public FIBPanel() {
		super();
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public FIBPanel(BlockDialog jBlockDialog, byte data[]) {
		super();
		this.jBlockDialog = jBlockDialog;
		this.data = data;
		try {
			jbInit();
			this.pIDContentLabel.setText(String.valueOf(data[0]) + "," + String.valueOf(data[1]) + "," + String.valueOf(data[2]));
			long fibLink = (long) (((long) (data[data.length - 8]) & 0xff) + (((long) (data[data.length - 7]) & 0xff) << 8) + (((long) (data[data.length - 6]) & 0xff) << 16)
					+ (((long) (data[data.length - 5]) & 0xff) << 24) + (((long) (data[data.length - 4]) & 0xff) << 32) + (((long) (data[data.length - 3]) & 0xff) << 40)
					+ (((long) (data[data.length - 2]) & 0xff) << 48) + (((long) (data[data.length - 1]) & 0xff) << 56));
			this.pFIBLinkContentLabel.setText(String.valueOf(fibLink));

			tableModel.addColumn("No. ");
			tableModel.addColumn("Block number");
			for (int x = 3, z = 0; x <= data.length - 8 - 8; x += 8, z++) {
				long link = (long) (((long) (data[x]) & 0xff) + (((long) (data[x + 1]) & 0xff) << 8) + (((long) (data[x + 2]) & 0xff) << 16) + (((long) (data[x + 3]) & 0xff) << 24)
						+ (((long) (data[x + 4]) & 0xff) << 32) + (((long) (data[x + 5]) & 0xff) << 40) + (((long) (data[x + 6]) & 0xff) << 48)
						+ (((long) (data[x + 7]) & 0xff) << 56));

				tableModel.addRow(new Object[] { z, String.valueOf(link) });
			}
			tableModel.fireTableDataChanged();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void jbInit() throws Exception {
		pIDLabel.setPreferredSize(new Dimension(200, 20));
		pIDLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pIDLabel.setText("ID : ");
		pIDPanel.setLayout(borderLayout1);
		this.setLayout(borderLayout5);
		pFileContentBlockLabel.setPreferredSize(new Dimension(200, 20));
		pFileContentBlockLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pFileContentBlockLabel.setText("File content block : ");
		pFileContentBlockLabel.setVerticalAlignment(SwingConstants.TOP);
		pFileContentBlockPanel.setLayout(borderLayout9);
		pFIBLinkLabel.setPreferredSize(new Dimension(200, 20));
		pFIBLinkLabel.setToolTipText("");
		pFIBLinkLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pFIBLinkLabel.setText("FIB link : ");
		pFIBLinkPanel.setLayout(borderLayout8);
		JScrollPane1.setPreferredSize(new Dimension(50, 200));
		JTable1.addMouseListener(new FIBPanel_JTable1_mouseAdapter(this));
		pFIBLinkContentLabel.addActionListener(new FIBPanel_pFIBLinkContentLabel_actionAdapter(this));
		pIDPanel.add(pIDLabel, java.awt.BorderLayout.WEST);
		pIDPanel.add(pIDContentLabel, java.awt.BorderLayout.CENTER);
		BoxLayout pMainPanelLayout = new BoxLayout(pMainPanel, javax.swing.BoxLayout.Y_AXIS);
		pMainPanel.setLayout(pMainPanelLayout);
		pMainPanel.add(pIDPanel);
		pMainPanel.add(pFIBLinkPanel);
		pFIBLinkPanel.add(pFIBLinkLabel, java.awt.BorderLayout.WEST);
		pFIBLinkPanel.add(pFIBLinkContentLabel, java.awt.BorderLayout.CENTER);
		pFileContentBlockPanel.add(pFileContentBlockLabel, java.awt.BorderLayout.WEST);
		JScrollPane1.getViewport().add(JTable1);
		pFileContentBlockPanel.add(JScrollPane1, java.awt.BorderLayout.CENTER);
		this.add(pMainPanel, java.awt.BorderLayout.NORTH);
		this.add(pFileContentBlockPanel, java.awt.BorderLayout.CENTER);
		JTable1.setModel(tableModel);
	}

	public void JTable1_mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			long blockNo = Long.parseLong(JTable1.getValueAt(JTable1.getSelectedRow(), 1).toString());
			if (blockNo != 0) {
				long offset = PFSImageConstants.partitionOffset + PFSImageConstants.superBlockSize + blockNo * PFSImageConstants.blockSize;
				byte data[] = PFSCommonLib.readFile(PFSImageConstants.filename, offset, PFSImageConstants.blockSize);
				jBlockDialog.jTabbedPane1.add(new BlockDialogPanel(jBlockDialog, new FILEContentPanel(data), blockNo), "FILE Content:" + blockNo);
				jBlockDialog.setTitle("Block Number : " + blockNo);
				jBlockDialog.jTabbedPane1.setSelectedIndex(jBlockDialog.jTabbedPane1.getTabCount() - 1);
				// new JBlockDialog(dialog, blockNo, data).setVisible(true);
			}
		}
	}

	public void pFIBLinkContentLabel_actionPerformed(ActionEvent e) {
		if (!pFIBLinkContentLabel.getText().equals("0")) {
			long blockNo = Long.valueOf(pFIBLinkContentLabel.getText());
			long offset = PFSImageConstants.partitionOffset + PFSImageConstants.superBlockSize + blockNo * PFSImageConstants.blockSize;
			byte data[] = PFSCommonLib.readFile(PFSImageConstants.filename, offset, PFSImageConstants.blockSize);
			jBlockDialog.jTabbedPane1.add(new BlockDialogPanel(jBlockDialog, new FIBPanel(jBlockDialog, data), blockNo), "FIB:" + pFIBLinkContentLabel.getText());
			jBlockDialog.setTitle("Block Number : " + blockNo);
			jBlockDialog.jTabbedPane1.setSelectedIndex(jBlockDialog.jTabbedPane1.getTabCount() - 1);
			// new JBlockDialog(dialog, pFIBLinkContentLabel.getText(),
			// data).setVisible(true);
		}
	}
}

class FIBPanel_pFIBLinkContentLabel_actionAdapter implements ActionListener {
	private FIBPanel adaptee;

	FIBPanel_pFIBLinkContentLabel_actionAdapter(FIBPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pFIBLinkContentLabel_actionPerformed(e);
	}
}

class FIBPanel_JTable1_mouseAdapter extends MouseAdapter {
	private FIBPanel adaptee;

	FIBPanel_JTable1_mouseAdapter(FIBPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void mouseClicked(MouseEvent e) {
		adaptee.JTable1_mouseClicked(e);
	}
}
