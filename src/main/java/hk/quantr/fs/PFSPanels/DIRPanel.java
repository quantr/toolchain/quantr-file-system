package hk.quantr.fs.PFSPanels;
import hk.quantr.fs.Dialogs.BlockDialog;
import hk.quantr.fs.Dialogs.BlockDialogPanel;
import hk.quantr.fs.Global;
import hk.quantr.fs.MainFrame;
import hk.quantr.fs.PExceptionDialog;
import hk.quantr.fs.PFSCommonLib;
import hk.quantr.fs.PFSImageConstants;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;


/**
 * <p>
 * Title: PFS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Petersoft
 * </p>
 *
 * @author Peter
 * @version 2005
 */
public class DIRPanel extends JPanel {
	JPanel pIDPanel = new JPanel();
	JLabel pIDLabel = new JLabel();
	JLabel pIDContentLabel = new JLabel();
	JPanel pPermissionPanel = new JPanel();
	JLabel pPermissionLabel = new JLabel();
	JLabel pPermissionContentLabel = new JLabel();
	JPanel pDirectoryNamePanel = new JPanel();
	JLabel pDirectoryNameLabel = new JLabel();
	JLabel pDirectoryNameContentLabel = new JLabel();
	JPanel pDirectoryIndirectBlockPanel = new JPanel();
	JLabel pDirectoryIndirectBlockLabel = new JLabel();
	JButton pDirectoryIndirectBlockContentLabel = new JButton();
	JPanel pNumberOfDirectoryPanel = new JPanel();
	JLabel pNumberOfDirectoryLabel = new JLabel();
	JLabel pNumberOfDirectoryContentLabel = new JLabel();
	JPanel pCreateTimePanel = new JPanel();
	JLabel pCreateTimeLabel = new JLabel();
	JLabel pCreateTimeContentLabel = new JLabel();
	JPanel pLastModifiedTimePanel = new JPanel();
	JLabel pLastModifiedTimeLabel = new JLabel();
	JLabel pLastModifiedTimeContentLabel = new JLabel();
	JPanel pNumberOfFilePanel = new JPanel();
	JLabel pNumberOfFileLabel = new JLabel();
	JLabel pNumberOfFileContentLabel = new JLabel();
	BorderLayout borderLayout1 = new BorderLayout();
	BorderLayout borderLayout2 = new BorderLayout();
	BorderLayout borderLayout3 = new BorderLayout();
	BorderLayout borderLayout4 = new BorderLayout();
	BorderLayout borderLayout5 = new BorderLayout();
	BorderLayout borderLayout6 = new BorderLayout();
	BorderLayout borderLayout7 = new BorderLayout();
	BorderLayout borderLayout8 = new BorderLayout();
	JPanel pFileOrDirectoryLinkPanel = new JPanel();
	JLabel pFileOrDirectoryLinkLabel = new JLabel();
	BorderLayout borderLayout9 = new BorderLayout();
	byte data[];
	Box box1 = Box.createVerticalBox();
	BorderLayout borderLayout10 = new BorderLayout();
	JScrollPane JScrollPane1 = new JScrollPane();
	JTable JTable1 = new JTable();
	NonEditableDefaultTableModel tableModel = new NonEditableDefaultTableModel();
	ImageIcon mainMenuDirectoryIcon = new ImageIcon(MainFrame.class.getResource("images/mainMenu/directory.png"));
	BlockDialog jBlockDialog;
	JPanel JPanel1 = new JPanel();
	JCheckBox pShowNullCheckBox = new JCheckBox();

	public DIRPanel() {
		super();
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public DIRPanel(BlockDialog jBlockDialog, byte data[]) {
		super();
		this.jBlockDialog = jBlockDialog;
		this.data = data;
		try {
			jbInit();
			this.pIDContentLabel.setText(String.valueOf(data[0]) + "," + String.valueOf(data[1]) + "," + String.valueOf(data[2]));
			this.pIDContentLabel.setIcon(mainMenuDirectoryIcon);
			this.pDirectoryNameContentLabel.setText(new String(data).substring(3, 503));
			long numberOfFile = (long) (((long) (data[503]) & 0xff) + (((long) (data[504]) & 0xff) << 8) + (((long) (data[505]) & 0xff) << 16)
					+ (((long) (data[506]) & 0xff) << 24));
			this.pNumberOfFileContentLabel.setText(String.valueOf(numberOfFile));
			long numberOfDirectory = (long) (((long) (data[507]) & 0xff) + (((long) (data[508]) & 0xff) << 8) + (((long) (data[509]) & 0xff) << 16)
					+ (((long) (data[510]) & 0xff) << 24));
			this.pNumberOfDirectoryContentLabel.setText(String.valueOf(numberOfDirectory));
			this.pPermissionContentLabel.setText(String.valueOf((char) data[511]) + String.valueOf((char) data[512]) + String.valueOf((char) data[513])
					+ String.valueOf((char) data[514]) + String.valueOf((char) data[515]) + String.valueOf((char) data[516]) + String.valueOf((char) data[517])
					+ String.valueOf((char) data[518]) + String.valueOf((char) data[519]));
			long createTime = (long) ((long) ((data[520]) & 0xff) + ((long) ((data[521]) & 0xff) << 8) + ((long) ((data[522]) & 0xff) << 16) + ((long) ((data[523]) & 0xff) << 24)
					+ ((long) ((data[524]) & 0xff) << 32) + ((long) ((data[525]) & 0xff) << 40) + ((long) ((data[526]) & 0xff) << 48) + (((long) (data[527]) & 0xff) << 56));
			this.pCreateTimeContentLabel.setText(String.valueOf(createTime));
			long lastModifiedTime = (long) (((long) (data[528]) & 0xff) + (((long) (data[529]) & 0xff) << 8) + (((long) (data[530]) & 0xff) << 16)
					+ (((long) (data[531]) & 0xff) << 24) + (((long) (data[532]) & 0xff) << 32) + (((long) (data[533]) & 0xff) << 40) + (((long) (data[534]) & 0xff) << 48)
					+ (((long) (data[535]) & 0xff) << 56));
			this.pLastModifiedTimeContentLabel.setText(String.valueOf(lastModifiedTime));
			long directoryIndirectBlock = (long) (((long) (data[536]) & 0xff) + (((long) (data[537]) & 0xff) << 8) + (((long) (data[538]) & 0xff) << 16)
					+ (((long) (data[539]) & 0xff) << 24) + (((long) (data[540]) & 0xff) << 32) + (((long) (data[541]) & 0xff) << 40) + (((long) (data[542]) & 0xff) << 48)
					+ (((long) (data[543]) & 0xff) << 56));
			this.pDirectoryIndirectBlockContentLabel.setText(String.valueOf(directoryIndirectBlock));
			addLinks();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void addLinks() {
		tableModel = new NonEditableDefaultTableModel();
		tableModel.addColumn("No. ");
		tableModel.addColumn("Type ");
		tableModel.addColumn("Block number");
		for (int x = 544, z = 0; x < (data.length - 9); x += 9, z++) {
			long link = (long) ((long) ((data[x + 1]) & 0xff) + ((long) ((data[x + 2]) & 0xff) << 8) + ((long) ((data[x + 3]) & 0xff) << 16) + ((long) ((data[x + 4]) & 0xff) << 24)
					+ ((long) ((data[x + 5]) & 0xff) << 32) + ((long) ((data[x + 6]) & 0xff) << 40) + ((long) ((data[x + 7]) & 0xff) << 48)
					+ ((long) ((data[x + 8]) & 0xff) << 56));
			if (pShowNullCheckBox.isSelected() || (!pShowNullCheckBox.isSelected() && link != 0)) {
				if (data[x] == 0) {
					tableModel.addRow(new Object[] { z, "file", String.valueOf(link) });
				} else {
					tableModel.addRow(new Object[] { z, "dir", String.valueOf(link) });
				}
			}
		}
		JTable1.setModel(tableModel);

	}

	private void jbInit() throws Exception {
		box1 = Box.createVerticalBox();
		pIDLabel.setPreferredSize(new Dimension(200, 20));
		pIDLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pIDLabel.setText("ID : ");
		pPermissionLabel.setPreferredSize(new Dimension(200, 20));
		pPermissionLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pPermissionLabel.setText("Permission : ");
		pPermissionContentLabel.setToolTipText("");
		pDirectoryNameLabel.setPreferredSize(new Dimension(200, 20));
		pDirectoryNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pDirectoryNameLabel.setText("Directory Name : ");
		pDirectoryIndirectBlockLabel.setPreferredSize(new Dimension(200, 20));
		pDirectoryIndirectBlockLabel.setToolTipText("");
		pDirectoryIndirectBlockLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pDirectoryIndirectBlockLabel.setText("Directory indirect block : ");
		pNumberOfDirectoryLabel.setPreferredSize(new Dimension(200, 20));
		pNumberOfDirectoryLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pNumberOfDirectoryLabel.setText("Number of directory : ");
		pCreateTimeLabel.setPreferredSize(new Dimension(200, 20));
		pCreateTimeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pCreateTimeLabel.setText("Create Time :");
		pLastModifiedTimeLabel.setPreferredSize(new Dimension(200, 20));
		pLastModifiedTimeLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pLastModifiedTimeLabel.setText("Last modified time : ");
		pNumberOfFileLabel.setPreferredSize(new Dimension(200, 20));
		pNumberOfFileLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pNumberOfFileLabel.setText("Number of file : ");
		pIDPanel.setLayout(borderLayout1);
		pPermissionPanel.setLayout(borderLayout2);
		pDirectoryNamePanel.setLayout(borderLayout3);
		pDirectoryIndirectBlockPanel.setLayout(borderLayout4);
		pNumberOfDirectoryPanel.setLayout(borderLayout5);
		pCreateTimePanel.setLayout(borderLayout6);
		pLastModifiedTimePanel.setLayout(borderLayout7);
		pNumberOfFilePanel.setLayout(borderLayout8);
		this.setLayout(borderLayout10);
		pFileOrDirectoryLinkLabel.setPreferredSize(new Dimension(200, 20));
		pFileOrDirectoryLinkLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pFileOrDirectoryLinkLabel.setText("File or Directory link : ");
		pFileOrDirectoryLinkPanel.setLayout(borderLayout9);
		JTable1.addMouseListener(new DIRPanel_JTable1_mouseAdapter(this));
		pShowNullCheckBox.setText("Show null");
		pShowNullCheckBox.addActionListener(new DIRPanel_pShowNullCheckBox_actionAdapter(this));
		pDirectoryIndirectBlockContentLabel.addActionListener(new DIRPanel_pDirectoryIndirectBlockContentLabel_actionAdapter(this));
		pIDPanel.add(pIDLabel, java.awt.BorderLayout.WEST);
		pIDPanel.add(pIDContentLabel, java.awt.BorderLayout.CENTER);
		pPermissionPanel.add(pPermissionLabel, java.awt.BorderLayout.WEST);
		pPermissionPanel.add(pPermissionContentLabel, java.awt.BorderLayout.CENTER);

		pDirectoryNamePanel.add(pDirectoryNameLabel, java.awt.BorderLayout.WEST);
		pDirectoryNamePanel.add(pDirectoryNameContentLabel, java.awt.BorderLayout.CENTER);

		pDirectoryIndirectBlockPanel.add(pDirectoryIndirectBlockLabel, java.awt.BorderLayout.WEST);
		pDirectoryIndirectBlockPanel.add(pDirectoryIndirectBlockContentLabel, java.awt.BorderLayout.CENTER);

		pNumberOfDirectoryPanel.add(pNumberOfDirectoryLabel, java.awt.BorderLayout.WEST);
		pNumberOfDirectoryPanel.add(pNumberOfDirectoryContentLabel, java.awt.BorderLayout.CENTER);

		pCreateTimePanel.add(pCreateTimeLabel, java.awt.BorderLayout.WEST);
		pCreateTimePanel.add(pCreateTimeContentLabel, java.awt.BorderLayout.CENTER);

		pLastModifiedTimePanel.add(pLastModifiedTimeLabel, java.awt.BorderLayout.WEST);
		pLastModifiedTimePanel.add(pLastModifiedTimeContentLabel, java.awt.BorderLayout.CENTER);

		pNumberOfFilePanel.add(pNumberOfFileLabel, java.awt.BorderLayout.WEST);
		pNumberOfFilePanel.add(pNumberOfFileContentLabel, java.awt.BorderLayout.CENTER);

		box1.add(pDirectoryNamePanel);
		box1.add(pIDPanel);
		box1.add(pNumberOfFilePanel);
		box1.add(pNumberOfDirectoryPanel);
		box1.add(pPermissionPanel);
		box1.add(pCreateTimePanel);
		box1.add(pLastModifiedTimePanel);
		box1.add(pDirectoryIndirectBlockPanel);
		box1.add(pFileOrDirectoryLinkPanel);
		pFileOrDirectoryLinkPanel.add(pFileOrDirectoryLinkLabel, java.awt.BorderLayout.WEST);
		this.add(box1, java.awt.BorderLayout.CENTER);
		pFileOrDirectoryLinkPanel.add(JScrollPane1, java.awt.BorderLayout.CENTER);
		JScrollPane1.getViewport().add(JTable1);
		pFileOrDirectoryLinkPanel.add(JPanel1, java.awt.BorderLayout.SOUTH);
		JPanel1.add(pShowNullCheckBox);
		JTable1.setModel(tableModel);
	}

	public void JTable1_mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			long blockNo = Long.parseLong(JTable1.getValueAt(JTable1.getSelectedRow(), 2).toString());
			if (blockNo != 0) {
				long offset = PFSImageConstants.partitionOffset + PFSImageConstants.superBlockSize + blockNo * PFSImageConstants.blockSize;
				byte data[] = PFSCommonLib.readFile(PFSImageConstants.filename, offset, PFSImageConstants.blockSize);
				if (data[0] == 'D' && data[1] == 'I' && data[2] == 'R') {
					jBlockDialog.jTabbedPane1.add(new BlockDialogPanel(jBlockDialog, new DIRPanel(jBlockDialog, data), blockNo), "Dir:" + blockNo);
					jBlockDialog.setTitle("Block Number : " + blockNo);
					jBlockDialog.jTabbedPane1.setSelectedIndex(jBlockDialog.jTabbedPane1.getTabCount() - 1);
				} else if (data[0] == 'F' && data[1] == 'I' && data[2] == 'L' && data[3] == 'E') {
					jBlockDialog.jTabbedPane1.add(new BlockDialogPanel(jBlockDialog, new FILEPanel(jBlockDialog, data), blockNo), "File:" + blockNo);
					jBlockDialog.setTitle("Block Number : " + blockNo);
					jBlockDialog.jTabbedPane1.setSelectedIndex(jBlockDialog.jTabbedPane1.getTabCount() - 1);
				}
				// new JBlockDialog(jBlockDialog, blockNo,
				// data).setVisible(true);
			}
		}
	}

	public void pShowNullCheckBox_actionPerformed(ActionEvent e) {
		addLinks();
	}

	public void pDirectoryIndirectBlockContentLabel_actionPerformed(ActionEvent e) {
		if (!pDirectoryIndirectBlockContentLabel.getText().equals("0")) {
			long blockNo = Long.valueOf(pDirectoryIndirectBlockContentLabel.getText());
			long offset = PFSImageConstants.partitionOffset + PFSImageConstants.superBlockSize + blockNo * PFSImageConstants.blockSize;
			byte data[] = PFSCommonLib.readFile(PFSImageConstants.filename, offset, PFSImageConstants.blockSize);
			jBlockDialog.jTabbedPane1.add(new BlockDialogPanel(jBlockDialog, new DIBPanel(jBlockDialog, data), blockNo), "DIB:" + pDirectoryIndirectBlockContentLabel.getText());
			jBlockDialog.setTitle("Block Number : " + blockNo);
			jBlockDialog.jTabbedPane1.setSelectedIndex(jBlockDialog.jTabbedPane1.getTabCount() - 1);
			// new JBlockDialog(jBlockDialog,
			// pDirectoryIndirectBlockContentLabel.getText(),
			// data).setVisible(true);
		}
	}
}

class DIRPanel_pShowNullCheckBox_actionAdapter implements ActionListener {
	private DIRPanel adaptee;

	DIRPanel_pShowNullCheckBox_actionAdapter(DIRPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pShowNullCheckBox_actionPerformed(e);
	}
}

class DIRPanel_pDirectoryIndirectBlockContentLabel_actionAdapter implements ActionListener {
	private DIRPanel adaptee;

	DIRPanel_pDirectoryIndirectBlockContentLabel_actionAdapter(DIRPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pDirectoryIndirectBlockContentLabel_actionPerformed(e);
	}
}

class DIRPanel_JTable1_mouseAdapter extends MouseAdapter {
	private DIRPanel adaptee;

	DIRPanel_JTable1_mouseAdapter(DIRPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void mouseClicked(MouseEvent e) {
		adaptee.JTable1_mouseClicked(e);
	}
}
