package hk.quantr.fs.PFSPanels;

import hk.quantr.fs.Dialogs.BlockDialog;
import hk.quantr.fs.Dialogs.BlockDialogPanel;
import hk.quantr.fs.Global;
import hk.quantr.fs.PExceptionDialog;
import hk.quantr.fs.PFSCommonLib;
import hk.quantr.fs.PFSImageConstants;
import hk.quantr.fs.PFSSettingConstants;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;

/**
 * <p>
 * Title: PFS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Petersoft
 * </p>
 *
 * @author Peter
 * @version 2005
 */
public class DIBPanel extends JPanel {
	JScrollPane JScrollPane1 = new JScrollPane();
	BorderLayout borderLayout2 = new BorderLayout();
	JTable JTable1 = new JTable();
	byte data[];
	JPanel pIDPanel = new JPanel();
	JLabel pIDLabel = new JLabel();
	JLabel pIDContentLabel = new JLabel();
	BorderLayout borderLayout1 = new BorderLayout();
	NonEditableDefaultTableModel tableModel = new NonEditableDefaultTableModel();
	JPanel JPanel1 = new JPanel();
	JLabel JLabel1 = new JLabel();
	JButton pDIBToolBarButton = new JButton();
	BorderLayout borderLayout3 = new BorderLayout();
	BlockDialog jBlockDialog;

	public DIBPanel() {
		super();
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public DIBPanel(BlockDialog jBlockDialog, byte data[]) {
		super();
		this.jBlockDialog = jBlockDialog;
		this.data = data;
		try {
			jbInit();
			this.pIDContentLabel.setText(String.valueOf(data[0]) + "," + String.valueOf(data[1]) + "," + String.valueOf(data[2]));

			tableModel.addColumn("No.");
			tableModel.addColumn("Type");
			tableModel.addColumn("Block number");
			for (int x = 3, z = 0; x < data.length - 9 - 8; x += 9, z++) {
				long link = (long) (((long) (data[x + 1]) & 0xff) + (((long) (data[x + 2]) & 0xff) << 8) + (((long) (data[x + 3]) & 0xff) << 16)
						+ (((long) (data[x + 4]) & 0xff) << 24) + (((long) (data[x + 5]) & 0xff) << 32) + (((long) (data[x + 6]) & 0xff) << 40)
						+ (((long) (data[x + 7]) & 0xff) << 48) + (((long) (data[x + 8]) & 0xff) << 56));
				if (data[x] == 0) {
					tableModel.addRow(new Object[] { z, "File", String.valueOf(link) });
				} else {
					tableModel.addRow(new Object[] { z, "Directory", String.valueOf(link) });
				}
			}

			// DIB
			long directoryIndirectBlock = (long) (((long) (data[PFSSettingConstants.blockSize - 8]) & 0xff) + (((long) (data[PFSSettingConstants.blockSize - 7]) & 0xff) << 8)
					+ (((long) (data[PFSSettingConstants.blockSize - 6]) & 0xff) << 16) + (((long) (data[PFSSettingConstants.blockSize - 5]) & 0xff) << 24)
					+ (((long) (data[PFSSettingConstants.blockSize - 4]) & 0xff) << 32) + (((long) (data[PFSSettingConstants.blockSize - 3]) & 0xff) << 40)
					+ (((long) (data[PFSSettingConstants.blockSize - 2]) & 0xff) << 48) + (((long) (data[PFSSettingConstants.blockSize - 1]) & 0xff) << 56));
			pDIBToolBarButton.setText(String.valueOf(directoryIndirectBlock));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void jbInit() throws Exception {
		this.setLayout(borderLayout2);
		pIDLabel.setPreferredSize(new Dimension(200, 20));
		pIDLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pIDLabel.setText("ID : ");
		pIDContentLabel.setPreferredSize(new Dimension(200, 20));
		pIDContentLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		pIDPanel.setLayout(borderLayout1);
		JLabel1.setPreferredSize(new Dimension(200, 20));
		JLabel1.setToolTipText("");
		JLabel1.setHorizontalAlignment(SwingConstants.RIGHT);
		JLabel1.setText("Directory indirect block : ");
		JPanel1.setLayout(borderLayout3);
		pDIBToolBarButton.addActionListener(new DIBPanel_pDIBToolBarButton_actionAdapter(this));
		this.add(JScrollPane1, java.awt.BorderLayout.CENTER);
		JScrollPane1.getViewport().add(JTable1);
		pIDPanel.add(pIDContentLabel, java.awt.BorderLayout.CENTER);
		pIDPanel.add(pIDLabel, java.awt.BorderLayout.WEST);
		this.add(pIDPanel, java.awt.BorderLayout.NORTH);
		this.add(JPanel1, java.awt.BorderLayout.SOUTH);
		JPanel1.add(JLabel1, java.awt.BorderLayout.WEST);
		JPanel1.add(pDIBToolBarButton, java.awt.BorderLayout.CENTER);
		JTable1.setModel(tableModel);
	}

	public void pDIBToolBarButton_actionPerformed(ActionEvent e) {
		if (!pDIBToolBarButton.getText().equals("0")) {
			long blockNo = Long.valueOf(pDIBToolBarButton.getText());
			long offset = PFSImageConstants.partitionOffset + PFSImageConstants.superBlockSize + blockNo * PFSImageConstants.blockSize;
			byte data[] = PFSCommonLib.readFile(PFSImageConstants.filename, offset, PFSImageConstants.blockSize);
			jBlockDialog.jTabbedPane1.add(new BlockDialogPanel(jBlockDialog, new DIBPanel(jBlockDialog, data), blockNo), "DIB:" + pDIBToolBarButton.getText());
			jBlockDialog.setTitle("Block Number : " + blockNo);
			jBlockDialog.jTabbedPane1.setSelectedIndex(jBlockDialog.jTabbedPane1.getTabCount() - 1);
		}
	}
}

class DIBPanel_pDIBToolBarButton_actionAdapter implements ActionListener {
	private DIBPanel adaptee;

	DIBPanel_pDIBToolBarButton_actionAdapter(DIBPanel adaptee) {
		this.adaptee = adaptee;
	}

	public void actionPerformed(ActionEvent e) {
		adaptee.pDIBToolBarButton_actionPerformed(e);
	}
}
