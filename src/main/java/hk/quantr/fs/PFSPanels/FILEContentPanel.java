package hk.quantr.fs.PFSPanels;

import hk.quantr.fs.Global;
import hk.quantr.fs.PExceptionDialog;
import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * <p>
 * Title: PFS
 * </p>
 *
 * <p>
 * Description:
 * </p>
 *
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 *
 * <p>
 * Company: Petersoft
 * </p>
 *
 * @author Peter
 * @version 2005
 */
public class FILEContentPanel extends JPanel {
	JScrollPane JScrollPane1 = new JScrollPane();
	BorderLayout borderLayout2 = new BorderLayout();
	JTextArea JTextArea1 = new JTextArea();

	public FILEContentPanel() {
		super();
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public FILEContentPanel(byte data[]) {
		super();
		try {
			jbInit();
			JTextArea1.setText(new String(data).trim());
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void jbInit() throws Exception {
		this.setLayout(borderLayout2);
		this.add(JScrollPane1, java.awt.BorderLayout.CENTER);
		JScrollPane1.getViewport().add(JTextArea1);
	}

	public JTextArea getTextArea() {
		return JTextArea1;
	}
}
