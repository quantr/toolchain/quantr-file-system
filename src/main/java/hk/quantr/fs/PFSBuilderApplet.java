package hk.quantr.fs;

import java.awt.BorderLayout;

import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;

/**
 * @author Peter
 * @version 2005
 */

public class PFSBuilderApplet extends JApplet {
	boolean isStandalone = false;
	BorderLayout borderLayout1 = new BorderLayout();
	BorderLayout borderLayout2 = new BorderLayout();
	JScrollPane JScrollPane1 = new JScrollPane();
	JLabel JLabel1 = new JLabel();

	// Get a parameter value
	// public String getParameter(String key, String def) {
	// return isStandalone ? System.getProperty(key, def) :
	// (getParameter(key) != null ? getParameter(key) : def);
	// }

	// Construct the applet
	public PFSBuilderApplet() {
	}

	// Initialize the applet
	public void init() {
		try {
			jbInit();
			MainFrame frame = new MainFrame();
			frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
			frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
			JScrollPane1.setVisible(true);
			JLabel1.setText(e.toString());
		}
	}

	// Component initialization
	private void jbInit() throws Exception {
		this.setLayout(borderLayout2);
		this.add(JScrollPane1, java.awt.BorderLayout.CENTER);
		JScrollPane1.getViewport().add(JLabel1);
		JScrollPane1.setVisible(false);
		// new JDialog(this).setVisible(true);
	}

	// Get Applet information
	public String getAppletInfo() {
		return "Applet Information";
	}

	// Get parameter info
	public String[][] getParameterInfo() {
		return null;
	}
}
