package hk.quantr.fs;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * This code was edited or generated using CloudGarden's Jigloo SWT/Swing GUI
 * Builder, which is free for non-commercial use. If Jigloo is being used
 * commercially (ie, by a corporation, company or business for any purpose
 * whatever) then you should purchase a license for each developer using Jigloo.
 * Please visit www.cloudgarden.com for details. Use of Jigloo implies
 * acceptance of these licensing terms. A COMMERCIAL LICENSE HAS NOT BEEN
 * PURCHASED FOR THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED LEGALLY FOR
 * ANY CORPORATE OR COMMERCIAL PURPOSE.
 */
public class AutoUpdater extends javax.swing.JFrame {
	private JButton pUpdateButton;
	private JPanel JPanel1;
	private JProgressBar JProgressBar1;

	/**
	 * Auto-generated main method to display this JFrame
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				AutoUpdater inst = new AutoUpdater();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
			}
		});
	}

	public AutoUpdater() {
		super();
		initGUI();
	}

	private void initGUI() {
		try {
			setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
			this.setTitle("Auto Updater");
			{
				JPanel1 = new JPanel();
				BorderLayout JPanel1Layout = new BorderLayout();
				JPanel1.setLayout(JPanel1Layout);
				getContentPane().add(JPanel1, BorderLayout.EAST);
				JPanel1.setBounds(41, 49, 10, 10);
				{
					pUpdateButton = new JButton();
					JPanel1.add(pUpdateButton, BorderLayout.CENTER);
					pUpdateButton.setText("Update");
				}
			}
			{
				JProgressBar1 = new JProgressBar();
				getContentPane().add(JProgressBar1, BorderLayout.CENTER);
			}
			pack();
			setSize(400, 100);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
