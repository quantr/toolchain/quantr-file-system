package hk.quantr.fs;

public class Library {
	public Library() {
	}

	public static boolean checkMagicNumber(byte superBlock[], int srcPos) {
		try {
			if (superBlock[0 + srcPos] == 'S' && superBlock[1 + srcPos] == 'B' && superBlock[2 + srcPos] == 'P' && superBlock[3 + srcPos] == 'E' && superBlock[4 + srcPos] == 'T'
					&& superBlock[5 + srcPos] == 'E' && superBlock[6 + srcPos] == 'R' && superBlock[7 + srcPos] == ' ' && superBlock[8 + srcPos] == 'F'
					&& superBlock[9 + srcPos] == 'I' && superBlock[10 + srcPos] == 'L' && superBlock[11 + srcPos] == 'E' && superBlock[12 + srcPos] == ' '
					&& superBlock[13 + srcPos] == 'S' && superBlock[14 + srcPos] == 'Y' && superBlock[15 + srcPos] == 'S' && superBlock[16 + srcPos] == 'T'
					&& superBlock[17 + srcPos] == 'E' && superBlock[18 + srcPos] == 'M') {
				return true;
			} else {
				return false;
			}
		} catch (Exception ex) {
			return false;
		}
	}

}
