package hk.quantr.fs;

import java.awt.Component;
import java.awt.Point;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import hk.quantr.fs.Dialogs.WriteDialog_TableModel;

public class PFSWriteImageAlgorithm {
	long NumberOfBlock;
	long FreeAddressBlockCapacity;
	long NumberOfFreeAddressBlock;
	long NumberOfLinkPerDirBlock;
	long NumberOfLinkPerFileBlock;
	long NumberOfLinkPerDirectoryIndirectBlock;
	JTable JTable;
	JScrollPane JScrollPane;
	JProgressBar JProgressBar;
	File rootNode;
	JButton pPauseButton;
	WriteDialog_TableModel JTableModel;
	Component parent;
	public boolean shouldStop;
	int numberOfFile;
	int numberOfFileWritten;
	JLabel pInfoLabel;
	RandomAccessFile bw;
	byte fabBuffer[][];
	boolean fabBufferUsed[];
	long writeFileIndirectBlock_TimeStamp;
	PIni ini;

	public PFSWriteImageAlgorithm() {
	}

	public void createPFS(File rootNode) {
		createPFS(null, null, null, null, null, rootNode, null, null);
	}

	public void createPFS(JDialog parent, JTable JTable, WriteDialog_TableModel JTableModel, JScrollPane JScrollPane, JProgressBar JProgressBar, File rootNode,
			JButton pPauseButton, JLabel pInfoLabel) {
		this.parent = parent;
		ini = new PIni(PFSSettingConstants.projectFile);
		init(JTable, JTableModel, JScrollPane, JProgressBar, rootNode, pPauseButton, pInfoLabel);
	}

	private void init(JTable JTable, WriteDialog_TableModel JTableModel, JScrollPane JScrollPane, JProgressBar JProgressBar, File rootNode, JButton pPauseButton,
			JLabel pInfoLabel) {
		this.JTable = JTable;
		this.JTableModel = JTableModel;
		this.JScrollPane = JScrollPane;
		this.JProgressBar = JProgressBar;
		this.rootNode = rootNode;
		this.pPauseButton = pPauseButton;
		this.pInfoLabel = pInfoLabel;
		start();
		if (pPauseButton != null) {
			pPauseButton.setVisible(false);
		}
	}

	private void start() {
		try {
			numberOfFileWritten = 0;
			if (!Global.GUI) {
				System.out.println("Calculate total number of file to compress to pfs image :");
			}
			numberOfFile = PFSCommonLib.numberOfFile(rootNode, Global.GUI);
			if (Global.GUI) {
				pInfoLabel.setText("");
				pPauseButton.setText("Pause");
				JProgressBar.setMaximum(numberOfFile);
				JProgressBar.setValue(0);
			}

			new File(PFSSettingConstants.filename).delete();
			bw = new RandomAccessFile(PFSSettingConstants.filename, "rw");

			if (!initImage()) {
				return;
			}

			// prepare
			showResult("Super Block size", PFSSettingConstants.superBlockSize);

			NumberOfBlock = (PFSSettingConstants.partitionSize - PFSSettingConstants.superBlockSize) / PFSSettingConstants.blockSize;
			showResult("Number Of Block", NumberOfBlock);

			FreeAddressBlockCapacity = PFSSettingConstants.blockSize - 3;
			showResult("Free Address Block Capacity", FreeAddressBlockCapacity);

			NumberOfFreeAddressBlock = NumberOfBlock / FreeAddressBlockCapacity;
			if (NumberOfBlock % FreeAddressBlockCapacity != 0) {
				NumberOfFreeAddressBlock++;
			}
			fabBuffer = new byte[(int) NumberOfFreeAddressBlock][PFSSettingConstants.blockSize];
			fabBufferUsed = new boolean[(int) NumberOfFreeAddressBlock];
			showResult("Number Of Free AddressBlock", NumberOfFreeAddressBlock);

			NumberOfLinkPerDirBlock = (PFSSettingConstants.blockSize - 544) / 9;
			showResult("Number Of link per directory block", NumberOfLinkPerDirBlock);

			NumberOfLinkPerFileBlock = (PFSSettingConstants.blockSize - 545) / 8;
			showResult("Number Of link per file block", NumberOfLinkPerFileBlock);

			NumberOfLinkPerDirectoryIndirectBlock = (PFSSettingConstants.blockSize - 8 - 3) / 9;
			showResult("Number Of link per directory indirect block", NumberOfLinkPerDirectoryIndirectBlock);

			// wrtie super block
			// ID
			byte superBlock[] = new byte[4096];
			superBlock[0] = 'S';
			superBlock[1] = 'B';

			// write magic number
			superBlock[2] = 'P';
			superBlock[3] = 'E';
			superBlock[4] = 'T';
			superBlock[5] = 'E';
			superBlock[6] = 'R';
			superBlock[7] = ' ';
			superBlock[8] = 'F';
			superBlock[9] = 'I';
			superBlock[10] = 'L';
			superBlock[11] = 'E';
			superBlock[12] = ' ';
			superBlock[13] = 'S';
			superBlock[14] = 'Y';
			superBlock[15] = 'S';
			superBlock[16] = 'T';
			superBlock[17] = 'E';
			superBlock[18] = 'M';

			// partition name
			byte c[] = PFSSettingConstants.partitionName.getBytes();
			for (int x = 0; x < 500; x++) {
				if (x < c.length) {
					superBlock[x + 19] = c[x];
				} else {
					superBlock[x + 19] = 0;
				}
			}

			// root directory link
			superBlock[520] = (byte) NumberOfFreeAddressBlock;
			superBlock[521] = (byte) (NumberOfFreeAddressBlock >> 8);
			superBlock[522] = (byte) (NumberOfFreeAddressBlock >> 16);
			superBlock[523] = (byte) (NumberOfFreeAddressBlock >> 24);
			superBlock[524] = (byte) (NumberOfFreeAddressBlock >> 32);
			superBlock[525] = (byte) (NumberOfFreeAddressBlock >> 40);
			superBlock[526] = (byte) (NumberOfFreeAddressBlock >> 48);
			superBlock[527] = (byte) (NumberOfFreeAddressBlock >> 56);

			// create time
			superBlock[528] = 0;
			superBlock[529] = 0;
			superBlock[530] = 0;
			superBlock[531] = 0;
			superBlock[532] = 0;
			superBlock[533] = 0;
			superBlock[534] = 0;
			superBlock[535] = 0;

			// last modified time
			superBlock[536] = 0;
			superBlock[537] = 0;
			superBlock[538] = 0;
			superBlock[539] = 0;
			superBlock[540] = 0;
			superBlock[541] = 0;
			superBlock[542] = 0;
			superBlock[543] = 0;

			// block size
			superBlock[544] = (byte) (PFSSettingConstants.blockSize / 1024);
			superBlock[545] = (byte) ((PFSSettingConstants.blockSize / 1024) >> 8);

			// number of free address blcok
			superBlock[546] = (byte) NumberOfFreeAddressBlock;
			superBlock[547] = (byte) (NumberOfFreeAddressBlock >> 8);
			superBlock[548] = (byte) (NumberOfFreeAddressBlock >> 16);
			superBlock[549] = (byte) (NumberOfFreeAddressBlock >> 24);
			superBlock[550] = (byte) (NumberOfFreeAddressBlock >> 32);
			superBlock[551] = (byte) (NumberOfFreeAddressBlock >> 40);
			superBlock[552] = (byte) (NumberOfFreeAddressBlock >> 48);
			superBlock[553] = (byte) (NumberOfFreeAddressBlock >> 56);

			bw.seek(PFSSettingConstants.partitionOffset);
			bw.write(superBlock, 0, 4096);
			// finish write super block

			// write Free Address Block
			// byte freeAddressBlock[] = new
			// byte[PFSSettingConstants.blockSize];
			// long index = 0;
			for (int z = 0; z < NumberOfFreeAddressBlock; z++) {
				// freeAddressBlock[0] = 'F';
				// freeAddressBlock[1] = 'A';
				// freeAddressBlock[2] = 'B';
				// for (int x = 3; x < PFSSettingConstants.blockSize; x++) {
				// if (index < NumberOfFreeAddressBlock) {
				// freeAddressBlock[x] = 1;
				// } else {
				// freeAddressBlock[x] = 0;
				// }
				// index++;
				// }
				// bw.write(freeAddressBlock);
				allocateFreeBlock();
			}
			// end write Free Address Block

			// write File and directory
			long freeBlockNumber = allocateFreeBlock();
			if (freeBlockNumber == -1) {
				runOutFreeBlock();
				return;
			}

			if (enumerateTree(freeBlockNumber, rootNode)) {
				this.showResult("write to pfs", "successfully");
			} else {
				this.showResult("write to pfs", "fail");
			}
			// end write File and directory

			// fill in the unused block
			// writeUnusedBlock();

			// write FAB buffer
			writeFABBuffer();

			if (PFSSettingConstants.trimto != -1) {
				System.out.println("trim to file size : " + PFSSettingConstants.trimto);
				bw.setLength(PFSSettingConstants.trimto);
			}

			bw.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
			if (Global.stopWhenException) {
				System.exit(0);
			}
		}
	}

	private boolean writeFABBuffer() {
		try {
			for (long x = 0; x < NumberOfFreeAddressBlock; x++) {
				fabBuffer[(int) x][0] = 'F';
				fabBuffer[(int) x][1] = 'A';
				fabBuffer[(int) x][2] = 'B';
				bw.seek(PFSSettingConstants.partitionOffset + PFSSettingConstants.superBlockSize + x * PFSSettingConstants.blockSize); // skip super block
				bw.write(fabBuffer[(int) x]);
			}
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
			if (Global.stopWhenException) {
				System.exit(0);
			}
			return false;
		}
	}

	private boolean initImage() {
		try {
			showResult("Init " + (PFSSettingConstants.partitionSize + PFSSettingConstants.partitionOffset) / 1024 / 1024 + "MB image", "");

			// int bufferSize = 1024 * 1024;
			// if (PFSBuilder.Globals.GUI) {
			// JProgressBar.setMaximum((long) (PFSSettingConstants.partitionSize
			// +
			// PFSSettingConstants.partitionOffset / bufferSize));
			// }
			// byte buffer[] = new byte[bufferSize];

			bw.seek(PFSSettingConstants.partitionSize + PFSSettingConstants.partitionOffset - 1);
			bw.write(0);
			// for (int x = 0;
			// x <
			// (PFSSettingConstants.partitionSize +
			// PFSSettingConstants.partitionOffset) /
			// bufferSize;
			// x++) {
			// if (shouldStop) {
			// bo2.close();
			// return false;
			// }
			//
			// if (PFSBuilder.Globals.GUI) {
			// while (pPauseButton.getText().equals("Resume")) {
			// Thread.currentThread().sleep(1000);\nThread.sleep(1000);
			// }
			// }
			// bo2.write(buffer);
			//
			// if (PFSBuilder.Globals.GUI) {
			// JProgressBar.setValue(x);
			// JTableModel.setValueAt(x + 1 + " MB written",
			// JTableModel.getRowCount() - 1, 1);
			// } else {
			// System.out.print(
			// "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b");
			// System.out.print(x + 1 + " MB written");
			// }
			// }

			if (Global.GUI) {
				JTableModel.setValueAt((PFSSettingConstants.partitionSize + PFSSettingConstants.partitionOffset) + " bytes written", JTableModel.getRowCount() - 1, 1);
			} else {
				//System.out.println();
			}

			// FileOutputStream fout = new
			// FileOutputStream(PFSConstants.filename);
			// FileChannel fc = fout.getChannel();
			// ByteBuffer buffer = ByteBuffer.allocateDirect(bufferSize);
			// for (int x = 0; x < (partitionSize +
			// PFSConstants.partitionOffset); x++) {
			// if (pStoJButton.getText().equals("OK")) {
			// return;
			// }
			// buffer.put((byte) 0);
			// if (x % (bufferSize - 1) == 0) {
			// buffer.flip();
			// fc.write(buffer);
			// buffer.clear();
			// JProgressBar.setValue(x / bufferSize);
			// JTableModel.setValueAt((x + 1)/bufferSize + "0 MB written",
			// JTableModel.getRowCount() - 1, 1);
			//
			// }
			// }
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
			if (Global.stopWhenException) {
				System.exit(0);
			}
			return false;
		}
	}

	private void writeUnusedBlock() {
		try {
			long freeBlockNumber;
			if (Global.GUI) {
				System.out.println(NumberOfFreeAddressBlock * FreeAddressBlockCapacity);
			}

			int x = 0;
			while ((freeBlockNumber = allocateFreeBlock()) != -1) {
				writeUnusedBlock(freeBlockNumber);
				x++;

				if (Global.GUI) {
					JTableModel.setValueAt(x + 1 + " unused blocks written", JTableModel.getRowCount() - 1, 1);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
			if (Global.stopWhenException) {
				System.exit(0);
			}
		}
	}

	private void runOutFreeBlock() {
		if (Global.GUI) {
			JOptionPane.showMessageDialog(parent, "Run out free block, partition is too small !!!", "Error", JOptionPane.ERROR_MESSAGE);
		} else {
			System.out.println("!!! Error : Run out free block, partition is too small !!!");
		}
	}

	private boolean enumerateTree(long currentBlockNumber, File node) {
		try {
			if (node.isDirectory()) {
				if (!writeDirectory(currentBlockNumber, node)) {
					return false;
				}
			} else {
				if (!writeFile(currentBlockNumber, node)) {
					return false;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
			if (Global.stopWhenException) {
				System.exit(0);
			}
		}
		return true;
	}

	private boolean writeUnusedBlock(long currentBlockNumber) {
		try {
			byte unusedBlock[] = new byte[PFSSettingConstants.blockSize];
			for (int x = 0; x < PFSSettingConstants.blockSize; x++) {
				unusedBlock[x] = 0;
			}
			bw.seek(PFSSettingConstants.partitionOffset + PFSSettingConstants.superBlockSize + currentBlockNumber * PFSSettingConstants.blockSize);
			bw.write(unusedBlock);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
			if (Global.stopWhenException) {
				System.exit(0);
			}
			return false;
		}
	}

	private boolean writeDirectory(long currentBlockNumber, File dir) {
		try {
			if (Global.GUI) {
				if (shouldStop) {
					return false;
				}

				while (pPauseButton.getText().equals("Resume")) {
					// Thread.currentThread().sleep(1000);
					Thread.sleep(1000);
				}
			}

			showResult("Write directory , block number=" + currentBlockNumber + " : " + dir.toString(), "");

			byte directoryBlock[] = new byte[PFSSettingConstants.blockSize];
			directoryBlock[0] = 'D';
			directoryBlock[1] = 'I';
			directoryBlock[2] = 'R';

			if (Global.GUI) {
				JTableModel.setValueAt("id written", JTableModel.getRowCount() - 1, 1);
			}

			// directory name
			byte c[] = dir.getName().getBytes();
			for (int x = 0; x < 500; x++) {
				if (x < c.length) {
					directoryBlock[x + 3] = c[x];
				} else {
					directoryBlock[x + 3] = 0;
				}
			}

			if (Global.GUI) {
				JTableModel.setValueAt("directory name written", JTableModel.getRowCount() - 1, 1);
			}

			// number of file
			File tempFiles[] = dir.listFiles(new fileOnlyFilter());
			if (tempFiles != null) {
				directoryBlock[503] = (byte) tempFiles.length;
				directoryBlock[504] = (byte) (tempFiles.length >> 8);
				directoryBlock[505] = (byte) (tempFiles.length >> 16);
				directoryBlock[506] = (byte) (tempFiles.length >> 24);
			}

			if (Global.GUI) {
				JTableModel.setValueAt("\"number of file\" field written", JTableModel.getRowCount() - 1, 1);
			}

			// number of sub directory
			tempFiles = dir.listFiles(new DirectoryOnlyFilter());
			if (tempFiles != null) {
				directoryBlock[507] = (byte) tempFiles.length;
				directoryBlock[508] = (byte) (tempFiles.length >> 8);
				directoryBlock[509] = (byte) (tempFiles.length >> 16);
				directoryBlock[510] = (byte) (tempFiles.length >> 24);
			}

			if (Global.GUI) {
				JTableModel.setValueAt("\"number of sub directory\" field written", JTableModel.getRowCount() - 1, 1);
			}

			// permission
			if (ini.getStringProperty(dir.toString(), "owner read", "").equals("true")) {
				directoryBlock[511] = 'r';
			} else {
				directoryBlock[511] = '-';
			}
			if (ini.getStringProperty(dir.toString(), "owner write", "").equals("true")) {
				directoryBlock[512] = 'w';
			} else {
				directoryBlock[512] = '-';
			}
			if (ini.getStringProperty(dir.toString(), "owner execute", "").equals("true")) {
				directoryBlock[513] = 'x';
			} else {
				directoryBlock[513] = '-';
			}
			if (ini.getStringProperty(dir.toString(), "group read", "").equals("true")) {
				directoryBlock[514] = 'r';
			} else {
				directoryBlock[514] = '-';
			}
			if (ini.getStringProperty(dir.toString(), "group write", "").equals("true")) {
				directoryBlock[515] = 'w';
			} else {
				directoryBlock[515] = '-';
			}
			if (ini.getStringProperty(dir.toString(), "group execute", "").equals("true")) {
				directoryBlock[516] = 'x';
			} else {
				directoryBlock[516] = '-';
			}
			if (ini.getStringProperty(dir.toString(), "other read", "").equals("true")) {
				directoryBlock[517] = 'r';
			} else {
				directoryBlock[517] = '-';
			}
			if (ini.getStringProperty(dir.toString(), "other write", "").equals("true")) {
				directoryBlock[518] = 'w';
			} else {
				directoryBlock[518] = '-';
			}
			if (ini.getStringProperty(dir.toString(), "other execute", "").equals("true")) {
				directoryBlock[519] = 'x';
			} else {
				directoryBlock[519] = '-';
			}

			if (Global.GUI) {
				JTableModel.setValueAt("permission written", JTableModel.getRowCount() - 1, 1);
			}

			// create time
			directoryBlock[520] = 0;
			directoryBlock[521] = 0;
			directoryBlock[522] = 0;
			directoryBlock[523] = 0;
			directoryBlock[524] = 0;
			directoryBlock[525] = 0;
			directoryBlock[526] = 0;
			directoryBlock[527] = 0;

			if (Global.GUI) {
				JTableModel.setValueAt("create time written", JTableModel.getRowCount() - 1, 1);
			}

			// last modified time
			directoryBlock[528] = 0;
			directoryBlock[529] = 0;
			directoryBlock[530] = 0;
			directoryBlock[531] = 0;
			directoryBlock[532] = 0;
			directoryBlock[533] = 0;
			directoryBlock[534] = 0;
			directoryBlock[535] = 0;

			if (Global.GUI) {
				JTableModel.setValueAt("last modified time written", JTableModel.getRowCount() - 1, 1);
			}

			// Directory indirect block
			long dibBlockNumber = -1;
			File files[] = dir.listFiles();
			if (files != null && files.length > NumberOfLinkPerDirBlock) {
				dibBlockNumber = allocateFreeBlock();
				if (dibBlockNumber == -1) {
					runOutFreeBlock();
					return false;
				}

				directoryBlock[536] = (byte) dibBlockNumber;
				directoryBlock[537] = (byte) (dibBlockNumber >> 8);
				directoryBlock[538] = (byte) (dibBlockNumber >> 16);
				directoryBlock[539] = (byte) (dibBlockNumber >> 24);
				directoryBlock[540] = (byte) (dibBlockNumber >> 32);
				directoryBlock[541] = (byte) (dibBlockNumber >> 40);
				directoryBlock[542] = (byte) (dibBlockNumber >> 48);
				directoryBlock[543] = (byte) (dibBlockNumber >> 56);
			} else {
				directoryBlock[536] = 0;
				directoryBlock[537] = 0;
				directoryBlock[538] = 0;
				directoryBlock[539] = 0;
				directoryBlock[540] = 0;
				directoryBlock[541] = 0;
				directoryBlock[542] = 0;
				directoryBlock[543] = 0;
			}

			if (Global.GUI) {
				JTableModel.setValueAt("\"directory indirect block\" field written", JTableModel.getRowCount() - 1, 1);
			}

			if (Global.GUI) {
				JTableModel.setValueAt("done", JTableModel.getRowCount() - 1, 1);
			}

			// File or Directory links
			for (int x = 0; x < NumberOfLinkPerDirBlock; x++) {
				if (shouldStop) {
					return false;
				}

				if (Global.GUI) {
					while (pPauseButton.getText().equals("Resume")) {
						// Thread.currentThread().sleep(1000);
						Thread.sleep(1000);
					}
				}

				if (files != null && x < files.length) {
					long freeBlockNumber2 = allocateFreeBlock();
					if (freeBlockNumber2 == -1) {
						runOutFreeBlock();
						return false;
					}

					if (files[x].isFile()) {
						directoryBlock[544 + x * 9] = 0;
						directoryBlock[545 + x * 9] = (byte) (freeBlockNumber2);
						directoryBlock[546 + x * 9] = (byte) (freeBlockNumber2 >> 8);
						directoryBlock[547 + x * 9] = (byte) (freeBlockNumber2 >> 16);
						directoryBlock[548 + x * 9] = (byte) (freeBlockNumber2 >> 24);
						directoryBlock[549 + x * 9] = (byte) (freeBlockNumber2 >> 32);
						directoryBlock[550 + x * 9] = (byte) (freeBlockNumber2 >> 40);
						directoryBlock[551 + x * 9] = (byte) (freeBlockNumber2 >> 48);
						directoryBlock[552 + x * 9] = (byte) (freeBlockNumber2 >> 56);
						if (!writeFile(freeBlockNumber2, files[x])) {
							this.showResult("write file : " + dir.listFiles()[x], "fail");
							return false;
						}
					} else if (files[x].isDirectory()) {
						directoryBlock[544 + x * 9] = 1;
						directoryBlock[545 + x * 9] = (byte) (freeBlockNumber2);
						directoryBlock[546 + x * 9] = (byte) (freeBlockNumber2 >> 8);
						directoryBlock[547 + x * 9] = (byte) (freeBlockNumber2 >> 16);
						directoryBlock[548 + x * 9] = (byte) (freeBlockNumber2 >> 24);
						directoryBlock[549 + x * 9] = (byte) (freeBlockNumber2 >> 32);
						directoryBlock[550 + x * 9] = (byte) (freeBlockNumber2 >> 40);
						directoryBlock[551 + x * 9] = (byte) (freeBlockNumber2 >> 48);
						directoryBlock[552 + x * 9] = (byte) (freeBlockNumber2 >> 56);
						if (!writeDirectory(freeBlockNumber2, files[x])) {
							this.showResult("write directory : " + files[x], "fail");
							return false;
						}
					}
				} else {
					directoryBlock[544 + x * 9] = 0;
					directoryBlock[545 + x * 9] = 0;
					directoryBlock[546 + x * 9] = 0;
					directoryBlock[547 + x * 9] = 0;
					directoryBlock[548 + x * 9] = 0;
					directoryBlock[549 + x * 9] = 0;
					directoryBlock[550 + x * 9] = 0;
					directoryBlock[551 + x * 9] = 0;
					directoryBlock[552 + x * 9] = 0;
				}
			}

			bw.seek(PFSSettingConstants.partitionOffset + PFSSettingConstants.superBlockSize + currentBlockNumber * PFSSettingConstants.blockSize);
			bw.write(directoryBlock);
			if (Global.GUI) {
				JProgressBar.setValue(JProgressBar.getValue() + 1);
			}

			// wrtie directory indirect block
			if (dibBlockNumber != -1) {
				if (!writeDirectoryIndirectBlock(dibBlockNumber, dir, (int) NumberOfLinkPerDirBlock)) {
					return false;
				}
			}
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
			if (Global.stopWhenException) {
				System.exit(0);
			}
			return false;
		}
	}

	private boolean writeDirectoryIndirectBlock(long currentBlockNumber, File dir, int arrayOffset) {
		try {
			byte dibBlock[] = new byte[PFSSettingConstants.blockSize];
			dibBlock[0] = 'D';
			dibBlock[1] = 'I';
			dibBlock[2] = 'B';

			// File or Directory links
			for (int x = 0; x < NumberOfLinkPerDirectoryIndirectBlock; x++) {
				if (shouldStop) {
					return false;
				}

				if (Global.GUI) {
					while (pPauseButton.getText().equals("Resume")) {
						// Thread.currentThread().sleep(1000);
						Thread.sleep(1000);
					}
				}
				if (arrayOffset + x < dir.listFiles().length) {
					long freeBlockNumber2 = allocateFreeBlock();
					if (freeBlockNumber2 == -1) {
						runOutFreeBlock();
						return false;
					}

					if (dir.listFiles()[x + arrayOffset].isFile()) {
						dibBlock[3 + x * 9] = 0;
						dibBlock[3 + x * 9 + 1] = (byte) (freeBlockNumber2);
						dibBlock[3 + x * 9 + 2] = (byte) (freeBlockNumber2 >> 8);
						dibBlock[3 + x * 9 + 3] = (byte) (freeBlockNumber2 >> 16);
						dibBlock[3 + x * 9 + 4] = (byte) (freeBlockNumber2 >> 24);
						dibBlock[3 + x * 9 + 5] = (byte) (freeBlockNumber2 >> 32);
						dibBlock[3 + x * 9 + 6] = (byte) (freeBlockNumber2 >> 40);
						dibBlock[3 + x * 9 + 7] = (byte) (freeBlockNumber2 >> 48);
						dibBlock[3 + x * 9 + 8] = (byte) (freeBlockNumber2 >> 56);
						if (!writeFile(freeBlockNumber2, dir.listFiles()[x + arrayOffset])) {
							return false;
						}
					} else {
						dibBlock[3 + x * 9] = 1;
						dibBlock[3 + x * 9 + 1] = (byte) (freeBlockNumber2);
						dibBlock[3 + x * 9 + 2] = (byte) (freeBlockNumber2 >> 8);
						dibBlock[3 + x * 9 + 3] = (byte) (freeBlockNumber2 >> 16);
						dibBlock[3 + x * 9 + 4] = (byte) (freeBlockNumber2 >> 24);
						dibBlock[3 + x * 9 + 5] = (byte) (freeBlockNumber2 >> 32);
						dibBlock[3 + x * 9 + 6] = (byte) (freeBlockNumber2 >> 40);
						dibBlock[3 + x * 9 + 7] = (byte) (freeBlockNumber2 >> 48);
						dibBlock[3 + x * 9 + 8] = (byte) (freeBlockNumber2 >> 56);
						if (!writeDirectory(freeBlockNumber2, dir.listFiles()[x + arrayOffset])) {
							return false;
						}
					}
				} else {
					dibBlock[3 + x * 9] = 0;
					dibBlock[3 + x * 9 + 1] = 0;
					dibBlock[3 + x * 9 + 2] = 0;
					dibBlock[3 + x * 9 + 3] = 0;
					dibBlock[3 + x * 9 + 4] = 0;
					dibBlock[3 + x * 9 + 5] = 0;
					dibBlock[3 + x * 9 + 6] = 0;
					dibBlock[3 + x * 9 + 7] = 0;
					dibBlock[3 + x * 9 + 8] = 0;
				}
			}

			if (arrayOffset + NumberOfLinkPerDirectoryIndirectBlock < dir.listFiles().length) {
				// we need another directory indirect block

				long dibBlockNumber = allocateFreeBlock();
				if (dibBlockNumber == -1) {
					runOutFreeBlock();
					return false;
				}

				dibBlock[PFSSettingConstants.blockSize - 8] = (byte) dibBlockNumber;
				dibBlock[PFSSettingConstants.blockSize - 7] = (byte) (dibBlockNumber >> 8);
				dibBlock[PFSSettingConstants.blockSize - 6] = (byte) (dibBlockNumber >> 16);
				dibBlock[PFSSettingConstants.blockSize - 5] = (byte) (dibBlockNumber >> 24);
				dibBlock[PFSSettingConstants.blockSize - 4] = (byte) (dibBlockNumber >> 32);
				dibBlock[PFSSettingConstants.blockSize - 3] = (byte) (dibBlockNumber >> 40);
				dibBlock[PFSSettingConstants.blockSize - 2] = (byte) (dibBlockNumber >> 48);
				dibBlock[PFSSettingConstants.blockSize - 1] = (byte) (dibBlockNumber >> 56);

				writeDirectoryIndirectBlock(dibBlockNumber, dir, (int) (arrayOffset + NumberOfLinkPerDirectoryIndirectBlock));
			}

			bw.seek(PFSSettingConstants.partitionOffset + PFSSettingConstants.superBlockSize + currentBlockNumber * PFSSettingConstants.blockSize);
			bw.write(dibBlock);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
			if (Global.stopWhenException) {
				System.exit(0);
			}
			return false;
		}
	}

	private boolean writeFile(long currentBlockNumber, File node) {
		try {
			writeFileIndirectBlock_TimeStamp = new Date().getTime();
			if (Global.GUI) {
				if (shouldStop) {
					return false;
				}

				if (pPauseButton.getText().equals("Close")) {
					return false;
				}

				while (pPauseButton.getText().equals("Resume")) {
					// Thread.currentThread().sleep(1000);
					Thread.sleep(1000);
				}
			}

			showResult((numberOfFileWritten + 1) + "/" + numberOfFile + " Block Number=" + currentBlockNumber + ", Write file (" + PFSCommonLib.convertFilesize(node.length()) + "): "
					+ node.toString(), "");

			// write File
			byte fileBlock[] = new byte[PFSSettingConstants.blockSize];
			fileBlock[0] = 'F';
			fileBlock[1] = 'I';
			fileBlock[2] = 'L';
			fileBlock[3] = 'E';

			if (Global.GUI) {
				JTableModel.setValueAt("id written", JTableModel.getRowCount() - 1, 1);
			}

			// file name
			byte c[] = node.getName().getBytes();
			for (int x = 0; x < 500; x++) {
				if (x < c.length) {
					fileBlock[x + 4] = c[x];
				} else {
					fileBlock[x + 4] = 0;
				}
			}

			if (Global.GUI) {
				JTableModel.setValueAt("filename written", JTableModel.getRowCount() - 1, 1);
			}

			// permission
			if (ini.getStringProperty(node.toString(), "owner read", "").equals("true")) {
				fileBlock[504] = 'r';
			} else {
				fileBlock[504] = '-';
			}
			if (ini.getStringProperty(node.toString(), "owner write", "").equals("true")) {
				fileBlock[505] = 'w';
			} else {
				fileBlock[505] = '-';
			}
			if (ini.getStringProperty(node.toString(), "owner execute", "").equals("true")) {
				fileBlock[506] = 'x';
			} else {
				fileBlock[506] = '-';
			}
			if (ini.getStringProperty(node.toString(), "group read", "").equals("true")) {
				fileBlock[507] = 'r';
			} else {
				fileBlock[507] = '-';
			}
			if (ini.getStringProperty(node.toString(), "group write", "").equals("true")) {
				fileBlock[508] = 'w';
			} else {
				fileBlock[508] = '-';
			}
			if (ini.getStringProperty(node.toString(), "group execute", "").equals("true")) {
				fileBlock[509] = 'x';
			} else {
				fileBlock[509] = '-';
			}
			if (ini.getStringProperty(node.toString(), "other read", "").equals("true")) {
				fileBlock[510] = 'r';
			} else {
				fileBlock[510] = '-';
			}
			if (ini.getStringProperty(node.toString(), "other write", "").equals("true")) {
				fileBlock[511] = 'w';
			} else {
				fileBlock[511] = '-';
			}
			if (ini.getStringProperty(node.toString(), "other execute", "").equals("true")) {
				fileBlock[512] = 'x';
			} else {
				fileBlock[512] = '-';
			}

			if (Global.GUI) {
				JTableModel.setValueAt("permission written", JTableModel.getRowCount() - 1, 1);
			}

			// create time
			fileBlock[513] = 0;
			fileBlock[514] = 0;
			fileBlock[515] = 0;
			fileBlock[516] = 0;
			fileBlock[517] = 0;
			fileBlock[518] = 0;
			fileBlock[519] = 0;
			fileBlock[520] = 0;

			if (Global.GUI) {
				JTableModel.setValueAt("create time written", JTableModel.getRowCount() - 1, 1);
			}

			// last modified time
			fileBlock[521] = 0;
			fileBlock[522] = 0;
			fileBlock[523] = 0;
			fileBlock[524] = 0;
			fileBlock[525] = 0;
			fileBlock[526] = 0;
			fileBlock[527] = 0;
			fileBlock[528] = 0;

			if (Global.GUI) {
				JTableModel.setValueAt("last modified time written", JTableModel.getRowCount() - 1, 1);
			}

			// filesize
			long filesize = node.length();
			fileBlock[529] = (byte) filesize;
			fileBlock[530] = (byte) (filesize >> 8);
			fileBlock[531] = (byte) (filesize >> 16);
			fileBlock[532] = (byte) (filesize >> 24);
			fileBlock[533] = (byte) (filesize >> 32);
			fileBlock[534] = (byte) (filesize >> 40);
			fileBlock[535] = (byte) (filesize >> 48);
			fileBlock[536] = (byte) (filesize >> 56);

			if (Global.GUI) {
				JTableModel.setValueAt("filesize written", JTableModel.getRowCount() - 1, 1);
			}

			// File indirect block link
			long numberOfFileContentBlock = (filesize % PFSSettingConstants.blockSize == 0) ? filesize / PFSSettingConstants.blockSize
					: (filesize / PFSSettingConstants.blockSize) + 1;
			long fibBlockNumber = 0;
			if (numberOfFileContentBlock > NumberOfLinkPerFileBlock) {
				fibBlockNumber = allocateFreeBlock();
				fileBlock[537] = (byte) fibBlockNumber;
				fileBlock[538] = (byte) (fibBlockNumber >> 8);
				fileBlock[539] = (byte) (fibBlockNumber >> 16);
				fileBlock[540] = (byte) (fibBlockNumber >> 24);
				fileBlock[541] = (byte) (fibBlockNumber >> 32);
				fileBlock[542] = (byte) (fibBlockNumber >> 40);
				fileBlock[543] = (byte) (fibBlockNumber >> 48);
				fileBlock[544] = (byte) (fibBlockNumber >> 56);
			} else {
				fileBlock[537] = 0;
				fileBlock[538] = 0;
				fileBlock[539] = 0;
				fileBlock[540] = 0;
				fileBlock[541] = 0;
				fileBlock[542] = 0;
				fileBlock[543] = 0;
				fileBlock[544] = 0;
			}

			if (Global.GUI) {
				JTableModel.setValueAt("File indirect block link written", JTableModel.getRowCount() - 1, 1);
			}

			// file content block link
			long fileOffset = 0;
			for (int x = 0, offset = 545; x < NumberOfLinkPerFileBlock; x++, offset += 8) {
				if (Global.GUI) {
					if (shouldStop) {
						return false;
					}

					if (pPauseButton.getText().equals("Close")) {
						return false;
					}

					while (pPauseButton.getText().equals("Resume")) {
						// Thread.currentThread().sleep(1000);
						Thread.sleep(1000);
					}
				}
				if (x < numberOfFileContentBlock) {
					long freeBlockNumber = allocateFreeBlock();
					if (freeBlockNumber == -1) {
						runOutFreeBlock();
						return false;
					}

					fileBlock[offset] = (byte) freeBlockNumber;
					fileBlock[offset + 1] = (byte) (freeBlockNumber >> 8);
					fileBlock[offset + 2] = (byte) (freeBlockNumber >> 16);
					fileBlock[offset + 3] = (byte) (freeBlockNumber >> 24);
					fileBlock[offset + 4] = (byte) (freeBlockNumber >> 32);
					fileBlock[offset + 5] = (byte) (freeBlockNumber >> 40);
					fileBlock[offset + 6] = (byte) (freeBlockNumber >> 48);
					fileBlock[offset + 7] = (byte) (freeBlockNumber >> 56);
					fileOffset += writeFileContentBlock(node, fileOffset, freeBlockNumber);
					if (Global.GUI) {
						if (fileOffset < 1024) {
							JTableModel.setValueAt("file content block link : " + fileOffset + " /" + filesize + " bytes", JTableModel.getRowCount() - 1, 1);
						} else if (fileOffset >= 1024 && fileOffset < 1024 * 1024) {
							JTableModel.setValueAt("file content block link : " + fileOffset / 1024 + " /" + filesize / 1024 + " KB", JTableModel.getRowCount() - 1, 1);
						} else if (fileOffset >= 1024 * 1024) {
							JTableModel.setValueAt("file content block link : " + fileOffset / 1024 / 1024 + " (" + fileOffset / 1024 + "KB) / " + filesize / 1024 / 1024 + " MB",
									JTableModel.getRowCount() - 1, 1);
						}
					} else {
						PFSCommonLib.printFilesize(fileOffset, filesize);
					}
				} else {
					fileBlock[offset] = 0;
					fileBlock[offset + 1] = 0;
					fileBlock[offset + 2] = 0;
					fileBlock[offset + 3] = 0;
					fileBlock[offset + 4] = 0;
					fileBlock[offset + 5] = 0;
					fileBlock[offset + 6] = 0;
					fileBlock[offset + 7] = 0;
				}
			}

			bw.seek(PFSSettingConstants.partitionOffset + PFSSettingConstants.superBlockSize + currentBlockNumber * PFSSettingConstants.blockSize);
			bw.write(fileBlock);

			if (Global.GUI) {
				JTableModel.setValueAt("done", JTableModel.getRowCount() - 1, 1);
			}

			// write file indirector block
			if (numberOfFileContentBlock > NumberOfLinkPerFileBlock) {
				if (!writeFileIndirectBlock(node, fileOffset, fibBlockNumber)) {
					return false;
				}
			}
			if (Global.GUI) {
				JProgressBar.setValue(JProgressBar.getValue() + 1);
			} else {
				System.out.print("\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b");
			}
			numberOfFileWritten++;
			if (Global.GUI) {
				pInfoLabel.setText(numberOfFileWritten + "/" + numberOfFile + " files written");
			}
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
			if (Global.stopWhenException) {
				System.exit(0);
			}
			return false;
		}
	}

	private long writeFileContentBlock(File node, long fileOffset, long blockNumber) {
		try {
			byte fileContentBytes[] = new byte[PFSSettingConstants.blockSize];
			int numberOfByteRead;

			RandomAccessFile bo = new RandomAccessFile(node, "r");
			bo.seek(fileOffset);
			numberOfByteRead = bo.read(fileContentBytes, 0, fileContentBytes.length);
			bo.close();

			bw.seek(PFSSettingConstants.partitionOffset + PFSSettingConstants.superBlockSize + blockNumber * PFSSettingConstants.blockSize);
			bw.write(fileContentBytes);

			return numberOfByteRead;
			// return 4096;
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
			if (Global.stopWhenException) {
				System.exit(0);
			}
		}
		return fileOffset;
	}

	private boolean writeFileIndirectBlock(File node, long fileOffset, long blockNumber) {
		try {
			byte FIBBlock[] = new byte[PFSSettingConstants.blockSize];
			FIBBlock[0] = 'F';
			FIBBlock[1] = 'I';
			FIBBlock[2] = 'B';
			for (int x = 3; x <= PFSSettingConstants.blockSize - 8 - 8; x += 8) {
				if (Global.GUI) {
					if (shouldStop) {
						return false;
					}

					while (pPauseButton.getText().equals("Resume")) {
						// Thread.currentThread().sleep(1000);
						Thread.sleep(1000);
					}
				}
				if (fileOffset < node.length()) {
					long fileContentBlockNumber = allocateFreeBlock();
					if (fileContentBlockNumber == -1) {
						runOutFreeBlock();
						return false;
					} else {
						FIBBlock[x] = (byte) fileContentBlockNumber;
						FIBBlock[x + 1] = (byte) (fileContentBlockNumber >> 8);
						FIBBlock[x + 2] = (byte) (fileContentBlockNumber >> 16);
						FIBBlock[x + 3] = (byte) (fileContentBlockNumber >> 24);
						FIBBlock[x + 4] = (byte) (fileContentBlockNumber >> 32);
						FIBBlock[x + 5] = (byte) (fileContentBlockNumber >> 40);
						FIBBlock[x + 6] = (byte) (fileContentBlockNumber >> 48);
						FIBBlock[x + 7] = (byte) (fileContentBlockNumber >> 56);

						fileOffset += writeFileContentBlock(node, fileOffset, fileContentBlockNumber);

						if (Global.GUI) {
							if (fileOffset < 1024) {
								JTableModel.setValueAt("file content block link : " + fileOffset + " /" + node.length() + " bytes", JTableModel.getRowCount() - 1, 1);
							} else if (fileOffset >= 1024 && fileOffset < 1024 * 1024) {
								JTableModel.setValueAt("file content block link : " + fileOffset / 1024 + " /" + node.length() / 1024 + " KB", JTableModel.getRowCount() - 1, 1);
							} else if (fileOffset >= 1024 * 1024) {
								JTableModel.setValueAt(
										"file content block link : " + fileOffset / 1024 / 1024 + " (" + fileOffset / 1024 + "KB) / " + node.length() / 1024 / 1024 + " MB",
										JTableModel.getRowCount() - 1, 1);
							}
						} else {
							PFSCommonLib.printFilesize(fileOffset, node.length());
							System.out.print(String.format("%.2f", ((float) fileOffset / (new Date().getTime() - writeFileIndirectBlock_TimeStamp)) / 1024) + "MB/s    ");
						}

					}
					// System.out.println(blockNumber + ":" +(x-3)+":"+
					// fileOffset);
				} else {
					FIBBlock[x] = 0;
					FIBBlock[x + 1] = 0;
					FIBBlock[x + 2] = 0;
					FIBBlock[x + 3] = 0;
					FIBBlock[x + 4] = 0;
					FIBBlock[x + 5] = 0;
					FIBBlock[x + 6] = 0;
					FIBBlock[x + 7] = 0;
				}
			}

			// File indirect block link
			if (fileOffset < node.length()) {
				long fibBlockNumber = allocateFreeBlock();
				if (fibBlockNumber == -1) {
					runOutFreeBlock();
					return false;
				}

				FIBBlock[PFSSettingConstants.blockSize - 8] = (byte) fibBlockNumber;
				FIBBlock[PFSSettingConstants.blockSize - 7] = (byte) (fibBlockNumber >> 8);
				FIBBlock[PFSSettingConstants.blockSize - 6] = (byte) (fibBlockNumber >> 16);
				FIBBlock[PFSSettingConstants.blockSize - 5] = (byte) (fibBlockNumber >> 24);
				FIBBlock[PFSSettingConstants.blockSize - 4] = (byte) (fibBlockNumber >> 32);
				FIBBlock[PFSSettingConstants.blockSize - 3] = (byte) (fibBlockNumber >> 40);
				FIBBlock[PFSSettingConstants.blockSize - 2] = (byte) (fibBlockNumber >> 48);
				FIBBlock[PFSSettingConstants.blockSize - 1] = (byte) (fibBlockNumber >> 56);
				if (!writeFileIndirectBlock(node, fileOffset, fibBlockNumber)) {
					return false;
				}
			} else {
				FIBBlock[PFSSettingConstants.blockSize - 8] = 0;
				FIBBlock[PFSSettingConstants.blockSize - 7] = 0;
				FIBBlock[PFSSettingConstants.blockSize - 6] = 0;
				FIBBlock[PFSSettingConstants.blockSize - 5] = 0;
				FIBBlock[PFSSettingConstants.blockSize - 4] = 0;
				FIBBlock[PFSSettingConstants.blockSize - 3] = 0;
				FIBBlock[PFSSettingConstants.blockSize - 2] = 0;
				FIBBlock[PFSSettingConstants.blockSize - 1] = 0;
			}
			// end File indirect block link

			bw.seek(PFSSettingConstants.partitionOffset + PFSSettingConstants.superBlockSize + blockNumber * PFSSettingConstants.blockSize);
			bw.write(FIBBlock);

			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
			if (Global.stopWhenException) {
				System.exit(0);
			}
			return false;
		}
	}

	private long allocateFreeBlock() {
		try {
			long blockNumber = 0;
			for (long x = 0; x < NumberOfFreeAddressBlock; x++) {
				if (!fabBufferUsed[(int) x]) {
					for (int offset = 3; offset < PFSSettingConstants.blockSize; offset++) {
						if (fabBuffer[(int) x][offset] == 0 && blockNumber < NumberOfBlock) {
							fabBuffer[(int) x][offset] = 1;
							if (offset == PFSSettingConstants.blockSize - 1) {
								fabBufferUsed[(int) x] = true;
							}
							return blockNumber;
						}
						blockNumber++;
					}
				} else {
					blockNumber += PFSSettingConstants.blockSize - 3;
				}
			}
			return -1;
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
			if (Global.stopWhenException) {
				System.exit(0);
			}
			return -1;
		}
	}

	private void showResult(String columneOneString, long columneTwoString) {
		showResult(columneOneString, String.valueOf(columneTwoString));
	}

	private void showResult(String columneOneString, String columneTwoString) {
		if (Global.GUI) {
			JTableModel.add(columneOneString, columneTwoString);
			JScrollPane.getViewport().setViewPosition(new Point(0, JTable.getMaximumSize().height));
		} else {
			System.out.println(columneOneString + " " + columneTwoString);
		}
	}
}
