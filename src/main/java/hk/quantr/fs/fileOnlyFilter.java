package hk.quantr.fs;

import java.io.File;

/**
 * <p>
 * Title: Petersoft Java Style
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author not attributable
 * @version 2004
 */
public class fileOnlyFilter implements java.io.FileFilter {
	// Accept all directories and all gif, jpg, tiff, or png files.
	public boolean accept(File f) {
		return !f.isDirectory();
	}
}
