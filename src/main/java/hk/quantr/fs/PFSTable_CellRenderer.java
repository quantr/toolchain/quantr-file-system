package hk.quantr.fs;

import java.awt.Color;
import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class PFSTable_CellRenderer extends JLabel implements TableCellRenderer {
	ImageIcon sbIcon = new ImageIcon(QFSBuilder.class.getResource("images/PFSTable/SB.png"));
	ImageIcon fabIcon = new ImageIcon(QFSBuilder.class.getResource("images/PFSTable/FAB.png"));
	ImageIcon dibIcon = new ImageIcon(QFSBuilder.class.getResource("images/PFSTable/DIB.png"));
	ImageIcon dirIcon = new ImageIcon(QFSBuilder.class.getResource("images/PFSTable/DIR.png"));
	ImageIcon fileIcon = new ImageIcon(QFSBuilder.class.getResource("images/PFSTable/FILE.png"));
	ImageIcon fibIcon = new ImageIcon(QFSBuilder.class.getResource("images/PFSTable/FIB.png"));
	ImageIcon fcbIcon = new ImageIcon(QFSBuilder.class.getResource("images/PFSTable/FCB.png"));
	ImageIcon unknownIcon = new ImageIcon(QFSBuilder.class.getResource("images/PFSTable/unknown.png"));

	public PFSTable_CellRenderer() {
		super();
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		if (isSelected) {
			this.setBackground(new Color(180, 226, 252));
		} else {
			this.setBackground(Color.white);
		}
		if (value != null) {
			if (((PFSTableModel) table.getModel()).getMode() == 0) {
				this.setIcon(null);
				if (column == 0) {
					this.setText(((Integer) value).toString());
				} else {
					this.setText((String) value);
				}
			} else {
				if (column == 0) {
					this.setIcon(null);
					this.setText(((Integer) value).toString());
				} else {
					String temp = new String((byte[]) value);
					if (temp.startsWith("SB")) {
						this.setIcon(sbIcon);
						this.setText("SB");
					} else if (temp.startsWith("FAB")) {
						this.setIcon(fabIcon);
						this.setText("FAB");
					} else if (temp.startsWith("DIR")) {
						this.setIcon(dirIcon);
						this.setText(PFSCommonLib.trimByteZero(temp.substring(3)));
					} else if (temp.startsWith("DIB")) {
						this.setIcon(dibIcon);
						this.setText("DIB");
					} else if (temp.startsWith("FILE")) {
						this.setIcon(fileIcon);
						// this.setText("FILE");
						this.setText(PFSCommonLib.trimByteZero(temp.substring(4)));
					} else if (temp.startsWith("FIB")) {
						this.setIcon(fibIcon);
						this.setText("FIB");
					} else {
						this.setIcon(unknownIcon);
						this.setText("");
					}
				}
			}
		} else {
			this.setText(null);
		}

		return this;
	}

	private void jbInit() throws Exception {
		this.setOpaque(true);
	}

}
