package hk.quantr.fs;

import java.awt.BorderLayout;
import java.awt.HeadlessException;

import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 * <p>
 * Title: PFS
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2005
 * </p>
 * 
 * <p>
 * Company: Petersoft
 * </p>
 * 
 * @author Peter
 * @version 2005
 */
public class PFSUserSelectorDialog extends JDialog {
	PFSUserSelectorPanel pFSUserSelectorPanel1 = new PFSUserSelectorPanel();
	BorderLayout borderLayout1 = new BorderLayout();

	public PFSUserSelectorDialog() throws HeadlessException {
		super();
		try {
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public PFSUserSelectorDialog(JFrame parent) throws HeadlessException {
		super(parent, "Select user", true);
		try {
			pFSUserSelectorPanel1 = new PFSUserSelectorPanel(this, PFSSettingConstants.tempDirectory);
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	public PFSUserSelectorDialog(JDialog parent) throws HeadlessException {
		super(parent, "Select user", true);
		try {
			pFSUserSelectorPanel1 = new PFSUserSelectorPanel(this, PFSSettingConstants.tempDirectory);
			jbInit();
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Global.GUI) {
				new PExceptionDialog(ex, Global.sessionNumber).setVisible(true);
			}
		}
	}

	private void jbInit() throws Exception {
		this.getContentPane().setLayout(borderLayout1);
		this.getContentPane().add(pFSUserSelectorPanel1, java.awt.BorderLayout.CENTER);
		this.setSize(400, 400);
		PFSCommonLib.centerDialog(this);
	}

	public boolean isCancel() {
		return pFSUserSelectorPanel1.isCancel();
	}

	public String getSelectedUser() {
		return pFSUserSelectorPanel1.getSelectedUser();
	}
}
